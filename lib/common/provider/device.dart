// 🎯 Dart imports:
import 'dart:async';
import 'dart:io';

// 📦 Package imports:
import 'package:device_info_plus/device_info_plus.dart';

class DeviceInfo {
  static DeviceInfo? _instance;
  late final String _deviceId;
  late final String _deviceName;
  late final String _deviceModel;
  late final String _productName;
  late final String _manufacturer;
  late final String _version;
  late final String _platform;
  late final bool _isPhysicalDevice;

  DeviceInfo._(this._deviceId, this._deviceName, this._deviceModel, this._productName, this._manufacturer, this._version, this._isPhysicalDevice) {
    _platform = Platform.isIOS ? 'APPLE' : 'ANDROID';
  }

  static Future<void> initialize() async {
    if (_instance == null) {
      Map<String, dynamic> deviceInfoMap = await _getDeviceInfoMap();
      _instance = DeviceInfo._(
        deviceInfoMap['deviceId'],
        deviceInfoMap['deviceName'],
        deviceInfoMap['model'],
        deviceInfoMap['product'],
        deviceInfoMap['manufacturer'],
        deviceInfoMap['version.release'],
        deviceInfoMap['isPhysicalDevice'],
      );
    }
  }

  static DeviceInfo get instance {
    if (_instance == null) {
      throw Exception('DeviceInfo is not initialized');
    }
    return _instance!;
  }

  String get deviceId => _deviceId;
  String get deviceName => _deviceName;
  String get deviceModel => _deviceModel;
  String get productName => _productName;
  String get manufacturer => _manufacturer;
  String get version => _version;
  String get platform => _platform;
  bool get isPhysicalDevice => _isPhysicalDevice;

  static Future<Map<String, dynamic>> _getDeviceInfoMap() async {
    DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
    Map<String, dynamic> map = <String, dynamic>{};
    if (Platform.isAndroid) {
      AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
      map['deviceId'] = androidInfo.id;
      map['deviceName'] = androidInfo.device;
      map['model'] = androidInfo.model;
      map['product'] = androidInfo.product;
      map['manufacturer'] = androidInfo.manufacturer;
      map['version.release'] = androidInfo.version.release;
      map['isPhysicalDevice'] = androidInfo.isPhysicalDevice;
    } else if (Platform.isIOS) {
      IosDeviceInfo iosInfo = await deviceInfo.iosInfo;
      map['deviceId'] = iosInfo.identifierForVendor;
      map['deviceName'] = iosInfo.name;
      map['model'] = iosInfo.model;
      map['product'] = iosInfo.systemName;
      map['manufacturer'] = iosInfo.systemName;
      map['version.release'] = iosInfo.systemVersion;
      map['isPhysicalDevice'] = iosInfo.isPhysicalDevice;
    } else {
      map['deviceId'] = 'unknown';
      map['deviceName'] = 'unknown';
      map['model'] = 'unknown';
      map['product'] = 'unknown';
      map['manufacturer'] = 'unknown';
      map['version.release'] = 'unknown';
      map['isPhysicalDevice'] = false;
    }
    return map;
  }
}
