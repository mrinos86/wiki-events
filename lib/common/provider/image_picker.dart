import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wiki_events/utils/utils.dart';

class ImageUtil {
  int? imageQuality;
  late BuildContext context;

  Future<String?> getImage(
      {String? title,
      String? message,
      required BuildContext context,
      int? imageQuality,
      CameraDevice preferredCameraDevice = CameraDevice.rear}) async {
    this.imageQuality = imageQuality ?? 70;
    this.context = context;
    String? imagePath;
    imagePath = await showCupertinoModalPopup(
        context: context,
        builder: (BuildContext context1) => CupertinoActionSheet(
              title: title != null
                  ? Text(
                      title,
                      style: size15_M_regular(textColor: colorGrey),
                    )
                  : null,
              cancelButton: CupertinoActionSheetAction(
                child: Text(
                  "Cancel",
                  style: size16_M_regular(textColor: Colors.blue),
                ),
                onPressed: () {
                  Navigator.of(context).pop(null);
                },
              ),
              actions: [
                CupertinoActionSheetAction(
                  onPressed: () async {
                    imagePath = await pickImageFromGallery(context,
                        imageQuality: imageQuality);
                    // ignore: use_build_context_synchronously
                    Navigator.of(context).pop(imagePath);
                  },
                  child: Text(
                    "Gallery",
                    style: size17_M_medium(),
                  ),
                ),
                CupertinoActionSheetAction(
                    onPressed: () async {
                      imagePath = await pickImageFromCamera(context,
                          imageQuality: imageQuality);
                      // ignore: use_build_context_synchronously
                      Navigator.of(context).pop(imagePath);
                    },
                    child: Text(
                      "Camera",
                      style: size17_M_medium(),
                    )),
              ],
            ));
    debugPrint("/*-/*-/*/*-*-/-===============>image picked ");
    if (imagePath != null) {
      imagePath = await _updateImage(imagePath!, imageQuality: imageQuality);
      debugPrint(
          "/*-/*-/*/*-*-/-===============>we get the cropped image from cropper ");
    }
    return imagePath;
  }

  Future<String?> _updateImage(String value, {int? imageQuality}) async {
    CroppedFile? croppedImage = await ImageCropper().cropImage(
      sourcePath: value,
      // aspectRatio: const CropAspectRatio(ratioX: 1, ratioY: 1),
      aspectRatioPresets: [
        CropAspectRatioPreset.square,
        CropAspectRatioPreset.ratio3x2,
        CropAspectRatioPreset.original,
        CropAspectRatioPreset.ratio4x3,
        CropAspectRatioPreset.ratio16x9
      ],
      compressQuality: imageQuality ?? 75,
      maxHeight: 250,
      maxWidth: 250,
      compressFormat: ImageCompressFormat.jpg,
    );
    return croppedImage?.path;
  }

/*  _cancelDialog(BuildContext context) {
    Navigator.of(context, rootNavigator: true).pop("Discard");
  }*/
}

/// ImagePikerFrom Gallery

Future<String?> pickImageFromGallery(context, {int? imageQuality}) async {
  final ImagePicker picker = ImagePicker();
  File? image;

  // Navigator.pop(context);
  final pikedImage = await picker.pickImage(
      source: ImageSource.gallery, imageQuality: imageQuality ?? 75);
  if (pikedImage != null) {
    image = File(pikedImage.path);
  } else {
    return null;
  }
  return image.path;
}

/// ImagePikerFrom Camera

Future<String?> pickImageFromCamera(context, {int? imageQuality}) async {
  final ImagePicker picker = ImagePicker();
  File? image;

  // Navigator.pop(context);
  final pikedImage = await picker.pickImage(
      source: ImageSource.camera, imageQuality: imageQuality ?? 75);
  if (pikedImage != null) {
    // Log.info(tag: "image", message: pikedImage.path);
    image = File(pikedImage.path);
  } else {
    return null;
  }
  return image.path;
}
