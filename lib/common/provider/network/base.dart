import 'dart:async';

import 'package:dio/dio.dart';
import 'package:wiki_events/utils/configs.dart';

abstract class BaseApi {
  late final Dio _dio;

  BaseApi({List<Interceptor> interceptors = const []}) {
    _dio = Dio(BaseOptions(
      baseUrl: AppConfigs.baseUrl,
      connectTimeout: const Duration(seconds: 15),
      receiveTimeout: const Duration(seconds: 15),
      headers: {
        'Accept': 'application/json',
      },
      contentType: "application/x-www-form-urlencoded",
      responseType: ResponseType.json,
    ))
      ..interceptors.addAll(interceptors);
  }

  Future<Response<Map<String, dynamic>>> get(String path,
      {Map<String, dynamic>? queryParameters,
      Map<String, dynamic>? headers}) async {
    Response response = await _dio.get(path,
        queryParameters: queryParameters, options: Options(headers: headers));
    if (response.statusCode == 204) {
      return Response(
        data: <String, dynamic>{
          'result': true,
          'payload': null,
        },
        statusCode: response.statusCode,
        requestOptions: response.requestOptions,
        statusMessage: response.statusMessage,
      );
    } else {
      return Response(
        data: response.data,
        statusCode: response.statusCode,
        requestOptions: response.requestOptions,
        statusMessage: response.statusMessage,
      );
    }
  }

  Future<Response<Map<String, dynamic>>> post(String path,
      {dynamic data,
      Map<String, dynamic>? queryParameters,
      String? contentType = "application/x-www-form-urlencoded",
      Map<String, dynamic>? headers,
      Function(int, int)? onSendProgress}) async {
    Response response = await _dio.post(path,
        data: data,
        queryParameters: queryParameters,
        options: Options(contentType: contentType, headers: headers),
        onSendProgress: onSendProgress);
    if (response.statusCode == 204) {
      return Response(
        data: <String, dynamic>{
          'result': true,
          'payload': null,
        },
        statusCode: response.statusCode,
        requestOptions: response.requestOptions,
        statusMessage: response.statusMessage,
      );
    } else {
      return Response(
        data: response.data,
        statusCode: response.statusCode,
        requestOptions: response.requestOptions,
        statusMessage: response.statusMessage,
      );
    }
  }

  Future<Response<Map<String, dynamic>>> put(String path,
      {dynamic data,
      Map<String, dynamic>? queryParameters,
      String? contentType = "application/x-www-form-urlencoded",
      Map<String, dynamic>? headers,
      Function(int, int)? onSendProgress}) async {
    Response response = await _dio.put(path,
        data: data,
        queryParameters: queryParameters,
        options: Options(contentType: contentType, headers: headers),
        onSendProgress: onSendProgress);
    if (response.statusCode == 204) {
      return Response(
        data: <String, dynamic>{
          'result': true,
          'payload': null,
        },
        statusCode: response.statusCode,
        requestOptions: response.requestOptions,
        statusMessage: response.statusMessage,
      );
    } else {
      return Response(
        data: response.data,
        statusCode: response.statusCode,
        requestOptions: response.requestOptions,
        statusMessage: response.statusMessage,
      );
    }
  }

  Future<Response<Map<String, dynamic>>> delete(String path,
      {Map<String, dynamic>? queryParameters,
      Map<String, dynamic>? headers}) async {
    Response response = await _dio.delete(path,
        queryParameters: queryParameters, options: Options(headers: headers));
    if (response.statusCode == 204) {
      return Response(
        data: <String, dynamic>{
          'result': true,
          'payload': null,
        },
        statusCode: response.statusCode,
        requestOptions: response.requestOptions,
        statusMessage: response.statusMessage,
      );
    } else {
      return Response(
        data: response.data,
        statusCode: response.statusCode,
        requestOptions: response.requestOptions,
        statusMessage: response.statusMessage,
      );
    }
  }

  Future<Response<Map<String, dynamic>>> patch(String path,
      {dynamic data,
      Map<String, dynamic>? queryParameters,
      String? contentType = "application/x-www-form-urlencoded",
      Map<String, dynamic>? headers,
      Function(int, int)? onSendProgress}) async {
    Response response = await _dio.patch(path,
        data: data,
        queryParameters: queryParameters,
        options: Options(contentType: contentType, headers: headers),
        onSendProgress: onSendProgress);
    if (response.statusCode == 204) {
      return Response(
        data: <String, dynamic>{
          'result': true,
          'payload': null,
        },
        statusCode: response.statusCode,
        requestOptions: response.requestOptions,
        statusMessage: response.statusMessage,
      );
    } else {
      return Response(
        data: response.data,
        statusCode: response.statusCode,
        requestOptions: response.requestOptions,
        statusMessage: response.statusMessage,
      );
    }
  }
}
