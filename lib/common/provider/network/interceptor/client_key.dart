import 'package:dio/dio.dart';
import 'package:wiki_events/utils/keys.dart';

class ClientKeyInterceptor extends Interceptor {
  final String xApiKey = AppKeys.API_KEY;

  ClientKeyInterceptor();

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    options.headers.addEntries([MapEntry('x-api-key', xApiKey)]);
    return super.onRequest(options, handler);
  }
}
