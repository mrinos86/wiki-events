import 'package:dio/dio.dart';
import 'package:wiki_events/auth/provider/authentication.dart';

class AccessTokenInterceptor extends Interceptor {
  final AuthServiceProvider _authService = AuthServiceProvider.instance;

  @override
  void onRequest(RequestOptions options, RequestInterceptorHandler handler) {
    if (_authService.user != null) {
      options.headers
          .addEntries([MapEntry('x-access-token', '${_authService.token}')]);
    }
    super.onRequest(options, handler);
  }
}
