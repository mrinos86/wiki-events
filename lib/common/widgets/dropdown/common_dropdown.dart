import 'package:dropdown_button2/dropdown_button2.dart';
import 'package:flutter/material.dart';
import 'package:wiki_events/events/view/view.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class CommonDropdown extends StatelessWidget {
  List<DropdownItems> dropdownList;
  DropdownItems? currentItem;
  Function onChangedCallBack;
  bool isError;
  String hintText;
  TextStyle? itemStyle;
  TextStyle? hintStyle;
  double? dropdownFieldHeight;
  double? dropdownMenuHeight;
  Color? menuColor;
  CommonDropdown({
    super.key,
    required this.dropdownList,
    required this.currentItem,
    required this.onChangedCallBack,
    this.isError = false,
    this.hintText = "Select",
    this.itemStyle,
    this.hintStyle,
    this.dropdownFieldHeight,
    this.dropdownMenuHeight,
  });

  @override
  Widget build(BuildContext context) {
    return DropdownButtonHideUnderline(
      child: DropdownButton2(
        isExpanded: true,
        hint: Row(
          children: [
            Expanded(
              child: Text(
                hintText,
                style: hintStyle ?? size14_M_medium(textColor: colorGrey),
              ),
            ),
          ],
        ),
        items: dropdownList
            .map(
              (item) => DropdownMenuItem<DropdownItems>(
                value: item,
                child: Text(item.title, style: itemStyle ?? size14_M_medium()),
              ),
            )
            .toList(),
        value: currentItem,
        onChanged: (DropdownItems? value) {
          onChangedCallBack(value);
        },
        buttonStyleData: ButtonStyleData(
          height: dropdownFieldHeight ?? 50,
          padding: const EdgeInsets.only(right: 12),
          decoration: common_border_dec(
              borderRadius: 8, borderColor: isError ? colorRed : colorGrey),
          elevation: 0,
        ),
        iconStyleData: const IconStyleData(
          icon: Icon(
            Icons.expand_more_outlined,
            color: primaryColor,
            size: 30,
          ),
        ),
        dropdownStyleData: DropdownStyleData(
          maxHeight: dropdownMenuHeight ?? 150,
          decoration: common_border_dec(
              borderRadius: 8,
              borderColor: darkGreyColor.withOpacity(0.5),
              color: menuColor),
          elevation: 10,
          offset: const Offset(0, -5),
        ),
        menuItemStyleData: const MenuItemStyleData(
          height: 35,
          padding: EdgeInsets.only(left: 8, right: 12),
        ),
      ),
    );
  }
}
