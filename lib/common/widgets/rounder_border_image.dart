import 'package:flutter/material.dart';
import 'package:wiki_events/utils/colors.dart';

class RoundedBorderBoxImage extends StatelessWidget {
  final double imageSize;
  final String? imageUrl;
  const RoundedBorderBoxImage({
    Key? key,
    required this.imageSize,
    required this.imageUrl,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipOval(
      child: SizedBox.fromSize(
        size: Size.fromRadius(imageSize),
        child: imageUrl == null
            ? Container(
                color: colorGrey,
                child: Icon(
                  Icons.person,
                  color: liteGreyColor,
                  size: imageSize,
                ),
              )
            : Image.network(
                imageUrl!,
                fit: BoxFit.cover,
                loadingBuilder: (BuildContext context, Widget child,
                    ImageChunkEvent? loadingProgress) {
                  if (loadingProgress == null) return child;
                  return Center(
                    child: CircularProgressIndicator(
                      value: loadingProgress.expectedTotalBytes != null
                          ? loadingProgress.cumulativeBytesLoaded /
                              loadingProgress.expectedTotalBytes!
                          : null,
                    ),
                  );
                },
                errorBuilder:
                    (BuildContext context, Object exception, stackTrace) {
                  return Container(
                    color: colorGrey,
                    child: Icon(
                      Icons.person,
                      color: liteGreyColor,
                      size: imageSize,
                    ),
                  );
                },
              ),
      ),
    );
  }
}
