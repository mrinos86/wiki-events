import 'package:flutter/material.dart';
import 'package:wiki_events/utils/utils.dart';

class NetworkImageWidget extends StatelessWidget {
  final double imageSize;
  final String imageUrl;
  final Widget? loadErrorWidge;
  const NetworkImageWidget({
    super.key,
    required this.imageSize,
    required this.imageUrl,
    this.loadErrorWidge,
  });

  @override
  Widget build(BuildContext context) {
    return Image.network(
      height: imageSize,
      width: imageSize,
      imageUrl,
      fit: BoxFit.cover,
      loadingBuilder: (BuildContext context, Widget child,
          ImageChunkEvent? loadingProgress) {
        if (loadingProgress == null) return child;
        return Center(
          child: CircularProgressIndicator(
            value: loadingProgress.expectedTotalBytes != null
                ? loadingProgress.cumulativeBytesLoaded /
                    loadingProgress.expectedTotalBytes!
                : null,
          ),
        );
      },
      errorBuilder: (BuildContext context, Object exception, stackTrace) {
        return loadErrorWidge ??
            Icon(
              Icons.image,
              color: colorGrey,
              size: imageSize,
            );
      },
    );
  }
}
