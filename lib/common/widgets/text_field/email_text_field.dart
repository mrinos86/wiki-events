import 'package:flutter/material.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class EmailTextField extends StatelessWidget {
  TextEditingController textController;
  bool isError;
  Function onTapped;
  TextStyle? textstyle;
  EmailTextField(
      {super.key,
      required this.textController,
      required this.isError,
      required this.onTapped,
      this.textstyle});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: textController,
      keyboardType: TextInputType.emailAddress,
      style: textstyle ?? size14_M_medium(),
      decoration: InputDecoration(
          hintText: "me@example.com",
          border: OutlineInputBorder(
              borderSide:
                  BorderSide(color: isError ? colorRed : colorGrey, width: 1.0),
              borderRadius: BorderRadius.circular(8),
              gapPadding: 6.0),
          enabledBorder: OutlineInputBorder(
              borderSide:
                  BorderSide(color: isError ? colorRed : colorGrey, width: 1.0),
              borderRadius: BorderRadius.circular(8),
              gapPadding: 6.0)),
      onTap: () {
        onTapped();
      },
      onFieldSubmitted: (value) => FocusScope.of(context).nextFocus(),
      onSaved: (value) => FocusScope.of(context).nextFocus(),
    );
  }
}
