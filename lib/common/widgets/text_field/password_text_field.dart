import 'package:flutter/material.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class PasswordTextField extends StatelessWidget {
  TextEditingController textController;
  bool isObsecureText;
  bool isError;
  Function onTapped;
  Function(bool) onTappedViewIcon;
  TextStyle? textStyle;
  PasswordTextField(
      {super.key,
      required this.textController,
      required this.isError,
      required this.isObsecureText,
      required this.onTapped,
      required this.onTappedViewIcon,
      this.textStyle});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: textController,
      obscureText: isObsecureText,
      keyboardType: TextInputType.visiblePassword,
      onTap: () {
        onTapped();
      },
      style: textStyle ?? size14_M_medium(),
      decoration: InputDecoration(
        hintText: "• • • • • • • •",
        suffixIcon: InkWell(
          onTap: () {
            onTappedViewIcon(!isObsecureText);
          },
          child: Icon(
            isObsecureText
                ? Icons.visibility_off_outlined
                : Icons.visibility_outlined,
            size: 24,
            color: isError ? colorRed : darkGreyColor,
          ),
        ),
        border: OutlineInputBorder(
            borderSide:
                BorderSide(color: isError ? colorRed : colorGrey, width: 1.0),
            borderRadius: BorderRadius.circular(10),
            gapPadding: 6.0),
        enabledBorder: OutlineInputBorder(
            borderSide:
                BorderSide(color: isError ? colorRed : colorGrey, width: 1.0),
            borderRadius: BorderRadius.circular(10),
            gapPadding: 6.0),
      ),
      onFieldSubmitted: (value) => FocusScope.of(context).unfocus(),
      onSaved: (value) => FocusScope.of(context).unfocus(),
    );
  }
}
