import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class TextFieldPrefixImage extends StatelessWidget {
  OutlineInputBorder kAuthInputBorder({Color? color}) => OutlineInputBorder(
        borderRadius: BorderRadius.circular(10),
        borderSide: const BorderSide(color: Colors.white),
      );
  TextFieldPrefixImage(
      {Key? key,
      this.margin,
      this.padding,
      this.labelText,
      this.hintText,
      this.labelStyle,
      this.controller,
      this.focusNode,
      this.onFieldSubmitted,
      this.suffixOnTap,
      this.onTap,
      this.border,
      this.iconColor,
      this.style,
      required this.validator,
      this.keyboardType,
      this.obscureText = false,
      this.suffixIcon,
      this.hintStyle,
      this.readOnly = false,
      this.inputFormatters,
      this.textInputAction,
      this.maxLines = 1,
      this.contentPadding,
      this.onChanged,
      this.prefixIcon,
      this.prefixWidget})
      : super(key: key);
  final EdgeInsets? margin;
  final EdgeInsets? padding;
  final BoxBorder? border;
  final Color? iconColor;
  final void Function()? onTap;
  final void Function(dynamic value)? onFieldSubmitted;
  final TextEditingController? controller;
  final FocusNode? focusNode;
  final TextInputAction? textInputAction;
  final String? hintText;
  final String? labelText;
  final TextStyle? labelStyle;
  bool? obscureText;
  final TextStyle? style;
  final void Function()? suffixOnTap;
  final String? Function(String? value) validator;
  final TextInputType? keyboardType;
  final Widget? suffixIcon;
  final TextStyle? hintStyle;
  final bool readOnly;
  final int? maxLines;
  final EdgeInsetsGeometry? contentPadding;
  final ValueChanged<String>? onChanged;
  final List<TextInputFormatter>? inputFormatters;
  final Widget? prefixIcon;
  final Widget? prefixWidget;
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: common_dec(borderRadius: 8, boxShadow: containerShadow),
      margin: margin,
      padding: padding,
      child: TextFormField(
        style: style ?? size14_M_medium(),
        onTap: onTap,
        onFieldSubmitted: onFieldSubmitted,
        keyboardType: keyboardType,
        controller: controller,
        focusNode: focusNode,
        textInputAction: textInputAction,
        obscureText: obscureText ?? false,
        validator: validator,
        readOnly: readOnly,
        inputFormatters: inputFormatters,
        maxLines: maxLines,
        onChanged: onChanged,
        decoration: InputDecoration(
            isDense: true,
            prefixIcon: prefixIcon,
            prefixIconConstraints: const BoxConstraints(
                maxHeight: 44, minHeight: 44, maxWidth: 70, minWidth: 40),
            suffixIconConstraints: const BoxConstraints(
                maxHeight: 44, minHeight: 44, maxWidth: 70, minWidth: 40),
            border: kAuthInputBorder(),
            focusedBorder: kAuthInputBorder(color: const Color(0xff006BCC)),
            enabledBorder: kAuthInputBorder(),
            errorBorder: kAuthInputBorder(),
            disabledBorder: kAuthInputBorder(),
            focusedErrorBorder:
                kAuthInputBorder(color: const Color(0xff83B4CF)),
            fillColor: Colors.white,
            filled: true,
            errorStyle: size12_M_semibold(textColor: errorColor),
            hintText: hintText,
            // ignore: prefer_const_constructors
            hintStyle: size14_M_regular(textColor: Color(0xff909CA2)),
            contentPadding: contentPadding ??
                const EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            prefix: prefixWidget,
            suffixIcon: suffixIcon),
      ),
    );
  }
}
