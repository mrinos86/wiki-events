import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class CustomTextField extends StatelessWidget {
  TextEditingController textController;
  TextInputType keybordType;
  List<TextInputFormatter>? inputFormat;
  int? maxline;
  String labelText;
  String hintText;
  bool isError;
  Function onTapped;
  TextStyle? textStyle;
  FocusNode? focusNode;
  final ValueChanged<String>? onChanged;
  final TextInputAction? textInputAction;
  TextCapitalization? textCapitalization;

  CustomTextField({
    super.key,
    required this.textController,
    required this.keybordType,
    this.maxline = 1,
    this.inputFormat,
    this.labelText = "",
    this.hintText = "",
    required this.isError,
    required this.onTapped,
    this.textStyle,
    this.focusNode,
    this.onChanged,
    this.textInputAction,
    this.textCapitalization,
  });

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: textController,
      // autovalidateMode: AutovalidateMode.onUserInteraction,
      // keyboardType: keybordType,
      inputFormatters: inputFormat,
      keyboardType: TextInputType.multiline,
      maxLines: maxline,
      style: textStyle ?? size14_M_medium(),
      focusNode: focusNode,
      onChanged: onChanged,
      textInputAction: textInputAction ?? TextInputAction.next,
      textCapitalization: textCapitalization ?? TextCapitalization.none,
      decoration: InputDecoration(
        label: Text(labelText),
        hintText: hintText,
        hintStyle: size14_M_regular(textColor: colorGrey),
        labelStyle: size14_M_regular(textColor: colorGrey),
        border: OutlineInputBorder(
            borderSide: BorderSide(
              color: isError ? colorRed : colorGrey,
            ),
            borderRadius: BorderRadius.circular(8),
            gapPadding: 6.0),
        enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: isError ? colorRed : colorGrey,
            ),
            borderRadius: BorderRadius.circular(8),
            gapPadding: 6.0),
      ),
      onTap: () {
        onTapped();
      },
      onFieldSubmitted: (value) => FocusScope.of(context).unfocus(),
      onSaved: (value) => FocusScope.of(context).unfocus(),
    );
  }
}
