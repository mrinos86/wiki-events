import 'package:flutter/material.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class FeatureImageWidget extends StatelessWidget {
  double size;
  String imagePath;
  FeatureImageWidget({super.key, required this.size, required this.imagePath});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: size,
      child: Image(
        image: AssetImage(imagePath),
        fit: BoxFit.fitHeight,
      ),
    );
  }
}

// ignore: must_be_immutable
class EventFeatureImage extends StatelessWidget {
  double size;
  String imagePath;
  EventFeatureImage({super.key, required this.size, required this.imagePath});

  @override
  Widget build(BuildContext context) {
    return Image.network(
      imagePath,
      width: size,
      height: size,
      fit: BoxFit.cover,
      loadingBuilder: (BuildContext context, Widget child,
          ImageChunkEvent? loadingProgress) {
        if (loadingProgress == null) return child;
        return Center(
          child: CircularProgressIndicator(
            value: loadingProgress.expectedTotalBytes != null
                ? loadingProgress.cumulativeBytesLoaded /
                    loadingProgress.expectedTotalBytes!
                : null,
          ),
        );
      },
      errorBuilder: (BuildContext context, Object exception, stackTrace) {
        return Icon(
          Icons.image,
          color: liteGreyColor,
          size: size,
        );
      },
    );
  }
}

// ignore: must_be_immutable
class FeatureImageWithName extends StatelessWidget {
  String name;
  double imageSize;
  String imagePath;
  FeatureImageWithName(
      {super.key,
      required this.name,
      required this.imageSize,
      required this.imagePath});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(15),
      child: Column(
        children: [
          FeatureImageWidget(size: imageSize, imagePath: imagePath),
          sizedBoxHeight(8),
          Text(
            name,
            textAlign: TextAlign.center,
            style: size12_M_medium(),
          )
        ],
      ),
    );
  }
}
