import 'package:flutter/material.dart';
import 'package:share_plus/share_plus.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class EventDetailCard extends StatelessWidget {
  EventModel event;
  Function callBackViewEventButton;

  EventDetailCard({
    super.key,
    required this.event,
    required this.callBackViewEventButton,
  });

  Future<void> share(BuildContext context) async {
    final Size size = MediaQuery.of(context).size;
    await Share.share(
      "Event Details",
      subject: "Title",
      sharePositionOrigin: Rect.fromLTWH(0, 0, size.width, size.height / 2),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.center,
      child: Container(
        // TODO: need to check this line for ipad???
        constraints: const BoxConstraints(maxWidth: 400, minWidth: 200),
        padding: const EdgeInsets.all(6),
        decoration: common_dec(borderRadius: 10, boxShadow: containerShadow),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              padding: const EdgeInsets.all(4),
              decoration: common_dec(
                  borderRadius: 8,
                  color: liteGreyColor,
                  boxShadow: containerShadow),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 80,
                    width: 80,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(10),
                      child: NetworkImageWidget(
                        imageSize: 80,
                        imageUrl: event.images!.isNotEmpty
                            ? event.images!.first.url!
                            : "",
                      ),
                    ),
                  ),
                  sizedBoxWidth(10),
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Text(event.name!, style: size14_M_bold()),
                        sizedBoxHeight(5),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          children: [
                            const Icon(
                              Icons.star,
                              size: 20,
                              color: primaryColor,
                            ),
                            sizedBoxWidth(5),
                            Text(event.reviewRate!, style: size13_M_bold()),
                            Text(" (${event.reviewCount})",
                                style: size12_M_regular(textColor: grey495057)),
                          ],
                        ),
                        sizedBoxHeight(5),
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            const Icon(
                              Icons.location_on_outlined,
                              size: 20,
                              color: colorBlack,
                            ),
                            sizedBoxWidth(5),
                            Expanded(
                              child: Text(
                                event.location!,
                                style: size12_M_regular(textColor: grey495057),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            sizedBoxHeight(8),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Event Details",
                      style: size13_M_semiBold(textColor: grey495057),
                    ),
                    sizedBoxHeight(2),
                    Row(
                      children: [
                        NetworkImageWidget(
                          imageSize: 20,
                          imageUrl: event.eventTypes![0].imageUrl!,
                          loadErrorWidge: const Icon(
                            Icons.category,
                            color: Color(0xffadb5bd),
                            size: 20,
                          ),
                        ),
                        sizedBoxWidth(5),
                        Text(
                          event.eventTypes![0].name!,
                          style: size13_M_medium(textColor: grey495057),
                        ),
                      ],
                    ),
                    sizedBoxHeight(2),
                    Row(
                      children: [
                        const SizedBox(
                          height: 20,
                          width: 20,
                          child: Image(
                            image: AssetImage(AppAssets.EVENT_DISTANCE),
                            fit: BoxFit.fitWidth,
                          ),
                        ),
                        sizedBoxWidth(5),
                        Text(
                          event.distance!,
                          style: size13_M_medium(textColor: grey495057),
                        ),
                      ],
                    ),
                  ],
                ),
                Container(
                  // height: 85.h,
                  // width: 85.h,
                  padding: const EdgeInsets.fromLTRB(11, 5, 11, 8),
                  decoration: common_dec(borderRadius: 8, color: darkGreyColor),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(getDateValue(event.date!),
                          style: size18_M_bold(textColor: liteGreyColor)),
                      Text(event.date!.getMonthName()!,
                          style: size13_M_regular(
                              textColor: liteGreyColor.withOpacity(0.7))),
                      Text(event.startTime!,
                          style: size13_M_regular(textColor: liteGreyColor)),
                    ],
                  ),
                ),
              ],
            ),
            sizedBoxHeight(5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                SizedBox(
                  height: 25,
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      shrinkWrap: true,
                      itemCount: event.eventFeatures!.length > 6
                          ? 6
                          : event.eventFeatures!.length,
                      itemBuilder: (context, index) {
                        return Row(
                          children: [
                            sizedBoxWidth(6),
                            NetworkImageWidget(
                              imageSize: 25,
                              imageUrl: event.eventFeatures![index].imageUrl!,
                            ),
                          ],
                        );
                      }),
                ),
                Row(
                  children: [
                    Text("View More  ",
                        style: size14_M_regular(textColor: grey495057)),
                    const Icon(
                      Icons.navigate_next_outlined,
                      size: 22,
                    )
                  ],
                ),
              ],
            ),
            sizedBoxHeight(5),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Expanded(
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      minimumSize: const Size(0, 40),
                    ),
                    onPressed: () async {
                      callBackViewEventButton(event.id);
                    },
                    child: const Text('View Event'),
                  ),
                ),
                sizedBoxWidth(10),
                Row(
                  children: [
                    InkWell(
                      onTap: () {
                        share(context);
                      },
                      child: Container(
                        padding: const EdgeInsets.all(8),
                        decoration: common_border_dec(
                            borderRadius: 8, borderColor: darkGreyColor),
                        child: const Icon(
                          Icons.share,
                          color: darkGreyColor,
                          size: 20,
                        ),
                      ),
                    ),
                    sizedBoxWidth(10),
                    Container(
                      padding: const EdgeInsets.all(8),
                      decoration: common_border_dec(
                          color: event.amILiked! ? darkGreyColor : colorWhite,
                          borderRadius: 8,
                          borderColor: darkGreyColor),
                      child: Icon(
                        Icons.favorite,
                        color: event.amILiked! ? primaryColor : darkGreyColor,
                        size: 20,
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
