import 'package:flutter/material.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class AgeFilterWidget extends StatefulWidget {
  Function(AgeFilter) onSelected;
  AgeFilterWidget({super.key, required this.onSelected});

  @override
  State<AgeFilterWidget> createState() => _AgeFilterWidgetState();
}

class _AgeFilterWidgetState extends State<AgeFilterWidget> {
  AgeFilter selectedValue = AgeFilter.ageAll;

  onPressedAgeFilter(AgeFilter value) {
    setState(() {
      selectedValue = value;
    });

    widget.onSelected(value);
  }

  ButtonStyle selectedButtonStyle = ElevatedButton.styleFrom(
    backgroundColor: darkGreyColor,
    shape: RoundedRectangleBorder(
      side: const BorderSide(color: darkGreyColor),
      borderRadius: BorderRadius.circular(8),
    ),
    minimumSize: const Size(60, 10),
  );
  ButtonStyle unSelectedButtonStyle = ElevatedButton.styleFrom(
    shape: RoundedRectangleBorder(
      side: const BorderSide(color: darkGreyColor),
      borderRadius: BorderRadius.circular(8),
    ),
    backgroundColor: backgroundColor,
    minimumSize: const Size(60, 10),
  );

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      scrollDirection: Axis.horizontal,
      child: Row(
        children: [
          ElevatedButton(
            style: selectedValue == AgeFilter.ageAll
                ? selectedButtonStyle
                : unSelectedButtonStyle,
            onPressed: () {
              onPressedAgeFilter(AgeFilter.ageAll);
            },
            child: Text('All',
                style: size12_M_medium(
                  textColor: selectedValue == AgeFilter.ageAll
                      ? colorWhite
                      : colorBlack,
                )),
          ),
          sizedBoxWidth(12),
          ElevatedButton(
            style: selectedValue == AgeFilter.age18to24
                ? selectedButtonStyle
                : unSelectedButtonStyle,
            onPressed: () {
              onPressedAgeFilter(AgeFilter.age18to24);
            },
            child: Text('18 - 24',
                style: size12_M_medium(
                  textColor: selectedValue == AgeFilter.age18to24
                      ? colorWhite
                      : colorBlack,
                )),
          ),
          sizedBoxWidth(12),
          ElevatedButton(
            style: selectedValue == AgeFilter.age25to34
                ? selectedButtonStyle
                : unSelectedButtonStyle,
            onPressed: () {
              onPressedAgeFilter(AgeFilter.age25to34);
            },
            child: Text('25 - 34',
                style: size12_M_medium(
                  textColor: selectedValue == AgeFilter.age25to34
                      ? colorWhite
                      : colorBlack,
                )),
          ),
          sizedBoxWidth(12),
          ElevatedButton(
            style: selectedValue == AgeFilter.age35to44
                ? selectedButtonStyle
                : unSelectedButtonStyle,
            onPressed: () {
              onPressedAgeFilter(AgeFilter.age35to44);
            },
            child: Text('35 - 44',
                style: size12_M_medium(
                  textColor: selectedValue == AgeFilter.age35to44
                      ? colorWhite
                      : colorBlack,
                )),
          ),
          sizedBoxWidth(12),
          ElevatedButton(
            style: selectedValue == AgeFilter.age45to54
                ? selectedButtonStyle
                : unSelectedButtonStyle,
            onPressed: () {
              onPressedAgeFilter(AgeFilter.age45to54);
            },
            child: Text('45 - 54',
                style: size12_M_medium(
                  textColor: selectedValue == AgeFilter.age45to54
                      ? colorWhite
                      : colorBlack,
                )),
          ),
          sizedBoxWidth(12),
          ElevatedButton(
            style: selectedValue == AgeFilter.age55to64
                ? selectedButtonStyle
                : unSelectedButtonStyle,
            onPressed: () {
              onPressedAgeFilter(AgeFilter.age55to64);
            },
            child: Text('55 - 64',
                style: size12_M_medium(
                  textColor: selectedValue == AgeFilter.age55to64
                      ? colorWhite
                      : colorBlack,
                )),
          ),
          sizedBoxWidth(12),
          ElevatedButton(
            style: selectedValue == AgeFilter.age65Plus
                ? selectedButtonStyle
                : unSelectedButtonStyle,
            onPressed: () {
              onPressedAgeFilter(AgeFilter.age65Plus);
            },
            child: Text('65+',
                style: size12_M_medium(
                  textColor: selectedValue == AgeFilter.age65Plus
                      ? colorWhite
                      : colorBlack,
                )),
          ),
        ],
      ),
    );
  }
}

enum AgeFilter {
  ageAll,
  age18to24,
  age25to34,
  age35to44,
  age45to54,
  age55to64,
  age65Plus,
}
