import 'package:flutter/material.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class ReviewSummaryCard extends StatelessWidget {
  double rating;
  int reviewerCount;
  Function callBackViewReviews;
  ReviewSummaryCard({
    super.key,
    required this.rating,
    required this.reviewerCount,
    required this.callBackViewReviews,
  });

  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Text(
          rating.toString(),
          style: size12_M_medium(),
        ),
        sizedBoxWidth(10),
        starWidget(count: rating, iconSize: 18),
        sizedBoxWidth(10),
        InkWell(
          onTap: () {
            callBackViewReviews();
          },
          child: Text(
            "$reviewerCount Reviews",
            style: size12_M_medium(textColor: colorGrey),
          ),
        ),
      ],
    );
  }
}

Widget starWidget({required double count, required double iconSize}) {
  List<Widget> returnWidget = [];

  var data = count.toString().split(".");
  int fullValue = int.parse(data[0]);
  double decimalVale = double.parse(data[1]);

  // set stars for full rating
  for (int i = 0; i < fullValue; i++) {
    returnWidget.add(Icon(
      Icons.star,
      size: iconSize,
      color: yellowFFD045,
    ));
  }
  // set stars for decimal rating
  if (fullValue < 5 && decimalVale > 0) {
    returnWidget.add(Icon(
      Icons.star_half,
      size: iconSize,
      color: yellowFFD045,
    ));
  }

  // set stars for remaining rating
  if (count <= 4) {
    int value = 5 - returnWidget.length;
    for (int i = 0; i < value; i++) {
      returnWidget.add(Icon(
        Icons.star_border_outlined,
        size: iconSize,
        color: yellowFFD045,
      ));
    }
  }

  return Row(
      mainAxisAlignment: MainAxisAlignment.center, children: returnWidget);
}
