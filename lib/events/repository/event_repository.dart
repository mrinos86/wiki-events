import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/events/model/response_model/event_features_res_model.dart';
import 'package:wiki_events/events/model/response_model/event_res_model.dart';
import 'package:wiki_events/events/model/response_model/event_type_res_model.dart';

class EventRepsitory extends BaseApi {
  EventRepsitory._()
      : super(interceptors: [
          ClientKeyInterceptor(),
          PrettyDioLogger(),
          AccessTokenInterceptor()
        ]);

  static EventRepsitory? _instance;

  static EventRepsitory get instance {
    _instance ??= EventRepsitory._();
    return _instance!;
  }

  Future<ApiResponse<EventModel>?> getEvent({required int eventId}) async {
    try {
      Response response = await get('events/$eventId');
      EventResModel eventResModel = EventResModel.fromJson(response.data);
      if (eventResModel.result != null && eventResModel.result!) {
        if (eventResModel.event != null) {
          return ApiResponse<EventModel>(
            result: true,
            message: eventResModel.message ?? 'Success',
            payload: eventResModel.event,
          );
        }
      }
      return ApiResponse<EventModel>(
        result: false,
        message: eventResModel.message ?? 'Failed',
        payload: null,
      );
    } catch (e) {
      return ApiResponse<EventModel>(
        result: false,
        message: e.toString(),
        payload: null,
      );
    }
  }

  Future<ApiResponse<List<EventTypeModel>>?> getEventTypeList() async {
    try {
      Response response = await get('events/types/list');
      EventTypeResModel eventTypeResModel =
          EventTypeResModel.fromJson(response.data);
      if (eventTypeResModel.result != null && eventTypeResModel.result!) {
        if (eventTypeResModel.payload != null) {
          return ApiResponse<List<EventTypeModel>>(
            result: true,
            message: eventTypeResModel.message ?? 'Success',
            payload: eventTypeResModel.payload!.data!,
          );
        }
      }
      return ApiResponse<List<EventTypeModel>>(
        result: true,
        message: eventTypeResModel.message ?? 'Failed',
        payload: null,
      );
    } catch (e) {
      return ApiResponse<List<EventTypeModel>>(
        result: false,
        message: e.toString(),
        payload: null,
      );
    }
  }

  Future<ApiResponse<List<EventFeatureModel>>?> getEventFeaturesList() async {
    try {
      Response response = await get('events/features/list');
      EventFeaturesResModel eventFeaturesResModel =
          EventFeaturesResModel.fromJson(response.data);
      if (eventFeaturesResModel.result != null &&
          eventFeaturesResModel.result!) {
        if (eventFeaturesResModel.payload != null) {
          return ApiResponse<List<EventFeatureModel>>(
            result: true,
            message: eventFeaturesResModel.message ?? 'Success',
            payload: eventFeaturesResModel.payload!.data!,
          );
        }
      }
      return ApiResponse<List<EventFeatureModel>>(
        result: true,
        message: eventFeaturesResModel.message ?? 'Failed',
        payload: null,
      );
    } catch (e) {
      return ApiResponse<List<EventFeatureModel>>(
        result: false,
        message: e.toString(),
        payload: null,
      );
    }
  }
}
