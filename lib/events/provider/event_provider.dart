import 'package:flutter/material.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/events/model/selectable_model.dart';

class EventProvider with ChangeNotifier {
  EventProvider._() : super();

  static EventProvider? _instance;

  static EventProvider get instance {
    _instance ??= EventProvider._();
    return _instance!;
  }

  EventRepsitory eventRepsitory = EventRepsitory.instance;

  EventModel? event;
  List<SelectableEventTypeModel> eventTypeList = [];
  List<SelectableEventFeatureModel> eventFeatureList = [];

  bool isLoading = false;

  set setLoading(bool value) {
    isLoading = value;
    notifyListeners();
  }

  set setEventTypeList(List<SelectableEventTypeModel> list) {
    eventTypeList = list;
    notifyListeners();
  }

  set setEventFeatureList(List<SelectableEventFeatureModel> list) {
    eventFeatureList = list;
    notifyListeners();
  }

  Future<void> getViewEvent({required int eventId}) async {
    var response = await eventRepsitory.getEvent(eventId: eventId);
    event = response!.payload;
    notifyListeners();
  }

  Future<void> getEventTypes() async {
    var response = await eventRepsitory.getEventTypeList();
    List<EventTypeModel> typeList = response!.payload!;
    setEventTypeList = [];
    for (var element in typeList) {
      eventTypeList
          .add(SelectableEventTypeModel(eventType: element, isSelected: false));
    }
    setEventTypeList = eventTypeList;
    notifyListeners();
  }

  Future<void> getEventFeatures() async {
    var response = await eventRepsitory.getEventFeaturesList();
    List<EventFeatureModel> featureList = response!.payload!;
    setEventFeatureList = [];
    for (var element in featureList) {
      eventFeatureList.add(SelectableEventFeatureModel(
          eventFeature: element, isSelected: false));
    }
    setEventFeatureList = eventFeatureList;
    notifyListeners();
  }
}
