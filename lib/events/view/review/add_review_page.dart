import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/common/provider/image_picker.dart';
import 'package:wiki_events/utils/utils.dart';

class AddReviewPage extends StatefulWidget {
  const AddReviewPage({super.key});

  @override
  State<AddReviewPage> createState() => _AddReviewPageState();
}

class _AddReviewPageState extends State<AddReviewPage> {
  late TextEditingController _titleController;
  late TextEditingController _commentsController;

  int ratingValue = 1;

  DropdownItems? _currentSelectedValue;

  List<DropdownItems> dropdownItemList = [
    DropdownItems(id: 1, title: "18 - 24"),
    DropdownItems(id: 2, title: "25 - 34"),
    DropdownItems(id: 3, title: "35 - 44"),
    DropdownItems(id: 4, title: "45 - 54"),
    DropdownItems(id: 5, title: "55 - 64"),
    DropdownItems(id: 6, title: "65+"),
  ];

  List<String> pickedImages = [];

  bool isTitleError = false;
  bool isAgeError = false;

  @override
  void initState() {
    _titleController = TextEditingController();
    _commentsController = TextEditingController();

    super.initState();
  }

  @override
  void dispose() {
    _titleController.dispose();
    _commentsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Add Review'),
          leading: GestureDetector(
            child: const Icon(
              Icons.arrow_back_ios,
              size: 22,
            ),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              fillOverscroll: true,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                // width: double.infinity,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    sizedBoxHeight(ScreenSize.getHeight(context, 0.02)),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Review Title',
                          style: size14_M_regular(),
                        ),
                        sizedBoxHeight(10),
                        CustomTextField(
                          textController: _titleController,
                          isError: isTitleError,
                          keybordType: TextInputType.name,
                          textCapitalization: TextCapitalization.sentences,
                          onTapped: () {
                            setState(() {
                              isTitleError = false;
                            });
                          },
                          hintText: "",
                          labelText: "Type the title of your review",
                        ),
                      ],
                    ),
                    sizedBoxHeight(25),
                    Align(
                      alignment: Alignment.topLeft,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'What is your rate?',
                            style: size14_M_regular(),
                          ),
                          sizedBoxHeight(12),
                          RatingBar.builder(
                            initialRating: 1,
                            minRating: 1,
                            direction: Axis.horizontal,
                            // allowHalfRating: true,
                            itemCount: 5,
                            itemPadding:
                                const EdgeInsets.symmetric(horizontal: 2),
                            itemBuilder: (context, _) => const Icon(
                              Icons.star_outlined,
                              color: yellowFFD045,
                              size: 15,
                            ),
                            onRatingUpdate: (rating) {
                              setState(() {
                                // ratingValue = int.parse(rating.toString());
                              });
                            },
                          ),
                        ],
                      ),
                    ),
                    sizedBoxHeight(25),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Age Bracket',
                          style: size14_M_regular(),
                        ),
                        sizedBoxHeight(10),
                        CommonDropdown(
                          dropdownList: dropdownItemList,
                          currentItem: _currentSelectedValue,
                          dropdownMenuHeight: 250,
                          hintText: "Select Category",
                          onChangedCallBack: (value) {
                            setState(() {
                              _currentSelectedValue = value;
                              isAgeError = false;
                            });
                          },
                          isError: isAgeError,
                        ),
                      ],
                    ),
                    sizedBoxHeight(25),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Your Comments',
                          style: size14_M_regular(),
                        ),
                        sizedBoxHeight(8),
                        TextFormField(
                          controller: _commentsController,
                          maxLines: 4,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          keyboardType: TextInputType.text,
                          style: size14_M_medium(),
                          textCapitalization: TextCapitalization.sentences,
                          decoration: InputDecoration(
                            alignLabelWithHint: true,
                            label: const Text(
                              "Type your comment",
                              textAlign: TextAlign.start,
                            ),
                            hintText: "",
                            border: OutlineInputBorder(
                                borderSide: const BorderSide(color: colorGrey),
                                borderRadius: BorderRadius.circular(8)),
                            enabledBorder: OutlineInputBorder(
                                borderSide: const BorderSide(color: colorGrey),
                                borderRadius: BorderRadius.circular(8)),
                          ),
                          onFieldSubmitted: (value) =>
                              FocusScope.of(context).unfocus(),
                          onSaved: (value) => FocusScope.of(context).unfocus(),
                        ),
                      ],
                    ),
                    sizedBoxHeight(25),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          'Upload images',
                          style: size14_M_regular(),
                        ),
                        sizedBoxHeight(15),
                        SizedBox(
                          height: 90,
                          child: Row(
                            children: [
                              GestureDetector(
                                onTap: () async {
                                  if (FocusScope.of(context).hasFocus) {
                                    FocusScope.of(context).unfocus();
                                    await Future.delayed(
                                        const Duration(milliseconds: 400));
                                  }
                                  // ignore: use_build_context_synchronously
                                  await _pickImagess(context);
                                },
                                child: DottedBorder(
                                  radius: const Radius.circular(12),
                                  strokeWidth: 2,
                                  dashPattern: const [8, 3],
                                  borderType: BorderType.RRect,
                                  color: Theme.of(context).primaryColor,
                                  child: Container(
                                    color: colorWhite,
                                    height: 90,
                                    width: 90,
                                    child: const Icon(
                                      Icons.file_upload_outlined,
                                      size: 35,
                                      color: primaryColor,
                                    ),
                                  ),
                                ),
                              ),
                              pickedImages.isNotEmpty
                                  ? Expanded(
                                      child: ListView.builder(
                                        scrollDirection: Axis.horizontal,
                                        itemCount: pickedImages.length,
                                        shrinkWrap: true,
                                        itemBuilder: (context, index) {
                                          return GestureDetector(
                                            onTap: () {
                                              setState(() {
                                                pickedImages.removeAt(index);
                                              });
                                            },
                                            child: Padding(
                                              padding: const EdgeInsets.only(
                                                  left: 10),
                                              child: DottedBorder(
                                                radius:
                                                    const Radius.circular(10),
                                                strokeWidth: 2,
                                                dashPattern: const [8, 3],
                                                borderType: BorderType.RRect,
                                                color: Theme.of(context)
                                                    .primaryColor,
                                                child: Container(
                                                  color: colorWhite,
                                                  height: 90,
                                                  width: 90,
                                                  child: Stack(
                                                    children: [
                                                      ClipRRect(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(10),
                                                        child: Image.file(
                                                          File(pickedImages[
                                                              index]),
                                                          height: 90,
                                                          width: 90,
                                                          fit: BoxFit.cover,
                                                        ),
                                                      ),
                                                      const Align(
                                                        alignment:
                                                            Alignment.center,
                                                        child: Icon(
                                                          Icons
                                                              .remove_circle_outline,
                                                          size: 35,
                                                          color: Colors.red,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          );
                                        },
                                      ),
                                    )
                                  : Container()
                            ],
                          ),
                        ),
                      ],
                    ),
                    sizedBoxHeight(60),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize:
                            Size(ScreenSize.getWidth(context, 0.6), 48),
                      ),
                      onPressed: () async {
                        onPressedSaveButton();
                      },
                      child: const Text('Save'),
                    ),
                    sizedBoxHeight(50),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _pickImagess(BuildContext context) async {
    String? imagePath = await ImageUtil().getImage(
      context: context,
    );
    if (imagePath != null && imagePath.isNotEmpty) {
      debugPrint(imagePath.toString());
      setState(() {
        pickedImages.add(imagePath);
      });
    }
  }

  onPressedSaveButton() async {
    String message = "";

    if (_currentSelectedValue == null) {
      setState(() {
        isAgeError = true;
        message = "Please select any age value";
      });
    }
    if (_titleController.text.isEmpty) {
      setState(() {
        isTitleError = true;
        message = "Please enter review title";
      });
    }

    if (_currentSelectedValue != null && _titleController.text.isNotEmpty) {
      Navigator.of(context).pop();
    } else {
      await showOKDialog(
        context: context,
        labelTitle: "Error !",
        labelContent: message,
      );
    }
  }
}

class DropdownItems {
  DropdownItems({
    required this.id,
    required this.title,
  });
  int id;
  String title;
}
