import 'package:flutter/material.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class ViewReviewsPage extends StatefulWidget {
  const ViewReviewsPage({
    super.key,
  });

  @override
  State<ViewReviewsPage> createState() => _ViewReviewsPageState();
}

List testReviewList = [
  [1, 70],
  [2, 10],
  [3, 45],
  [4, 90],
  [5, 05]
];

class _ViewReviewsPageState extends State<ViewReviewsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text('Review and Rating'),
        leading: GestureDetector(
          child: const Icon(
            Icons.arrow_back_ios,
            size: 22,
          ),
          onTap: () {
            Navigator.of(context).pop();
          },
        ),
        actions: [
          GestureDetector(
            child: const Padding(
              padding: EdgeInsets.only(right: 12),
              child: Icon(
                Icons.add,
                size: 28,
              ),
            ),
            onTap: () {
              Pages.addReview.push(context);
            },
          ),
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "44",
              style: size44_M_bold(),
            ),
            starWidget(count: 4, iconSize: 24),
            sizedBoxHeight(5),
            ListView.builder(
              padding: const EdgeInsets.only(left: 10, right: 5),
              shrinkWrap: true,
              itemCount: testReviewList.length,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: Row(
                    children: [
                      Text(testReviewList[index][0].toString(),
                          style: size14_M_semibold()),
                      sizedBoxWidth(12),
                      Expanded(
                        child: StepProgressIndicator(
                          totalSteps: 100,
                          currentStep: testReviewList[index][1],
                          size: 8,
                          padding: 0,
                          selectedColor: yellowFFD045,
                          unselectedColor: colorGrey.withOpacity(0.5),
                          roundedEdges: const Radius.circular(10),
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
            sizedBoxHeight(12),
            AgeFilterWidget(
              onSelected: (value) {
                debugPrint(value.toString());
              },
            ),
            sizedBoxHeight(15),
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: 3,
                  itemBuilder: (context, index) {
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 35),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  const RoundedBorderBoxImage(
                                    imageSize: 22,
                                    // profileUrl: null,
                                    imageUrl:
                                        "https://img.freepik.com/free-photo/young-bearded-man-with-striped-shirt_273609-5677.jpg",
                                  ),
                                  sizedBoxWidth(12),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Text("John Doe",
                                          style: size12_M_medium()),
                                      sizedBoxHeight(3),
                                      starWidget(count: 5, iconSize: 13)
                                    ],
                                  ),
                                ],
                              ),
                              Text("2023/02/12",
                                  style: size12_M_medium(textColor: colorGrey)),
                            ],
                          ),
                          sizedBoxHeight(15),
                          Text(
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam",
                            style: size12_M_medium(),
                          ),
                          sizedBoxHeight(12),
                          SizedBox(
                            height: 70,
                            child: ListView.builder(
                                scrollDirection: Axis.horizontal,
                                shrinkWrap: true,
                                itemCount: 5,
                                itemBuilder: (context, index) {
                                  return Padding(
                                    padding: const EdgeInsets.only(right: 12),
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(10),
                                      child: const Image(
                                        image: AssetImage(
                                            'assets/images/image2.JPG'),
                                        fit: BoxFit.fitHeight,
                                        height: 70,
                                        width: 70,
                                      ),
                                    ),
                                  );
                                }),
                          )
                        ],
                      ),
                    );
                  }),
            ),
          ],
        ),
      ),
    );
  }
}
