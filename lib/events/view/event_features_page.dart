import 'package:flutter/material.dart';
import 'package:wiki_events/app/app.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/events/model/selectable_model.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class EventFeaturesPage extends StatefulWidget {
  List<dynamic> selectedFeaureList;
  EventFeaturesPage({super.key, required this.selectedFeaureList});

  @override
  State<EventFeaturesPage> createState() => _EventFeaturesPageState();
}

class _EventFeaturesPageState extends State<EventFeaturesPage> {
  late EventProvider eventProvider;

  List<SelectableEventFeatureModel> eventFeatureList = [];

  @override
  void initState() {
    loadEventFeatures();
    super.initState();
  }

  void setSelectedEventFeatures(EventProvider provider) {
    if (widget.selectedFeaureList.isNotEmpty) {
      for (var element in eventProvider.eventFeatureList) {
        for (var selectItem in widget.selectedFeaureList) {
          if (selectItem.id == element.eventFeature.id) {
            int index = eventProvider.eventFeatureList.indexOf(element);
            eventProvider.eventFeatureList[index].isSelected = true;
          }
        }
      }
      provider.eventFeatureList = provider.eventFeatureList;
    }
  }

  Future<void> loadEventFeatures() async {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      eventProvider = Provider.of<EventProvider>(context, listen: false);
      eventProvider.setLoading = true;
      await eventProvider.getEventFeatures();
      if (eventProvider.eventFeatureList.isNotEmpty) {
        setSelectedEventFeatures(eventProvider);
      }
      eventProvider.setLoading = false;
    });
  }

  onPressedBackButton() {
    List<EventFeatureModel> returnEventFeatureList = [];
    if (eventFeatureList.isNotEmpty) {
      for (var element in eventFeatureList) {
        if (element.isSelected) {
          returnEventFeatureList.add(element.eventFeature);
        }
      }
    }
    Navigator.of(context).pop(returnEventFeatureList);
  }

  @override
  Widget build(BuildContext context) {
    eventProvider = Provider.of<EventProvider>(context, listen: true);
    eventFeatureList = eventProvider.eventFeatureList;
    return Stack(
      children: [
        Scaffold(
          backgroundColor: backgroundColor,
          appBar: AppBar(
            title: const Text('Event Feature'),
            leading: GestureDetector(
              onTap: onPressedBackButton,
              child: const Icon(
                Icons.arrow_back_ios,
                size: 22,
              ),
            ),
          ),
          body: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                sizedBoxHeight(20),
                Expanded(
                  child: GridView.builder(
                      shrinkWrap: true,
                      itemCount: eventFeatureList.length,
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        // mainAxisSpacing: 8.h,
                        mainAxisExtent: 110,
                        crossAxisSpacing: 0,
                        childAspectRatio: 1.6,
                        crossAxisCount: 2,
                      ),
                      itemBuilder: (_, index) {
                        return Container(
                          decoration: BoxDecoration(
                            border: Border(
                              top: BorderSide(
                                color: index == 0 || index == 1
                                    ? Colors.transparent
                                    : Colors.grey,
                              ),
                              right: BorderSide(
                                color: index % 2 != 1
                                    ? Colors.grey
                                    : Colors.transparent,
                              ),
                            ),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(12),
                            child: Stack(
                              children: [
                                Align(
                                  alignment: Alignment.center,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      sizedBoxHeight(6),
                                      EventFeatureImage(
                                          size: 35,
                                          imagePath: eventFeatureList[index]
                                              .eventFeature
                                              .imageUrl!),
                                      sizedBoxHeight(8),
                                      Expanded(
                                        child: Text(
                                            eventFeatureList[index]
                                                .eventFeature
                                                .name!,
                                            textAlign: TextAlign.center,
                                            style: size12_M_medium()),
                                      )
                                    ],
                                  ),
                                ),
                                Align(
                                  alignment: Alignment.topRight,
                                  child: Transform.scale(
                                    scale: 1,
                                    child: Checkbox(
                                      visualDensity: const VisualDensity(
                                          vertical:
                                              VisualDensity.minimumDensity,
                                          horizontal:
                                              VisualDensity.minimumDensity),
                                      activeColor: colorBlack,
                                      value: eventFeatureList[index].isSelected,
                                      onChanged: (value) {
                                        eventFeatureList[index].isSelected =
                                            value!;
                                        eventProvider.setEventFeatureList =
                                            eventFeatureList;
                                      },
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        );
                      }),
                ),
              ],
            ),
          ),
        ),
        eventProvider.isLoading ? const OnPageLoader() : Container()
      ],
    );
  }
}
