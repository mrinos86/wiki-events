import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wiki_events/common/widgets/network_image_widget.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';
import 'dart:ui' as ui;
import 'dart:math' as math;

class ViewEventPageParam {
  ViewEventFrom viewEventFrom;
  int? eventId;
  ViewEventPageParam({required this.viewEventFrom, this.eventId});
}

// ignore: must_be_immutable
class ViewEventPage extends StatefulWidget {
  ViewEventPageParam param;
  ViewEventPage({
    required this.param,
    super.key,
  });

  @override
  State<ViewEventPage> createState() => _ViewEventPageState();
}

class _ViewEventPageState extends State<ViewEventPage> {
  int selectedIndex = 0;
  PageController controller = PageController();

  late GoogleMapController googleMapController;

  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 4,
  );
  late BitmapDescriptor markerCustomIcon = BitmapDescriptor.defaultMarker;

  late EventProvider eventProvider;

  EventModel? viewEvent;
  // TODO: remove this, when you implemented all view events
  bool showRealData = false;

  @override
  void initState() {
    if (widget.param.eventId != null) {
      setState(() => showRealData = true);
    }
    loadEvent();
    super.initState();
  }

  Future<void> loadEvent() async {
    if (showRealData) {
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        eventProvider = Provider.of<EventProvider>(context, listen: false);
        eventProvider.setLoading = true;
        await eventProvider.getViewEvent(eventId: widget.param.eventId!);
        eventProvider.setLoading = false;
      });
    }
    addCustomIcon();
  }

  Future<void> addCustomIcon() async {
    final Uint8List markerIcon =
        await getBytesFromAsset('assets/images/place.png', 100);
    setState(() {
      markerCustomIcon = BitmapDescriptor.fromBytes(markerIcon);
    });
  }

  // for marker icon
  Future<Uint8List> getBytesFromAsset(String path, int width) async {
    ByteData data = await rootBundle.load(path);
    ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(),
        targetWidth: width);
    ui.FrameInfo fi = await codec.getNextFrame();
    return (await fi.image.toByteData(format: ui.ImageByteFormat.png))!
        .buffer
        .asUint8List();
  }

  @override
  Widget build(BuildContext context) {
    if (showRealData) {
      eventProvider = Provider.of<EventProvider>(context, listen: true);
      viewEvent = eventProvider.event;
    }
    return Stack(
      children: [
        Scaffold(
          backgroundColor: backgroundColor,
          appBar: AppBar(
            title: const Text(''),
            leading: GestureDetector(
              child: const Icon(
                Icons.arrow_back_ios,
                size: 22,
              ),
              onTap: () {
                Navigator.of(context).pop();
              },
            ),
          ),
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Column(
                          children: [
                            SizedBox(
                                height: MediaQuery.of(context).size.width,
                                child: Stack(
                                  children: [
                                    PageView.builder(
                                        itemCount: showRealData &&
                                                viewEvent != null &&
                                                viewEvent!.images != null &&
                                                viewEvent!.images!.isNotEmpty
                                            ? viewEvent!.images!.length
                                            : 3,
                                        scrollDirection: Axis.horizontal,
                                        controller: controller,
                                        onPageChanged: (value) {
                                          setState(() {
                                            selectedIndex = value;
                                          });
                                        },
                                        itemBuilder: (context, index) {
                                          return showRealData &&
                                                  viewEvent != null &&
                                                  viewEvent!.images != null &&
                                                  viewEvent!.images!.isNotEmpty
                                              ? Container(
                                                  color: backgroundColor,
                                                  child: ClipRRect(
                                                    borderRadius:
                                                        BorderRadius.circular(
                                                            22),
                                                    child: NetworkImageWidget(
                                                      imageSize:
                                                          MediaQuery.of(context)
                                                                  .size
                                                                  .width -
                                                              50,
                                                      imageUrl: viewEvent!
                                                              .images!
                                                              .isNotEmpty
                                                          ? viewEvent!
                                                              .images![index]
                                                              .url!
                                                          : "",
                                                    ),
                                                  ),
                                                )
                                              : ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(22),
                                                  child: const Image(
                                                    image: AssetImage(
                                                        'assets/images/image2.JPG'),
                                                    fit: BoxFit.cover,
                                                  ),
                                                );
                                        }),
                                    Align(
                                      alignment: Alignment.bottomCenter,
                                      child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: _buildPageIndicator()),
                                    ),
                                  ],
                                )),
                            sizedBoxHeight(15),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        showRealData && viewEvent != null
                                            ? viewEvent!.name!
                                            : "BackStreet boys",
                                        style: size18_M_bold(),
                                      ),
                                      sizedBoxHeight(6),
                                      ReviewSummaryCard(
                                        rating:
                                            showRealData && viewEvent != null
                                                ? double.parse(
                                                    viewEvent!.reviewRate!)
                                                : 3.5,
                                        reviewerCount:
                                            showRealData && viewEvent != null
                                                ? viewEvent!.reviewCount!
                                                : 20,
                                        callBackViewReviews: () {
                                          Pages.viewReview.push(context);
                                        },
                                      ),
                                    ],
                                  ),
                                ),
                                sizedBoxWidth(10),
                                Row(
                                  children: [
                                    Container(
                                      padding: const EdgeInsets.all(10),
                                      decoration: common_border_dec(
                                          borderRadius: 10,
                                          borderColor: darkGreyColor),
                                      child: const Icon(
                                        Icons.share,
                                        color: darkGreyColor,
                                        size: 28,
                                      ),
                                    ),
                                    sizedBoxWidth(10),
                                    Container(
                                      padding: const EdgeInsets.all(10),
                                      decoration: common_border_dec(
                                          borderRadius: 10,
                                          borderColor: darkGreyColor),
                                      child: const Icon(
                                        Icons.favorite,
                                        color: darkGreyColor,
                                        size: 28,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            sizedBoxHeight(20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Event Details",
                                  style: size14_M_semibold(),
                                ),
                                sizedBoxHeight(12),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Row(
                                          children: [
                                            showRealData && viewEvent != null
                                                ? NetworkImageWidget(
                                                    imageSize: 20,
                                                    imageUrl: viewEvent!
                                                        .eventTypes![0]
                                                        .imageUrl!,
                                                    loadErrorWidge: const Icon(
                                                      Icons.category,
                                                      color: Color(0xffadb5bd),
                                                      size: 20,
                                                    ),
                                                  )
                                                : const Icon(
                                                    Icons.category,
                                                    color: Color(0xffadb5bd),
                                                    size: 20,
                                                  ),
                                            sizedBoxWidth(8),
                                            Text(
                                              showRealData && viewEvent != null
                                                  ? viewEvent!
                                                      .eventTypes![0].name!
                                                  : "Sports & Fitness",
                                              style: size13_M_medium(
                                                  textColor: grey495057),
                                            ),
                                          ],
                                        ),
                                        sizedBoxHeight(5),
                                        Row(
                                          children: [
                                            const SizedBox(
                                              height: 20,
                                              width: 20,
                                              child: Image(
                                                image: AssetImage(
                                                    AppAssets.EVENT_DISTANCE),
                                                fit: BoxFit.fitWidth,
                                              ),
                                            ),
                                            sizedBoxWidth(8),
                                            Text(
                                              "10.5KM",
                                              style: size13_M_medium(
                                                  textColor: grey495057),
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(right: 40.0),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Row(
                                            children: [
                                              const Icon(
                                                Icons.calendar_month,
                                                color: Color(0xffadb5bd),
                                                size: 20,
                                              ),
                                              sizedBoxWidth(8),
                                              Text(
                                                showRealData &&
                                                        viewEvent != null
                                                    ? viewEvent!.date!
                                                    : "23/03/2023",
                                                style: size13_M_medium(
                                                    textColor: grey495057),
                                              ),
                                            ],
                                          ),
                                          sizedBoxHeight(5),
                                          Row(
                                            children: [
                                              const Icon(
                                                Icons.access_time_filled,
                                                color: Color(0xffadb5bd),
                                                size: 20,
                                              ),
                                              sizedBoxWidth(8),
                                              Text(
                                                showRealData &&
                                                        viewEvent != null
                                                    ? viewEvent!.startTime!
                                                    : "10:30 PM",
                                                style: size13_M_medium(
                                                    textColor: grey495057),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            sizedBoxHeight(20),
                            Align(
                              alignment: Alignment.centerLeft,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: [
                                  Text(
                                    "Event Description",
                                    style: size14_M_semibold(),
                                  ),
                                  sizedBoxHeight(12),
                                  Text(
                                    showRealData && viewEvent != null
                                        ? viewEvent!.description!
                                        : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
                                    style: size12_M_medium(),
                                  ),
                                ],
                              ),
                            ),
                            sizedBoxHeight(20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Event Features",
                                  style: size14_M_semibold(),
                                ),
                                sizedBoxHeight(12),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    showRealData && viewEvent != null
                                        ? SizedBox(
                                            height: 36,
                                            child: ListView.builder(
                                                scrollDirection:
                                                    Axis.horizontal,
                                                shrinkWrap: true,
                                                itemCount: viewEvent!
                                                            .eventFeatures!
                                                            .length >
                                                        7
                                                    ? 7
                                                    : viewEvent!
                                                        .eventFeatures!.length,
                                                itemBuilder: (context, index) {
                                                  return Row(
                                                    children: [
                                                      sizedBoxWidth(6),
                                                      NetworkImageWidget(
                                                        imageSize: 36,
                                                        imageUrl: viewEvent!
                                                            .eventFeatures![
                                                                index]
                                                            .imageUrl!,
                                                      ),
                                                    ],
                                                  );
                                                }),
                                          )
                                        : Row(
                                            children: [
                                              FeatureImageWidget(
                                                  size: 36,
                                                  imagePath: AppAssets
                                                      .FEATURE_BOOKING_REQUIRED),
                                              sizedBoxWidth(8),
                                              FeatureImageWidget(
                                                  size: 36,
                                                  imagePath: AppAssets
                                                      .FEATURE_PARKING),
                                              sizedBoxWidth(8),
                                              FeatureImageWidget(
                                                  size: 36,
                                                  imagePath: AppAssets
                                                      .FEATURE_FAMILY_FRIENDLY),
                                              sizedBoxWidth(8),
                                              FeatureImageWidget(
                                                  size: 36,
                                                  imagePath: AppAssets
                                                      .FEATURE_DOG_ALLOWED),
                                              sizedBoxWidth(8),
                                              FeatureImageWidget(
                                                  size: 36,
                                                  imagePath: AppAssets
                                                      .FEATURE_BYO_DRINKS),
                                              sizedBoxWidth(8),
                                              FeatureImageWidget(
                                                  size: 36,
                                                  imagePath: AppAssets
                                                      .FEATURE_FOOD_STALL),
                                              sizedBoxWidth(8),
                                              FeatureImageWidget(
                                                  size: 36,
                                                  imagePath: AppAssets
                                                      .FEATURE_WIFI_AVAILABLE),
                                              sizedBoxWidth(8),
                                            ],
                                          ),
                                    InkWell(
                                      onTap: () {
                                        Pages.viewEventFeatures.push(context);
                                      },
                                      child: Transform.rotate(
                                        angle: 270 * math.pi / 180,
                                        child: const Icon(
                                            Icons.expand_circle_down_outlined,
                                            size: 35,
                                            color: colorGrey),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                            sizedBoxHeight(20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Information and Tickets",
                                  style: size14_M_semibold(),
                                ),
                                sizedBoxHeight(12),
                                OutlinedButton(
                                  style: ElevatedButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                  ),
                                  onPressed: () {},
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'Tickets',
                                        style: size14_M_regular(
                                            textColor: darkGreyColor),
                                      ),
                                      const SizedBox(
                                        height: 23,
                                        width: 23,
                                        child: Image(
                                          image: AssetImage(
                                              'assets/images/ticket.png'),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                sizedBoxHeight(8),
                                OutlinedButton(
                                  style: ElevatedButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                  ),
                                  onPressed: () {},
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'More Info',
                                        style: size14_M_regular(
                                            textColor: darkGreyColor),
                                      ),
                                      const Icon(
                                        Icons.open_in_new,
                                        size: 26,
                                        color: colorBlack,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                            sizedBoxHeight(20),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  "Venue Details",
                                  style: size14_M_semibold(),
                                ),
                                sizedBoxHeight(12),
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        const Icon(
                                          Icons.location_on_sharp,
                                          size: 20,
                                          color: colorGrey,
                                        ),
                                        sizedBoxWidth(8),
                                        Expanded(
                                          child: Text(
                                            showRealData && viewEvent != null
                                                ? viewEvent!.location!
                                                : "Max Ground, 134, Clayton Road, Clayton",
                                            style: size12_M_semibold(
                                                textColor: primaryColor),
                                          ),
                                        ),
                                      ],
                                    ),
                                    sizedBoxHeight(5),
                                    ReviewSummaryCard(
                                      rating: 4.4,
                                      reviewerCount: 19,
                                      callBackViewReviews: () {
                                        Pages.viewReview.push(context);
                                      },
                                    )
                                  ],
                                ),
                              ],
                            ),
                          ],
                        ),
                      ),
                      sizedBoxHeight(20),
                      SizedBox(
                        height: 280,
                        child: Stack(
                          children: [
                            GoogleMap(
                              mapType: MapType.normal,
                              markers: {
                                Marker(
                                  markerId: const MarkerId("i"),
                                  position: showRealData && viewEvent != null
                                      ? LatLng(
                                          double.parse(viewEvent!.latitude!),
                                          double.parse(viewEvent!.longitude!))
                                      : const LatLng(
                                          37.42796133580664, -122.085749655962),
                                  icon: markerCustomIcon,
                                  draggable: true,
                                )
                              },
                              initialCameraPosition: showRealData &&
                                      viewEvent != null
                                  ? CameraPosition(
                                      target: LatLng(
                                          double.parse(viewEvent!.latitude!),
                                          double.parse(viewEvent!.longitude!)))
                                  : _kGooglePlex,
                              myLocationEnabled: true,
                              myLocationButtonEnabled: false,
                              zoomControlsEnabled: false,
                            ),
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Padding(
                                padding: const EdgeInsets.all(8),
                                child: OutlinedButton(
                                  style: ElevatedButton.styleFrom(
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(8),
                                    ),
                                  ),
                                  onPressed: () {},
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        'More Events from this venue',
                                        style: size14_M_regular(
                                            textColor: darkGreyColor),
                                      ),
                                      const Icon(
                                        Icons.calendar_today_outlined,
                                        size: 26,
                                        color: darkGreyColor,
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      sizedBoxHeight(12),
                      Padding(
                        padding: const EdgeInsets.all(12),
                        child: Column(
                          children: [
                            Container(
                              decoration: common_dec(
                                borderRadius: 12,
                                color:
                                    const Color(0xfff99f39).withOpacity(0.15),
                              ),
                              padding: const EdgeInsets.all(8),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "23 °C",
                                        style: size21_M_medium(),
                                      ),
                                      sizedBoxHeight(6),
                                      Row(
                                        children: [
                                          Text(
                                            "Clayton  ",
                                            style: size16_M_light(),
                                          ),
                                          Text(
                                            " 9:41 AM",
                                            style: size14_M_light(),
                                          ),
                                        ],
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 75,
                                    width: 75,
                                    child: ClipRRect(
                                      borderRadius: BorderRadius.circular(22),
                                      child: const Image(
                                        image: AssetImage(
                                            'assets/images/clouds-sun.png'),
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            sizedBoxHeight(30),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                ElevatedButton(
                                  style: ElevatedButton.styleFrom(
                                    minimumSize: Size(
                                        ScreenSize.getWidth(context, 0.6), 48),
                                  ),
                                  onPressed: () async {
                                    Pages.eventForum.push(context);
                                  },
                                  child: const Text('Event Forum'),
                                ),
                              ],
                            ),
                            sizedBoxHeight(15),
                            widget.param.viewEventFrom == ViewEventFrom.explore
                                ? OutlinedButton(
                                    style: ElevatedButton.styleFrom(
                                      minimumSize: Size(
                                          ScreenSize.getWidth(context, 0.6),
                                          48),
                                    ),
                                    onPressed: () {
                                      Pages.reportEvent.push(context);
                                    },
                                    child: const Text('Report This Event'),
                                  )
                                : widget.param.viewEventFrom ==
                                        ViewEventFrom.myEvents
                                    ? Column(
                                        children: [
                                          OutlinedButton(
                                            style: ElevatedButton.styleFrom(
                                              minimumSize: Size(
                                                  ScreenSize.getWidth(
                                                      context, 0.6),
                                                  48),
                                            ),
                                            onPressed: () {
                                              Pages.editEvent.push(context);
                                            },
                                            child: const Text('Edit Event'),
                                          ),
                                          sizedBoxHeight(5),
                                          TextButton(
                                            onPressed: () {},
                                            child: Text('Event History',
                                                style: size14_M_medium(
                                                        textColor: primaryColor)
                                                    .copyWith(
                                                  decoration:
                                                      TextDecoration.underline,
                                                  decorationColor: primaryColor,
                                                  decorationThickness: 2,
                                                )),
                                          ),
                                        ],
                                      )
                                    : Container(),
                          ],
                        ),
                      ),
                      sizedBoxHeight(20),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        showRealData && eventProvider.isLoading
            ? const OnPageLoader()
            : Container()
      ],
    );
  }

  List<Widget> _buildPageIndicator() {
    List<Widget> list = [];
    for (int i = 0;
        i < (showRealData && viewEvent != null ? viewEvent!.images!.length : 3);
        i++) {
      list.add(i == selectedIndex ? _indicator(true) : _indicator(false));
    }
    return list;
  }

  Widget _indicator(bool isActive) {
    return Padding(
      padding: const EdgeInsets.all(5),
      child: SizedBox(
        height: 30,
        width: 20,
        child: AnimatedContainer(
          duration: const Duration(milliseconds: 100),
          margin: const EdgeInsets.symmetric(horizontal: 3),
          height: 12,
          width: 12,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              border: Border.all(color: colorBlack),
              color: isActive ? colorWhite : Colors.transparent),
        ),
      ),
    );
  }
}
