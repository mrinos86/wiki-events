import 'package:flutter/material.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/utils/utils.dart';

class ViewEventFeaturesPage extends StatefulWidget {
  const ViewEventFeaturesPage({super.key});

  @override
  State<ViewEventFeaturesPage> createState() => _ViewEventFeaturesPageState();
}

class _ViewEventFeaturesPageState extends State<ViewEventFeaturesPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: backgroundColor,
      appBar: AppBar(
        title: const Text('Event Feature Description'),
        leading: GestureDetector(
          child: const Icon(
            Icons.arrow_back_ios,
            size: 22,
          ),
          onTap: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(12, 12, 12, 40),
                child: Table(
                  border: TableBorder.symmetric(
                    outside: BorderSide.none,
                    inside: const BorderSide(
                        width: 0.8,
                        color: Colors.grey,
                        style: BorderStyle.solid),
                  ),
                  children: [
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Free",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_FREE,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Donation",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_DONATION,
                      )),
                    ]),
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Tickets / Booking Required",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_BOOKING_REQUIRED,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Free Parking",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_PARKING,
                      )),
                    ]),
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Paid Parking",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_PAID_PARKING,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Family Friendly",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_FAMILY_FRIENDLY,
                      )),
                    ]),
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Kids Activities",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_KIDS_ACTIVITY,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Age 18+",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_AGE_18,
                      )),
                    ]),
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Indigenous Culture",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_INDIGENOUS_CULTURE,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Catering / Food Stalls",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_FOOD_STALL,
                      )),
                    ]),
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "BYO Food",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_BYO_FOODS,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "BYODrinks",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_BYO_DRINKS,
                      )),
                    ]),
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Licenced",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_LICENCED,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "EFTPOS / ATM Available",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_ATM,
                      )),
                    ]),
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Dogs Allowed",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_DOG_ALLOWED,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Dogs Not Allowed",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_DOG_NOT_ALLOWED,
                      )),
                    ]),
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Toilets",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_TOILETS,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Wheekchair Accessible",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_WHEELCHAIR,
                      )),
                    ]),
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "ASL Interpretation, audio description",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_ASL_INTERPRETATION,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Sensory Friendly",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_SENSOR_FRIENDLY,
                      )),
                    ]),
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Outdoors",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_OUTDOORS,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Shaded / Covered",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_SHADED_COVERED,
                      )),
                    ]),
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "BYO Chairs",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_BYO_CHAIRS,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Air Conditioned",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_AIR_CONDITION,
                      )),
                    ]),
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Camping Available",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_CAMPING_AVAILABLE,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Accommodation Nearby",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_ACCOMMODATION_NEARBY,
                      )),
                    ]),
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Social Distancing",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_SOCIAL_DISTANCE,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Wi-Fi Available",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_WIFI_AVAILABLE,
                      )),
                    ]),
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Telstra Coverage",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_TELSTRA_COVERAGE,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Optus Coverage",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_OPTUS_COVERAGE,
                      )),
                    ]),
                    TableRow(children: [
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Medical Services",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_MEDICAL_SERVICE,
                      )),
                      TableCell(
                          child: FeatureImageWithName(
                        name: "Security",
                        imageSize: 38,
                        imagePath: AppAssets.FEATURE_SECURITY,
                      )),
                    ]),
                  ],
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
