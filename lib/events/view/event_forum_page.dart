import 'package:flutter/material.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/utils/utils.dart';

class EventForumPage extends StatefulWidget {
  const EventForumPage({super.key});

  @override
  State<EventForumPage> createState() => _EventForumPageState();
}

class _EventForumPageState extends State<EventForumPage> {
  final TextEditingController _chatController = TextEditingController();
  bool chatTexfieldError = false;

  List<ChatData> chatList = [
    ChatData(
      ownChat: false,
      profileImage:
          "https://img.freepik.com/free-photo/young-bearded-man-with-striped-shirt_273609-5677.jpg",
      text: "Hi",
      date: "24 May 2023",
      time: "5:30PM",
    ),
    ChatData(
        ownChat: true,
        text: "Hi",
        date: "24 May 2023",
        time: "5:32PM",
        seenStatus: SeenType.send),
    ChatData(
      ownChat: false,
      profileImage:
          "https://img.freepik.com/free-photo/young-bearded-man-with-striped-shirt_273609-5677.jpg",
      text: "Hello, Nice to see you",
      date: "24 May 2023",
      time: "5:35PM",
    ),
    ChatData(
        ownChat: true,
        text: "Hello, Nice to see you",
        date: "25 May 2023",
        time: "5:40PM",
        seenStatus: SeenType.delivered),
    ChatData(
        ownChat: true,
        text: "Hi",
        date: "25 May 2023",
        time: "5:32PM",
        seenStatus: SeenType.send),
    ChatData(
      ownChat: false,
      profileImage:
          "https://img.freepik.com/free-photo/young-bearded-man-with-striped-shirt_273609-5677.jpg",
      text: "Hello, Nice to see you",
      date: "25 May 2023",
      time: "5:35PM",
    ),
    ChatData(
        ownChat: true,
        text: "Hello, Nice to see you",
        time: "5:45PM",
        date: "26 May 2023",
        seenStatus: SeenType.seen),
    ChatData(
        ownChat: true,
        text: "Hello, Nice to see you",
        date: "26 May 2023",
        time: "5:45PM",
        seenStatus: SeenType.seen),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColorECF4F1,
      appBar: AppBar(
        title: const Text('Event Forum'),
        leading: GestureDetector(
          child: const Icon(
            Icons.arrow_back_ios,
            size: 22,
          ),
          onTap: () {
            Navigator.of(context).pop("ssss");
          },
        ),
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 16),
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Expanded(
              child: ListView.builder(
                  shrinkWrap: true,
                  padding: EdgeInsets.zero,
                  itemCount: chatList.length,
                  itemBuilder: (context, i) {
                    bool displayDate = false;
                    if (i == 0) {
                      displayDate = true;
                    } else {
                      if (chatList[i].date != chatList[i - 1].date) {
                        displayDate = true;
                      }
                    }
                    return Column(
                      children: [
                        sizedBoxHeight(15),
                        displayDate
                            ? Column(
                                children: [
                                  Text(
                                    chatList[i].date,
                                    style: size13_M_medium(
                                        textColor:
                                            darkGreyColor.withOpacity(0.7)),
                                  ),
                                  sizedBoxHeight(15),
                                ],
                              )
                            : Container(),
                        Align(
                          alignment: chatList[i].ownChat
                              ? Alignment.centerRight
                              : Alignment.centerLeft,
                          child: SizedBox(
                            width: MediaQuery.of(context).size.width / 1.5,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                !chatList[i].ownChat
                                    ? RoundedBorderBoxImage(
                                        imageSize: 22,
                                        imageUrl: chatList[i].profileImage!,
                                      )
                                    : Container(),
                                sizedBoxWidth(10),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: chatList[i].ownChat
                                        ? CrossAxisAlignment.end
                                        : CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        padding: const EdgeInsets.all(8),
                                        decoration: common_border_dec(
                                            color: chatList[i].ownChat
                                                ? primaryColor
                                                : colorWhite,
                                            borderRadius: 4,
                                            borderColor: colorGrey,
                                            borderWidth: 0.5),
                                        child: Text(
                                          chatList[i].text,
                                          style: size14_M_medium(
                                              textColor: chatList[i].ownChat
                                                  ? colorWhite
                                                  : colorBlack),
                                        ),
                                      ),
                                      sizedBoxHeight(5),
                                      Row(
                                        mainAxisAlignment: chatList[i].ownChat
                                            ? MainAxisAlignment.end
                                            : MainAxisAlignment.end,
                                        children: [
                                          Text(
                                            chatList[i].time,
                                            style: size13_M_regular(
                                                textColor: colorGrey),
                                          ),
                                          chatList[i].ownChat
                                              ? Row(
                                                  children: [
                                                    sizedBoxWidth(5),
                                                    Icon(
                                                      chatList[i].seenStatus! ==
                                                              SeenType.send
                                                          ? Icons.done
                                                          : Icons.done_all,
                                                      size: 15,
                                                      color: chatList[i]
                                                                  .seenStatus! ==
                                                              SeenType.seen
                                                          ? primaryColor
                                                          : colorGrey,
                                                    ),
                                                  ],
                                                )
                                              : Container()
                                        ],
                                      )
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    );
                  }),
            ),
            sizedBoxHeight(10),
            Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Expanded(
                  child: CustomTextField(
                    hintText: "",
                    labelText: "Start typing here",
                    maxline: null,
                    isError: chatTexfieldError,
                    textController: _chatController,
                    keybordType: TextInputType.text,
                    textInputAction: TextInputAction.done,
                    onChanged: (value) {},
                    onTapped: () {
                      setState(() {
                        chatTexfieldError = false;
                      });
                    },
                  ),
                ),
                sizedBoxWidth(10),
                GestureDetector(
                  onTap: () async {
                    if (_chatController.text.isEmpty) {
                      setState(() {
                        chatTexfieldError = true;
                      });
                      await showOKDialog(
                        context: context,
                        labelTitle: "Error !",
                        labelContent: "Please enter message to send",
                      );
                    }
                  },
                  child: Container(
                      decoration: common_border_dec(
                          borderRadius: 8, color: primaryColor),
                      padding: const EdgeInsets.all(8),
                      child: const Icon(Icons.send, size: 35)),
                ),
              ],
            ),
            sizedBoxHeight(30),
          ],
        ),
      ),
    );
  }
}

class ChatData {
  bool ownChat;
  String? profileImage;
  String text;
  String date;
  String time;
  SeenType? seenStatus;
  ChatData({
    required this.ownChat,
    this.profileImage,
    required this.text,
    required this.date,
    required this.time,
    this.seenStatus,
  });
}

enum SeenType { send, delivered, seen }
