import 'package:flutter/material.dart';
import 'package:wiki_events/common/common.dart';

import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/events/model/selectable_model.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class EventTypePage extends StatefulWidget {
  List<dynamic> selectedTypeList;
  EventTypePage({super.key, required this.selectedTypeList});

  @override
  State<EventTypePage> createState() => _EventTypePageState();
}

class _EventTypePageState extends State<EventTypePage> {
  List<SelectableEventTypeModel> eventTypeList = [];

  late EventProvider eventProvider;

  @override
  void initState() {
    loadEventTypes();
    super.initState();
  }

  Future<void> loadEventTypes() async {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      eventProvider = Provider.of<EventProvider>(context, listen: false);
      eventProvider.setLoading = true;
      await eventProvider.getEventTypes();
      if (eventProvider.eventTypeList.isNotEmpty) {
        setSelectedEventTypes(eventProvider);
      }
      eventProvider.setLoading = false;
    });
  }

  void setSelectedEventTypes(EventProvider provider) {
    if (widget.selectedTypeList.isNotEmpty) {
      for (var element in eventProvider.eventTypeList) {
        for (var selectItem in widget.selectedTypeList) {
          if (selectItem.id == element.eventType.id) {
            int index = eventProvider.eventTypeList.indexOf(element);
            eventProvider.eventTypeList[index].isSelected = true;
          }
        }
      }
      provider.eventTypeList = provider.eventTypeList;
    }
  }

  onPressedBackButton() {
    List<EventTypeModel> returnEventTypeList = [];
    if (eventTypeList.isNotEmpty) {
      for (var element in eventTypeList) {
        if (element.isSelected) {
          returnEventTypeList.add(element.eventType);
        }
      }
    }
    Navigator.of(context).pop(returnEventTypeList);
  }

  @override
  Widget build(BuildContext context) {
    eventProvider = Provider.of<EventProvider>(context, listen: true);
    eventTypeList = eventProvider.eventTypeList;
    return Stack(
      children: [
        Scaffold(
          backgroundColor: bgColorECF4F1,
          appBar: AppBar(
            title: const Text('Event Type'),
            leading: GestureDetector(
              onTap: onPressedBackButton,
              child: const Icon(
                Icons.arrow_back_ios,
                size: 22,
              ),
            ),
          ),
          body: SingleChildScrollView(
            child: Container(
              padding: const EdgeInsets.symmetric(horizontal: 16),
              width: double.infinity,
              child: Column(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: const EdgeInsets.fromLTRB(12, 12, 12, 0),
                    decoration: common_border_dec(
                        borderRadius: 8, borderColor: textFieldOutlineColor),
                    child: ListView.builder(
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: eventTypeList.length,
                      itemBuilder: (context, i) {
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 12),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Row(
                                children: [
                                  NetworkImageWidget(
                                    imageSize: 28,
                                    imageUrl:
                                        eventTypeList[i].eventType.imageUrl!,
                                  ),
                                  sizedBoxWidth(12),
                                  Text(
                                    eventTypeList[i].eventType.name!,
                                    style: size14_M_regular(),
                                  ),
                                ],
                              ),
                              Transform.scale(
                                scale: 1,
                                child: Checkbox(
                                    visualDensity: const VisualDensity(
                                      vertical: VisualDensity.minimumDensity,
                                    ),
                                    activeColor: colorBlack,
                                    value: eventTypeList[i].isSelected,
                                    onChanged: (value) {
                                      eventTypeList[i].isSelected = value!;
                                      eventProvider.setEventTypeList =
                                          eventTypeList;
                                    }),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        eventProvider.isLoading ? const OnPageLoader() : Container()
      ],
    );
  }
}
