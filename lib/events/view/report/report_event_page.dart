import 'package:flutter/material.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/utils/utils.dart';

class ReportEventPage extends StatefulWidget {
  const ReportEventPage({super.key});

  @override
  State<ReportEventPage> createState() => _ReportEventPageState();
}

class _ReportEventPageState extends State<ReportEventPage> {
  late TextEditingController _commentsController;

  DropdownItems? _currentSelectedValue;

  List<DropdownItems> dropdownItemList = [
    DropdownItems(id: 1, title: "Reason1"),
    DropdownItems(id: 2, title: "Reason2"),
    DropdownItems(id: 3, title: "Reason3"),
    DropdownItems(id: 4, title: "Reason4"),
  ];

  bool isReasonError = false;

  @override
  void initState() {
    _commentsController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _commentsController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: backgroundColor,
        appBar: AppBar(
          title: const Text("Report Event"),
          leading: GestureDetector(
            child: const Icon(
              Icons.arrow_back_ios,
              size: 22,
            ),
            onTap: () {
              Navigator.of(context).pop();
              // widget.isFrom.go(context);
            },
          ),
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          width: double.infinity,
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Reason',
                    style: size14_M_regular(),
                  ),
                  sizedBoxHeight(10),
                  CommonDropdown(
                    dropdownList: dropdownItemList,
                    currentItem: _currentSelectedValue,
                    hintText: "Select reason",
                    onChangedCallBack: (value) {
                      setState(() {
                        _currentSelectedValue = value;
                        isReasonError = false;
                      });
                    },
                    isError: isReasonError,
                  ),
                ],
              ),
              sizedBoxHeight(24),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Comments',
                    style: size14_M_regular(),
                  ),
                  sizedBoxHeight(8),
                  TextFormField(
                    controller: _commentsController,
                    maxLines: 4,
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    keyboardType: TextInputType.text,
                    style: size14_M_medium(),
                    decoration: InputDecoration(
                      alignLabelWithHint: true,
                      label: const Text(
                        "Type your comment",
                        textAlign: TextAlign.start,
                      ),
                      hintText: "",
                      border: OutlineInputBorder(
                          borderSide: const BorderSide(color: colorGrey),
                          borderRadius: BorderRadius.circular(8)),
                      enabledBorder: OutlineInputBorder(
                          borderSide: const BorderSide(color: colorGrey),
                          borderRadius: BorderRadius.circular(8)),
                    ),
                    onFieldSubmitted: (value) =>
                        FocusScope.of(context).unfocus(),
                    onSaved: (value) => FocusScope.of(context).unfocus(),
                  ),
                ],
              ),
              const Spacer(),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(ScreenSize.getWidth(context, 0.6), 48),
                ),
                onPressed: () async {
                  onPressedSubmitButton();
                },
                child: const Text('Submit'),
              ),
              sizedBoxHeight(50),
            ],
          ),
        ));
  }

  onPressedSubmitButton() async {
    if (_currentSelectedValue == null) {
      await showOKDialog(
        context: context,
        labelTitle: "Error !",
        labelContent: "Please select any reason",
      );
      setState(() {
        isReasonError = true;
      });
    } else {
      await showOKDialog(
        context: context,
        labelTitle: "Success !",
        labelContent: "You have successfully reported this event",
        onPressOk: () {
          Navigator.of(context).pop();
          Navigator.of(context).pop();
        },
      );
    }
  }
}
