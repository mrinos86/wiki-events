// ignore_for_file: unnecessary_question_mark

import 'package:wiki_events/events/events.dart';

class EventResModel {
  EventResModel({this.event, this.message, this.result});

  EventModel? event;
  String? message;
  bool? result;

  EventResModel.fromJson(dynamic json) {
    event = EventModel.fromJson(json['payload']);
    message = json['message'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    if (event != null) {
      map['payload'] = event!.toJson();
    }
    map['message'] = message;
    map['result'] = result;

    return map;
  }
}
