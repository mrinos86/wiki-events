class EventModel {
  String? id;
  int? userId;
  String? name;
  int? ageRestricted;
  String? date;
  String? startTime;
  String? endTime;
  String? description;
  String? location;
  String? latitude;
  String? longitude;
  String? createdAt;
  String? updatedAt;
  String? distance;
  bool? amILiked;
  String? reviewRate;
  int? reviewCount;
  List<EventImageModel>? images;
  List<EventTypeModel>? eventTypes;
  List<EventFeatureModel>? eventFeatures;

  EventModel({
    this.id,
    this.userId,
    this.name,
    this.ageRestricted,
    this.date,
    this.startTime,
    this.endTime,
    this.description,
    this.location,
    this.latitude,
    this.longitude,
    this.createdAt,
    this.updatedAt,
    this.distance,
    this.amILiked,
    this.images,
    this.eventTypes,
    this.eventFeatures,
    this.reviewRate,
    this.reviewCount,
  });

  EventModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['user_id'];
    name = json['name'];
    ageRestricted = json['age_restricted'];
    date = json['date'];
    startTime = json['start_time'];
    endTime = json['end_time'];
    description = json['description'];
    location = json['location'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    createdAt = json['created_at'];
    updatedAt = json['updated_at'];
    distance = json['distance'];
    amILiked = json['am_i_liked'];
    reviewRate = json['reviews_rate'];
    reviewCount = json['reviews_count'];
    if (json['images'] != null) {
      images = <EventImageModel>[];
      json['images'].forEach((v) {
        images!.add(EventImageModel.fromJson(v));
      });
    }
    if (json['event_types'] != null) {
      eventTypes = <EventTypeModel>[];
      json['event_types'].forEach((v) {
        eventTypes!.add(EventTypeModel.fromJson(v));
      });
    }
    if (json['event_features'] != null) {
      eventFeatures = <EventFeatureModel>[];
      json['event_features'].forEach((v) {
        eventFeatures!.add(EventFeatureModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['user_id'] = userId;
    data['name'] = name;
    data['age_restricted'] = ageRestricted;
    data['date'] = date;
    data['start_time'] = startTime;
    data['end_time'] = endTime;
    data['description'] = description;
    data['location'] = location;
    data['latitude'] = latitude;
    data['longitude'] = longitude;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['distance'] = distance;
    data['am_i_liked'] = amILiked;
    data['reviews_rate'] = reviewRate;
    data['reviews_count'] = reviewCount;
    if (images != null) {
      data['images'] = images!.map((v) => v.toJson()).toList();
    }
    if (eventTypes != null) {
      data['event_types'] = eventTypes!.map((v) => v.toJson()).toList();
    }
    if (eventFeatures != null) {
      data['event_features'] = eventFeatures!.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class EventImageModel {
  int? id;
  String? imageableType;
  int? imageableId;
  String? url;
  String? path;

  EventImageModel(
      {this.id, this.imageableType, this.imageableId, this.url, this.path});

  EventImageModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    imageableType = json['imageable_type'];
    imageableId = json['imageable_id'];
    url = json['url'];
    path = json['path'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['imageable_type'] = imageableType;
    data['imageable_id'] = imageableId;
    data['url'] = url;
    data['path'] = path;
    return data;
  }
}

class EventTypeModel {
  String? id;
  String? name;
  String? imageUrl;
  String? imageUrlNormal;
  String? imageUrlSelect;

  EventTypeModel(
      {this.id,
      this.name,
      this.imageUrl,
      this.imageUrlNormal,
      this.imageUrlSelect});

  EventTypeModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    imageUrl = json['image_url'];
    imageUrlNormal = json['image_url_normal'];
    imageUrlSelect = json['image_url_select'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['image_url'] = imageUrl;
    data['image_url_normal'] = imageUrlNormal;
    data['image_url_select'] = imageUrlSelect;
    return data;
  }
}

class EventFeatureModel {
  String? id;
  String? name;
  String? imageUrl;

  EventFeatureModel({this.id, this.name, this.imageUrl});

  EventFeatureModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    imageUrl = json['image_url'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['name'] = name;
    data['image_url'] = imageUrl;
    return data;
  }
}
