import 'package:wiki_events/events/events.dart';

class ExploreSelectableEventModel {
  bool isSelected;
  EventModel event;
  ExploreSelectableEventModel({required this.event, required this.isSelected});
}

class SelectableEventTypeModel {
  bool isSelected;
  EventTypeModel eventType;
  SelectableEventTypeModel({required this.eventType, required this.isSelected});
}

class SelectableEventFeatureModel {
  bool isSelected;
  EventFeatureModel eventFeature;

  SelectableEventFeatureModel({
    required this.eventFeature,
    required this.isSelected,
  });
}
