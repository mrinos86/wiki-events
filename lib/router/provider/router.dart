import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

import 'package:wiki_events/app/app.dart';
import 'package:wiki_events/auth/auth.dart';
import 'package:wiki_events/bottom_nav_bar/view/nav_controller.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/explore/explore.dart';
import 'package:wiki_events/more/view/app_settings_page.dart';
import 'package:wiki_events/my_events/my_event.dart';
import 'package:wiki_events/my_events/view/check_duplicate_events_page.dart';
import 'package:wiki_events/notification/view/notifications_page.dart';
import 'package:wiki_events/profile/profile.dart';
import 'package:wiki_events/more/more.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/saved/saved_events.dart';

final GlobalKey<NavigatorState> _rootNavigatorKey = GlobalKey<NavigatorState>();
final GlobalKey<NavigatorState> _shellNavigatorKey =
    GlobalKey<NavigatorState>();

class AppRouter {
  final AppStateServiceProvider _appStateServiceProvider;

  AppRouter(this._appStateServiceProvider);
  final String bottomNavPrefix = '/bottomNav';

  GoRouter get goRouter => _goRouter;

  late final GoRouter _goRouter = GoRouter(
    refreshListenable: _appStateServiceProvider,
    initialLocation: '/',
    navigatorKey: _rootNavigatorKey,
    debugLogDiagnostics: true,
    routes: <RouteBase>[
      GoRoute(
        path: Pages.splash.toPath(),
        name: Pages.splash.toPathName(),
        pageBuilder: (context, state) =>
            MaterialPage(key: state.pageKey, child: const SplashPage()),
      ),
      GoRoute(
          path: Pages.intro.toPath(),
          name: Pages.intro.toPathName(),
          pageBuilder: (context, state) =>
              MaterialPage(key: state.pageKey, child: const IntroPage()),
          routes: [
            GoRoute(
              path: Pages.signUp.toPath(isSubRoute: true),
              name: Pages.signUp.toPathName(),
              pageBuilder: (context, state) => MaterialPage(
                  key: state.pageKey,
                  child: SignUpPage(
                      isClient: state.queryParameters.containsKey('type')
                          ? state.queryParameters['type'] == 'client'
                          : true)),
            ),
            GoRoute(
                path: Pages.signIn.toPath(isSubRoute: true),
                name: Pages.signIn.toPathName(),
                pageBuilder: (context, state) =>
                    MaterialPage(key: state.pageKey, child: const SignInPage()),
                routes: [
                  GoRoute(
                      path: Pages.forgotPassword.toPath(isSubRoute: true),
                      name: Pages.forgotPassword.toPathName(),
                      parentNavigatorKey: _rootNavigatorKey,
                      pageBuilder: (context, state) => MaterialPage(
                          key: state.pageKey,
                          child: const ForgotPasswordPage()),
                      routes: [
                        GoRoute(
                          path: Pages.forgotPasswordSuccess
                              .toPath(isSubRoute: true),
                          name: Pages.forgotPasswordSuccess.toPathName(),
                          pageBuilder: (context, state) {
                            String email = state.extra as String;
                            return MaterialPage(
                                key: state.pageKey,
                                child: ForgotPasswordSuccessPage(email: email));
                          },
                        ),
                      ]),
                ]),
          ]),
      GoRoute(
        path: Pages.emailVerification.toPath(),
        name: Pages.emailVerification.toPathName(),
        pageBuilder: (context, state) => MaterialPage(
            key: state.pageKey, child: const EmailVerificationPage()),
      ),
      GoRoute(
        path: Pages.completeProfile.toPath(),
        name: Pages.completeProfile.toPathName(),
        pageBuilder: (context, state) => MaterialPage(
            key: state.pageKey, child: const CompleteProfilePage()),
      ),
      ShellRoute(
        navigatorKey: _shellNavigatorKey,
        builder: (context, state, child) {
          return NavController(child: child);
        },
        routes: <RouteBase>[
          GoRoute(
              path: Pages.explore.toPath(pathPrefix: bottomNavPrefix),
              name: Pages.explore.toPathName(),
              pageBuilder: (context, state) => NoTransitionPage(
                    key: state.pageKey,
                    child: const ExplorePage(),
                  ),
              routes: [
                GoRoute(
                  path: Pages.exploreList.toPath(isSubRoute: true),
                  name: Pages.exploreList.toPathName(),
                  pageBuilder: (context, state) => MaterialPage(
                      key: state.pageKey, child: const ExploreListPage()),
                ),
                GoRoute(
                  path: Pages.exploreFilter.toPath(isSubRoute: true),
                  name: Pages.exploreFilter.toPathName(),
                  parentNavigatorKey: _rootNavigatorKey,
                  pageBuilder: (context, state) => MaterialPage(
                      key: state.pageKey, child: const ExploreFilterPage()),
                  routes: [
                    GoRoute(
                      path: Pages.sortOption.toPath(isSubRoute: true),
                      name: Pages.sortOption.toPathName(),
                      parentNavigatorKey: _rootNavigatorKey,
                      pageBuilder: (context, state) => MaterialPage(
                          key: state.pageKey, child: const SortOptionPage()),
                    ),
                  ],
                ),
                // GoRouter.of(context).goNamed(Pages.viewEvent.toPath()) will only work if navigate from browse screen
                // Use GoRouter.of(context).pushNamed(Pages.viewEvent.toPath()) to navigate to this page from other screens
                // because it will try to navigate to the page using the root navigator
                GoRoute(
                    path: Pages.eventFeatures.toPath(isSubRoute: true),
                    name: Pages.eventFeatures.toPathName(),
                    parentNavigatorKey: _rootNavigatorKey,
                    pageBuilder: (context, state) {
                      List<dynamic> list = state.extra as List<dynamic>;
                      return MaterialPage(
                          key: state.pageKey,
                          child: EventFeaturesPage(selectedFeaureList: list));
                    }),
                GoRoute(
                    path: Pages.eventType.toPath(isSubRoute: true),
                    name: Pages.eventType.toPathName(),
                    parentNavigatorKey: _rootNavigatorKey,
                    pageBuilder: (context, state) {
                      List<dynamic> list = state.extra as List<dynamic>;
                      return MaterialPage(
                          key: state.pageKey,
                          child: EventTypePage(selectedTypeList: list));
                    }),
                GoRoute(
                  path: Pages.eventForum.toPath(isSubRoute: true),
                  name: Pages.eventForum.toPathName(),
                  parentNavigatorKey: _rootNavigatorKey,
                  pageBuilder: (context, state) => MaterialPage(
                      key: state.pageKey, child: const EventForumPage()),
                ),
                GoRoute(
                    path: Pages.viewEvent.toPath(isSubRoute: true),
                    name: Pages.viewEvent.toPathName(),
                    parentNavigatorKey: _rootNavigatorKey,
                    pageBuilder: (context, GoRouterState state) {
                      ViewEventPageParam params =
                          state.extra as ViewEventPageParam;
                      return MaterialPage(
                        key: state.pageKey,
                        child: ViewEventPage(param: params),
                      );
                    },
                    routes: [
                      GoRoute(
                        path: Pages.viewEventFeatures.toPath(isSubRoute: true),
                        name: Pages.viewEventFeatures.toPathName(),
                        parentNavigatorKey: _rootNavigatorKey,
                        pageBuilder: (context, GoRouterState state) {
                          return MaterialPage(
                              key: state.pageKey,
                              child: const ViewEventFeaturesPage());
                        },
                      ),
                      GoRoute(
                          path: Pages.viewReview.toPath(isSubRoute: true),
                          name: Pages.viewReview.toPathName(),
                          parentNavigatorKey: _rootNavigatorKey,
                          pageBuilder: (context, GoRouterState state) {
                            return MaterialPage(
                                key: state.pageKey,
                                child: const ViewReviewsPage());
                          },
                          routes: [
                            GoRoute(
                              path: Pages.addReview.toPath(isSubRoute: true),
                              name: Pages.addReview.toPathName(),
                              parentNavigatorKey: _rootNavigatorKey,
                              pageBuilder: (context, GoRouterState state) {
                                return MaterialPage(
                                    key: state.pageKey,
                                    child: const AddReviewPage());
                              },
                            ),
                          ]),
                      GoRoute(
                        path: Pages.reportEvent.toPath(isSubRoute: true),
                        name: Pages.reportEvent.toPathName(),
                        parentNavigatorKey: _rootNavigatorKey,
                        pageBuilder: (context, GoRouterState state) {
                          return MaterialPage(
                              key: state.pageKey,
                              child: const ReportEventPage());
                        },
                      ),
                    ]),
              ]),
          GoRoute(
            path: Pages.saved.toPath(pathPrefix: bottomNavPrefix),
            name: Pages.saved.toPathName(),
            pageBuilder: (context, state) =>
                NoTransitionPage(key: state.pageKey, child: const SavedPage()),
            routes: [
              GoRoute(
                path: Pages.savedList.toPath(isSubRoute: true),
                name: Pages.savedList.toPathName(),
                pageBuilder: (context, state) => MaterialPage(
                    key: state.pageKey, child: const SavedListPage()),
              ),
            ],
          ),
          GoRoute(
            path: Pages.myEvents.toPath(pathPrefix: bottomNavPrefix),
            name: Pages.myEvents.toPathName(),
            pageBuilder: (context, state) => NoTransitionPage(
                key: state.pageKey, child: const MyEventsPage()),
            routes: [
              GoRoute(
                path: Pages.addEvent.toPath(isSubRoute: true),
                name: Pages.addEvent.toPathName(),
                parentNavigatorKey: _rootNavigatorKey,
                pageBuilder: (context, GoRouterState state) {
                  int stepNo = state.extra as int;
                  return MaterialPage(
                      key: state.pageKey, child: AddEventPage(step: stepNo));
                },
              ),
              GoRoute(
                path: Pages.editEvent.toPath(isSubRoute: true),
                name: Pages.editEvent.toPathName(),
                parentNavigatorKey: _rootNavigatorKey,
                pageBuilder: (context, GoRouterState state) {
                  return MaterialPage(
                      key: state.pageKey, child: const EditEventPage());
                },
              ),
              GoRoute(
                path: Pages.checkDuplicateEvent.toPath(isSubRoute: true),
                name: Pages.checkDuplicateEvent.toPathName(),
                parentNavigatorKey: _rootNavigatorKey,
                pageBuilder: (context, GoRouterState state) {
                  return MaterialPage(
                      key: state.pageKey,
                      child: const CheckDuplicateEventPage());
                },
              )
            ],
          ),
          GoRoute(
            path: Pages.notifications.toPath(pathPrefix: bottomNavPrefix),
            name: Pages.notifications.toPathName(),
            pageBuilder: (context, state) => NoTransitionPage(
                key: state.pageKey, child: const NotificationsPage()),
          ),
          GoRoute(
              path: Pages.more.toPath(pathPrefix: bottomNavPrefix),
              name: Pages.more.toPathName(),
              pageBuilder: (context, state) =>
                  NoTransitionPage(key: state.pageKey, child: const MorePage()),
              routes: [
                GoRoute(
                  path: Pages.termsAndConditions.toPath(isSubRoute: true),
                  name: Pages.termsAndConditions.toPathName(),
                  parentNavigatorKey: _rootNavigatorKey,
                  pageBuilder: (context, state) => MaterialPage(
                      key: state.pageKey, child: const TermsAndConditionPage()),
                ),
                GoRoute(
                  path: Pages.privacyPolicy.toPath(isSubRoute: true),
                  name: Pages.privacyPolicy.toPathName(),
                  parentNavigatorKey: _rootNavigatorKey,
                  pageBuilder: (context, state) => MaterialPage(
                      key: state.pageKey, child: const PrivacyPolicyPage()),
                ),
                GoRoute(
                  path: Pages.aboutUs.toPath(isSubRoute: true),
                  name: Pages.aboutUs.toPathName(),
                  parentNavigatorKey: _rootNavigatorKey,
                  pageBuilder: (context, state) => MaterialPage(
                      key: state.pageKey, child: const AboutUsPage()),
                ),
                GoRoute(
                  path: Pages.faqs.toPath(isSubRoute: true),
                  name: Pages.faqs.toPathName(),
                  parentNavigatorKey: _rootNavigatorKey,
                  pageBuilder: (context, state) =>
                      MaterialPage(key: state.pageKey, child: const FAQsPage()),
                ),
                GoRoute(
                  path: Pages.appSettings.toPath(isSubRoute: true),
                  name: Pages.appSettings.toPathName(),
                  parentNavigatorKey: _rootNavigatorKey,
                  pageBuilder: (context, state) => MaterialPage(
                      key: state.pageKey, child: const AppSettingsPage()),
                ),
              ]),
        ],
      ),
      GoRoute(
        path: Pages.notFound.toPath(),
        name: Pages.notFound.toPathName(),
        pageBuilder: (context, state) => MaterialPage(
            key: state.pageKey,
            child: NotFoundPage(error: state.error.toString())),
      ),
    ],
    errorPageBuilder: (context, state) => MaterialPage(
        key: state.pageKey, child: NotFoundPage(error: state.error.toString())),
    redirect: (context, state) async {
      final stateName = _goRouter.routeInformationParser.configuration;
      final String splashPath =
          stateName.namedLocation(Pages.splash.toPathName());
      final String introPath =
          stateName.namedLocation(Pages.intro.toPathName());
      final String emailVerifyPath =
          stateName.namedLocation(Pages.emailVerification.toPathName());
      final String completeProfilePath =
          stateName.namedLocation(Pages.completeProfile.toPathName());

      final bool isInitialized = _appStateServiceProvider.isInitialized;
      final bool isSignedIn = _appStateServiceProvider.isLoggedIn;
      final bool isEmailVerified = _appStateServiceProvider.isEmailVerified;
      final bool isProfileCompleted =
          _appStateServiceProvider.isProfileCompleted;

      final bool isGoingToSplash = state.matchedLocation == splashPath;
      final bool isGoingToIntro = state.matchedLocation.startsWith(introPath);
      final bool isGoingToEmailVerify =
          state.matchedLocation.startsWith(emailVerifyPath);
      final bool isGoingToCompleteProfile =
          state.matchedLocation.startsWith(completeProfilePath);
      final bool isGoingToHome =
          state.matchedLocation.startsWith(bottomNavPrefix);

      if (!isInitialized && !isGoingToSplash) {
        return splashPath;
      }

      if (isInitialized && !isSignedIn && !isGoingToIntro) {
        return introPath;
      }
      if (isInitialized &&
          isSignedIn &&
          !isEmailVerified &&
          !isGoingToEmailVerify) {
        return emailVerifyPath;
      }
      if (isInitialized &&
          isSignedIn &&
          isEmailVerified &&
          !isProfileCompleted &&
          !isGoingToCompleteProfile) {
        await AuthServiceProvider.instance.refreshUser(null);
        return completeProfilePath;
      }
      if (isInitialized &&
          isSignedIn &&
          isEmailVerified &&
          isProfileCompleted &&
          !isGoingToHome) {
        return stateName.namedLocation(Pages.explore.toPathName());
      }
      return null;
    },
  );
}
