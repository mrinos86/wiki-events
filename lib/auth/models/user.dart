import 'package:dio/dio.dart';
import 'package:wiki_events/profile/profile.dart';

class AuthUserResponse {
  User? payload;
  String? message;
  bool? result;

  AuthUserResponse({this.payload, this.message, this.result});

  AuthUserResponse.fromJson(Map<String, dynamic> json) {
    payload = json['payload'] != null ? User.fromJson(json['payload']) : null;
    message = json['message'];
    result = json['result'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    if (payload != null) {
      data['payload'] = payload!.toJson();
    }
    data['message'] = message;
    data['result'] = result;
    return data;
  }
}

class User {
  String? uuid;
  String? email;
  String? emailVerifiedAt;
  String? firstName;
  String? lastName;
  String? areaCode;
  String? phone;
  String? fullName;
  String? avatarUrl;
  String? timeZone;
  String? about;
  int? categoryId;
  UserCategory? category;
  String? homeLocation;
  String? homeLat;
  String? homeLong;
  String? accessToken;

  User(
      {this.uuid,
      this.email,
      this.emailVerifiedAt,
      this.firstName,
      this.lastName,
      this.fullName,
      this.areaCode,
      this.avatarUrl,
      this.timeZone,
      this.about,
      this.categoryId,
      this.category,
      this.homeLocation,
      this.homeLat,
      this.homeLong,
      this.accessToken});

  User.fromJson(Map<String, dynamic> json) {
    uuid = json['uuid'];
    email = json['email'];
    emailVerifiedAt = json['email_verified_at'];
    firstName = json['first_name'];
    lastName = json['last_name'];
    fullName = json['full_name'];
    areaCode = json['area_code'];
    phone = json['phone'];
    avatarUrl = json['avatar_url'];
    timeZone = json['timezone'];
    about = json['about'];
    categoryId = json['category_id'];
    category = json["category"] != null
        ? UserCategory.fromJson(json["category"])
        : null;
    homeLocation = json['home_location'];
    homeLat = json['home_latitude'];
    homeLong = json['home_longitude'];
    accessToken = json['access_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['uuid'] = uuid;
    data['email'] = email;
    data['email_verified_at'] = emailVerifiedAt;
    data['first_name'] = firstName;
    data['last_name'] = lastName;
    data['full_name'] = fullName;
    data['area_code'] = areaCode;
    data['phone'] = phone;
    data['avatar_url'] = avatarUrl;
    data['timezone'] = timeZone;
    data['about'] = about;
    data['category_id'] = categoryId;
    data['category'] = category?.toJson();
    data['home_location'] = homeLocation;
    data['home_latitude'] = homeLat;
    data['home_longitude'] = homeLong;
    data['access_token'] = accessToken;
    return data;
  }
}

class UserSignUpRequest {
  String? deviceId;
  String? deviceType;
  String? devicePushToken;
  String? email;
  String? password;
  String? passwordConfirmation;

  UserSignUpRequest({
    required this.deviceId,
    required this.deviceType,
    this.devicePushToken,
    required this.email,
    required this.password,
    required this.passwordConfirmation,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['device_id'] = deviceId;
    data['device_type'] = deviceType;
    data['device_push_token'] = devicePushToken;
    data['email'] = email;
    data['password'] = password;
    data['password_confirmation'] = passwordConfirmation;
    return data;
  }

  FormData toFormData() =>
      FormData.fromMap(toJson(), ListFormat.multiCompatible);
}

class UserSignInRequest {
  String? deviceId;
  String? deviceType;
  String? devicePushToken;
  String? email;
  String? password;

  UserSignInRequest({
    required this.deviceId,
    required this.deviceType,
    this.devicePushToken,
    required this.email,
    required this.password,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['device_id'] = deviceId;
    data['device_type'] = deviceType;
    data['device_push_token'] = devicePushToken;
    data['email'] = email;
    data['password'] = password;
    return data;
  }

  FormData toFormData() =>
      FormData.fromMap(toJson(), ListFormat.multiCompatible);
}
