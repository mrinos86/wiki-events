import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:wiki_events/auth/auth.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/utils/keys.dart';

class AuthServiceProvider extends BaseApi with ChangeNotifier {
  static AuthServiceProvider? _instance;

  AuthServiceProvider._()
      : super(interceptors: [ClientKeyInterceptor(), PrettyDioLogger()]) {
    _init();
  }

  static AuthServiceProvider get instance {
    if (_instance == null) {
      throw Exception("Auth Service Provider not initialized");
    }
    return _instance!;
  }

  static void initialize() {
    if (_instance == null) {
      _instance = AuthServiceProvider._();
    } else {
      throw Exception("Auth Service provider already initialized");
    }
  }

  void _init() {}

  User? _user;
  final StreamController<User?> _streamController =
      StreamController.broadcast();
  final StorageService _storageService = StorageService.instance;
  final DeviceInfo _deviceInfo = DeviceInfo.instance;

  Stream<User?> get onAuthStateChanged => _streamController.stream;
  User? get user => _user;
  String? get token => _user?.accessToken;

  Future<ApiResponse<User?>> signUp(String email, String password) async {
    try {
      UserSignUpRequest userRegister = UserSignUpRequest(
        email: email,
        password: password,
        passwordConfirmation: password,
        deviceId: _deviceInfo.deviceId,
        deviceType: _deviceInfo.platform,
      );
      Response response =
          await post('register', data: userRegister.toFormData());
      AuthUserResponse authUserResponse =
          AuthUserResponse.fromJson(response.data);
      if (authUserResponse.result != null && authUserResponse.result!) {
        if (authUserResponse.payload != null) {
          _saveUser(authUserResponse.payload!);
          return ApiResponse<User?>(
            result: true,
            message: authUserResponse.message ?? 'Success',
            payload: authUserResponse.payload,
          );
        }
      }
      return ApiResponse<User?>(
        result: false,
        message: authUserResponse.message ?? 'Failed to register',
        payload: null,
      );
    } on DioError catch (e) {
      return onDioError<User?>(e);
    } catch (e) {
      return onError<User?>(e);
    }
  }

  Future<ApiResponse<User?>> signIn(String email, String password) async {
    try {
      UserSignInRequest userSignIn = UserSignInRequest(
        email: email,
        password: password,
        deviceId: _deviceInfo.deviceId,
        deviceType: _deviceInfo.platform,
      );
      Response response = await post('login', data: userSignIn.toFormData());
      AuthUserResponse authUserResponse =
          AuthUserResponse.fromJson(response.data);
      if (authUserResponse.result != null && authUserResponse.result!) {
        if (authUserResponse.payload != null) {
          _saveUser(authUserResponse.payload!);
          return ApiResponse<User?>(
            result: true,
            message: authUserResponse.message ?? 'Success',
            payload: authUserResponse.payload,
          );
        }
      }
      return ApiResponse<User?>(
        result: false,
        message: authUserResponse.message ?? 'Failed to login',
        payload: null,
      );
    } on DioError catch (e) {
      return onDioError<User?>(e);
    } catch (e) {
      return onError<User?>(e);
    }
  }

  Future<ApiResponse<User?>> verifyEmail(String code) async {
    try {
      Response response = await get('register/$code/verify',
          headers: {'x-access-token': _user?.accessToken});
      AuthUserResponse authUserResponse =
          AuthUserResponse.fromJson(response.data);
      if (authUserResponse.result != null && authUserResponse.result!) {
        await refreshUser(authUserResponse.payload);
        return ApiResponse<User?>(
          result: authUserResponse.result ?? true,
          message: authUserResponse.message ?? 'Success',
          payload: authUserResponse.payload,
        );
      } else {
        return ApiResponse<User?>(
          result: authUserResponse.result ?? false,
          message: authUserResponse.message ?? 'Failed',
          payload: authUserResponse.payload,
        );
      }
    } on DioError catch (e) {
      return onDioError<User?>(e);
    } catch (e) {
      return onError<User?>(e);
    }
  }

  Future<ApiResponse<User?>> resendOTP(String code) async {
    try {
      Response response = await get('register/otp-resend',
          headers: {'x-access-token': _user?.accessToken});
      AuthUserResponse authUserResponse =
          AuthUserResponse.fromJson(response.data);
      if (authUserResponse.result != null && authUserResponse.result!) {
        await refreshUser(authUserResponse.payload);
        return ApiResponse<User?>(
          result: authUserResponse.result ?? true,
          message: authUserResponse.message ?? 'Success',
          payload: authUserResponse.payload,
        );
      } else {
        return ApiResponse<User?>(
          result: authUserResponse.result ?? false,
          message: authUserResponse.message ?? 'Failed',
          payload: authUserResponse.payload,
        );
      }
    } on DioError catch (e) {
      return onDioError<User?>(e);
    } catch (e) {
      return onError<User?>(e);
    }
  }

  Future<ApiResponse<dynamic>> forgotPassword(String email) async {
    try {
      Response response = await post('password/email',
          data: FormData.fromMap({'email': email}));

      DynamicPayloadResponse dynamicRes =
          DynamicPayloadResponse.fromJson(response.data);
      if (dynamicRes.result != null && dynamicRes.result!) {
        return ApiResponse<dynamic>(
          result: true,
          message: dynamicRes.message ?? 'Success',
          payload: dynamicRes.payload,
        );
      }
      return ApiResponse<dynamic>(
        result: false,
        message: dynamicRes.message ?? 'Failed to login',
        payload: null,
      );
    } on DioError catch (e) {
      return onDioError<dynamic>(e);
    } catch (e) {
      return onError<dynamic>(e);
    }
  }

  Future<ApiResponse> refreshUser(User? user) async {
    /// This is only for sync user data from server should be called when user is already logged in
    /// If the [user] is not null will not fetch from server instead provided object will be saved
    try {
      if (user != null) {
        user.accessToken = _user?.accessToken;
        _saveUser(user);
        return ApiResponse(
          result: true,
          message: 'Success',
          payload: null,
        );
      }

      if (_user == null) {
        return ApiResponse(
          result: false,
          message: 'AuthUser is not logged in',
          payload: null,
        );
      }

      Response response = await get('profile', headers: {
        'x-access-token': _user?.accessToken,
      });
      AuthUserResponse authUserResponse =
          AuthUserResponse.fromJson(response.data);
      if (authUserResponse.result != null && authUserResponse.result!) {
        if (authUserResponse.payload != null) {
          // Add Access Token to AuthUser Profile Model doesn't have accessToken
          authUserResponse.payload!.accessToken = _user?.accessToken;
          _saveUser(authUserResponse.payload!);
          return ApiResponse(
            result: true,
            message: authUserResponse.message ?? 'Success',
            payload: authUserResponse.payload,
          );
        }
      }
      return ApiResponse(
        result: false,
        message: authUserResponse.message ?? 'Failed',
        payload: null,
      );
    } on DioError catch (e) {
      return onDioError(e);
    } catch (e) {
      return onError(e);
    }
  }

  Future<ApiResponse<User?>> signOut() async {
    try {
      Response response =
          await get('logout', headers: {'x-access-token': _user?.accessToken});
      NoPayloadResponse baseResponse =
          NoPayloadResponse.fromJson(response.data);
      if (baseResponse.result != null && baseResponse.result!) {
        _clearUser();
        return ApiResponse<User?>(
          result: true,
          message: baseResponse.message ?? 'Success',
          payload: null,
        );
      }
      return ApiResponse<User?>(
        result: false,
        message: baseResponse.message ?? 'Failed to logout',
        payload: null,
      );
    } on DioError catch (e) {
      return onDioError<User?>(e);
    } catch (e) {
      return onError<User?>(e);
    }
  }

  Future<void> startUpCheck() async {
    String? userString = _storageService.getString(AppKeys.USER_DATA_KEY);
    if (userString != null) {
      _user = User.fromJson(json.decode(userString));
      _streamController.add(_user);
      log('User Token: ${_user?.accessToken}');
      notifyListeners();
      return Future.value();
    } else {
      _streamController.add(null);
      notifyListeners();
      return Future.value();
    }
  }

  Future<bool> _saveUser(User user) async {
    bool result = await _storageService.setString(
        AppKeys.USER_DATA_KEY, jsonEncode(user.toJson()));
    if (!result) {
      _user = null;
      _streamController.add(null);
      notifyListeners();
      throw Exception('Failed to save user');
    }
    _user = user;
    _streamController.add(user);
    notifyListeners();
    return result;
  }

  Future<bool> _clearUser() async {
    bool result = await _storageService.remove(AppKeys.USER_DATA_KEY);
    if (result) {
      await _storageService.clear();
      _user = null;
      _streamController.add(null);
      notifyListeners();
      return result;
    } else {
      throw Exception('Failed to clear user');
    }
  }
}
