import 'package:flutter/material.dart';
import 'package:wiki_events/app/app.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class IntroPage extends StatefulWidget {
  const IntroPage({super.key});

  @override
  State<IntroPage> createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        body: Stack(
          children: [
            Positioned.fill(
              child: Center(
                child: Column(
                  // mainAxisSize: MainAxisSize.min,
                  children: [
                    sizedBoxHeight(200),
                    const AppLogo(),
                    sizedBoxHeight(32),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize:
                            Size(ScreenSize.getWidth(context, 0.6), 48),
                      ),
                      onPressed: () {
                        Pages.signUp.go(context);
                      },
                      child: const Text('Sign Up'),
                    ),
                    sizedBoxHeight(20),
                    TextButton(
                      style: TextButton.styleFrom(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 8, vertical: 0),
                          shape: const StadiumBorder()),
                      onPressed: () {
                        // Pages.signIn.go(context);
                      },
                      child: Text(
                        'Explore as Guest',
                        style: size14_M_medium(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Positioned(
                bottom: 20,
                left: 0,
                right: 0,
                child: Container(
                  width: double.infinity,
                  padding:
                      const EdgeInsets.symmetric(vertical: 32, horizontal: 16),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        'Already have an account?',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(25),
                      OutlinedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize:
                              Size(ScreenSize.getWidth(context, 0.6), 48),
                        ),
                        onPressed: () {
                          Pages.signIn.go(context);
                        },
                        child: const Text('Sign In'),
                      ),
                    ],
                  ),
                )),
          ],
        ),
      ),
    );
  }
}
