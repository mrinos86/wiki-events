import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pinput/pinput.dart';
import 'package:wiki_events/app/app.dart';
import 'package:wiki_events/auth/auth.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/utils/utils.dart';

class EmailVerificationPage extends StatefulWidget {
  const EmailVerificationPage({super.key});

  @override
  State<EmailVerificationPage> createState() => _EmailVerificationPageState();
}

class _EmailVerificationPageState extends State<EmailVerificationPage> {
  late TextEditingController _pinCodeEditingController;

  late AppStateServiceProvider _appStateServiceProvider;

  @override
  void initState() {
    _appStateServiceProvider = AppStateServiceProvider.instance;
    _pinCodeEditingController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _pinCodeEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final defaultPinTheme = PinTheme(
      width: 56,
      height: 66,
      margin: const EdgeInsets.symmetric(horizontal: 6),
      textStyle: size22_M_medium(),
      decoration: BoxDecoration(
        color: Theme.of(context).colorScheme.surface,
        border: Border.all(color: colorGrey),
        borderRadius: BorderRadius.circular(8),
      ),
    );
    final focusedPinTheme = PinTheme(
      width: 58,
      height: 68,
      margin: const EdgeInsets.symmetric(horizontal: 6),
      textStyle: size22_M_medium(),
      decoration: BoxDecoration(
          color: Theme.of(context).colorScheme.surface,
          border: Border.all(color: primaryColor),
          borderRadius: BorderRadius.circular(8),
          boxShadow: [
            BoxShadow(
              color: primaryColor.withOpacity(0.2),
              blurRadius: 16,
              offset: const Offset(0, 6),
            ),
          ]),
    );
    final submittedPinTheme = defaultPinTheme.copyBorderWith(
      border: Border.all(color: primaryColor, width: 0.5),
    );
    return WillPopScope(
      onWillPop: () async => false,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Scaffold(
          appBar: AppBar(
            // title: const Text("Sign Up"),
            automaticallyImplyLeading: false,
            actions: [
              Padding(
                padding: const EdgeInsets.all(15.0),
                child: InkWell(
                  onTap: () async {
                    AppStateServiceProvider.instance.showLoader;
                    ApiResponse response =
                        await AuthServiceProvider.instance.signOut();
                    AppStateServiceProvider.instance.hideLoader;
                    if (!response.result && mounted) {
                      await showOKDialog(
                        context: context,
                        labelTitle: "Error !",
                        labelContent: "",
                      );
                    }
                  },
                  customBorder: const CircleBorder(),
                  child: Text("Logout", style: size16_M_regular()),
                ),
              ),
            ],
          ),
          body: CustomScrollView(
            slivers: [
              SliverFillRemaining(
                hasScrollBody: false,
                fillOverscroll: true,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 16),
                  child: Column(
                    children: [
                      sizedBoxHeight(ScreenSize.getHeight(context, 0.03)),
                      Text(
                        "Please enter your \nverification code",
                        textAlign: TextAlign.center,
                        style: size22_M_medium(),
                      ),
                      sizedBoxHeight(50),
                      Text(
                        "The verification code has been sent to",
                        textAlign: TextAlign.center,
                        style: size16_M_regular(),
                      ),
                      Text(
                        _appStateServiceProvider.currentUser!.email!,
                        textAlign: TextAlign.center,
                        style: size16_M_regular(),
                      ),
                      sizedBoxHeight(15),
                      Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 24, vertical: 12),
                        width: ScreenSize.width(context),
                        height: ScreenSize.getHeight(context, 0.16),
                        child: Pinput(
                          defaultPinTheme: defaultPinTheme,
                          focusedPinTheme: focusedPinTheme,
                          submittedPinTheme: submittedPinTheme,
                          hapticFeedbackType: HapticFeedbackType.lightImpact,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          controller: _pinCodeEditingController,
                          inputFormatters: [
                            FilteringTextInputFormatter.digitsOnly
                          ],
                        ),
                      ),
                      const Spacer(),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize:
                              Size(ScreenSize.getWidth(context, 0.6), 48),
                        ),
                        onPressed: () async {
                          if (_pinCodeEditingController.text.length != 4) {
                            await showOKDialog(
                              context: context,
                              labelTitle: "Incorrect Verification Code",
                              labelContent:
                                  "Please enter a valid verification code",
                            );
                            return;
                          }

                          AppStateServiceProvider.instance.showLoader;
                          ApiResponse response =
                              await AuthServiceProvider.instance.verifyEmail(
                                  _pinCodeEditingController.text.trim());
                          AppStateServiceProvider.instance.hideLoader;

                          if (!response.result && mounted) {
                            await showOKDialog(
                              context: context,
                              labelTitle: "Incorrect Verification Code",
                              labelContent: response.message,
                            );
                          }
                        },
                        child: const Text("Verify"),
                      ),
                      sizedBoxHeight(25),
                      RichText(
                        text: TextSpan(
                            text: "Didn’t receive the verification code? ",
                            style: size14_M_regular(),
                            children: [
                              TextSpan(
                                  text: "Resend",
                                  style:
                                      size14_M_regular(textColor: primaryColor),
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () async {
                                      AppStateServiceProvider
                                          .instance.showLoader;

                                      await AuthServiceProvider.instance
                                          .resendOTP(_pinCodeEditingController
                                              .text
                                              .trim());
                                      AppStateServiceProvider
                                          .instance.hideLoader;
                                    }),
                            ]),
                      ),
                      sizedBoxHeight(50),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
