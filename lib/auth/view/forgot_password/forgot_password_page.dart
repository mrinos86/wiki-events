import 'package:flutter/material.dart';
import 'package:wiki_events/app/provider/provider.dart';
import 'package:wiki_events/auth/provider/provider.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class ForgotPasswordPage extends StatefulWidget {
  const ForgotPasswordPage({
    Key? key,
  }) : super(key: key);

  @override
  State<ForgotPasswordPage> createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  late TextEditingController _emailController;

  final GlobalKey<FormState> _formKey = GlobalKey();

  bool _isEmailValidate = false;
  bool _isEmailError = false;

  @override
  void initState() {
    _emailController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Forgot Password'),
          leading: GestureDetector(
            child: const Padding(
              padding: EdgeInsets.only(left: 10),
              child: Icon(
                Icons.arrow_back_ios,
                size: 25,
              ),
            ),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            width: double.infinity,
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  sizedBoxHeight(ScreenSize.getHeight(context, 0.05)),
                  Text(
                    "Enter your registered email address below and we’ll send you a password reset email.",
                    style: size16_M_regular(),
                    textAlign: TextAlign.center,
                  ),
                  sizedBoxHeight(24),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Email',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(8),
                      EmailTextField(
                        textController: _emailController,
                        onTapped: () {
                          setState(() => _isEmailError = false);
                        },
                        isError: _isEmailError,
                      ),
                    ],
                  ),
                  sizedBoxHeight(24),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      minimumSize: Size(ScreenSize.getWidth(context, 0.6), 48),
                    ),
                    onPressed: () async {
                      await onPressedSubmitButton();
                    },
                    child: const Text('Submit'),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  checkEmailValidate() {
    setState(() {
      if (_emailController.text.isEmpty) {
        _isEmailValidate = false;
      } else if (EmailValidator.validate(_emailController.text) == false) {
        _isEmailValidate = false;
      } else {
        _isEmailValidate = true;
      }
    });
  }

  onPressedSubmitButton() async {
    FocusScope.of(context).unfocus();
    checkEmailValidate();

    if (!_isEmailValidate) {
      setState(() => _isEmailError = true);

      await showOKDialog(
        context: context,
        labelTitle: "Error !",
        labelContent: "Please enter a valid email address",
      );
    } else {
      final email = _emailController.text.trim();

      AppStateServiceProvider.instance.showLoader;
      ApiResponse response =
          await AuthServiceProvider.instance.forgotPassword(email);
      AppStateServiceProvider.instance.hideLoader;

      if (!response.result && mounted) {
        await showOKDialog(
          context: context,
          labelTitle: "Error !",
          labelContent: response.message,
        );
      } else if (response.result) {
        Pages.forgotPasswordSuccess.go(context, extra: email);
      }
    }
  }
}
