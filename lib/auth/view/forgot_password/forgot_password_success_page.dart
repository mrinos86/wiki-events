import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:wiki_events/app/app.dart';
import 'package:wiki_events/auth/auth.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class ForgotPasswordSuccessPage extends StatefulWidget {
  String email;
  ForgotPasswordSuccessPage({Key? key, required this.email}) : super(key: key);

  @override
  State<ForgotPasswordSuccessPage> createState() =>
      _ForgotPasswordSuccessPageState();
}

class _ForgotPasswordSuccessPageState extends State<ForgotPasswordSuccessPage> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Forgot Password'),
          leading: GestureDetector(
            child: const Padding(
              padding: EdgeInsets.only(left: 10),
              child: Icon(
                Icons.arrow_back_ios,
                size: 25,
              ),
            ),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          width: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              sizedBoxHeight(ScreenSize.getHeight(context, 0.05)),
              Text(
                "Please Check Your Email",
                textAlign: TextAlign.center,
                style: size22_M_regular(),
              ),
              sizedBoxHeight(20),
              Text(
                "A password reset link has been sent to your dedicated email",
                style: size16_M_regular(),
                textAlign: TextAlign.center,
              ),
              sizedBoxHeight(50),
              RichText(
                text: TextSpan(
                    text: "Didn’t receive the link? ",
                    style: size14_M_medium(),
                    children: [
                      TextSpan(
                          text: "Resend",
                          style: size14_M_medium(textColor: primaryColor),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () async {
                              // Will be a API call
                              AppStateServiceProvider.instance.showLoader;
                              await AuthServiceProvider.instance
                                  .forgotPassword(widget.email);
                              AppStateServiceProvider.instance.hideLoader;
                            }),
                    ]),
              ),
              sizedBoxHeight(50),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(ScreenSize.getWidth(context, 0.6), 48),
                ),
                onPressed: () {
                  Pages.signIn.go(context);
                },
                child: const Text('Sign In'),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
