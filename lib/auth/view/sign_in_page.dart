import 'package:flutter/material.dart';
import 'package:wiki_events/app/app.dart';
import 'package:wiki_events/auth/auth.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class SignInPage extends StatefulWidget {
  const SignInPage({
    Key? key,
  }) : super(key: key);

  @override
  State<SignInPage> createState() => _SignInPageState();
}

class _SignInPageState extends State<SignInPage> {
  late TextEditingController _emailController;
  late TextEditingController _passwordController;

  final GlobalKey<FormState> _formKey = GlobalKey();
  bool _isObscureText = true;

  bool _isEmailError = false;
  bool _isPasswordError = false;

  bool _isEmailValidate = false;
  bool _isPasswordValidate = false;

  @override
  void initState() {
    _emailController = TextEditingController();
    _passwordController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _emailController.dispose();
    _passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Sign In'),
          leading: GestureDetector(
            child: const Padding(
              padding: EdgeInsets.only(left: 10),
              child: Icon(
                Icons.arrow_back_ios,
                size: 25,
              ),
            ),
            onTap: () {
              Navigator.of(context).pop();
            },
          ),
        ),
        body: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              fillOverscroll: true,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                width: double.infinity,
                child: Form(
                  key: _formKey,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      sizedBoxHeight(ScreenSize.getHeight(context, 0.03)),
                      SizedBox(
                          width: MediaQuery.of(context).size.width / 1.8,
                          child: const AppLogo()),
                      sizedBoxHeight(ScreenSize.getHeight(context, 0.02)),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Email',
                            style: size14_M_regular(),
                          ),
                          sizedBoxHeight(8),
                          EmailTextField(
                            textController: _emailController,
                            onTapped: () {
                              setState(() => _isEmailError = false);
                            },
                            isError: _isEmailError,
                          ),
                        ],
                      ),
                      sizedBoxHeight(16),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Password',
                            style: size14_M_regular(),
                          ),
                          sizedBoxHeight(8),
                          PasswordTextField(
                            textController: _passwordController,
                            isObsecureText: _isObscureText,
                            onTappedViewIcon: (value) {
                              setState(() => _isObscureText = value);
                            },
                            onTapped: () {
                              setState(() => _isPasswordError = false);
                            },
                            isError: _isPasswordError,
                          ),
                        ],
                      ),
                      sizedBoxHeight(25),
                      GestureDetector(
                        onTap: () {
                          setState(() {
                            _isEmailError = false;
                            _isPasswordError = false;
                          });
                          Pages.forgotPassword.go(context);
                        },
                        child: Text(
                          "Forgot Password?",
                          style: size14_M_semibold(),
                          textAlign: TextAlign.center,
                        ),
                      ),
                      sizedBoxHeight(24),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize:
                              Size(ScreenSize.getWidth(context, 0.6), 48),
                        ),
                        onPressed: () async {
                          await onPressedSignInButton();
                        },
                        child: const Text('Sign In'),
                      ),
                      sizedBoxHeight(16),
                      const Spacer(),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 16),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Don\'t have an account?',
                              style: size14_M_regular(),
                            ),
                            TextButton(
                              style: TextButton.styleFrom(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 8, vertical: 0),
                                  shape: const StadiumBorder()),
                              onPressed: () {
                                Pages.signUp.go(context);
                              },
                              child: Text(
                                'Sign Up',
                                style: size14_M_medium(textColor: primaryColor),
                              ),
                            ),
                          ],
                        ),
                      ),
                      sizedBoxHeight(32),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  checkEmailValidate() {
    setState(() {
      if (_emailController.text.isEmpty) {
        _isEmailValidate = false;
      } else if (EmailValidator.validate(_emailController.text) == false) {
        _isEmailValidate = false;
      } else {
        _isEmailValidate = true;
      }
    });
  }

  checkPasswordValidate() {
    setState(() {
      if (_passwordController.text.isEmpty) {
        _isPasswordValidate = false;
      } else if (_passwordController.text.length < 8) {
        _isPasswordValidate = false;
      } else {
        _isPasswordValidate = true;
      }
    });
  }

  onPressedSignInButton() async {
    checkEmailValidate();
    checkPasswordValidate();

    if (_isEmailValidate && _isPasswordValidate) {
      final email = _emailController.text.trim();
      final password = _passwordController.text.trim();

      AppStateServiceProvider.instance.showLoader;
      ApiResponse response =
          await AuthServiceProvider.instance.signIn(email, password);
      AppStateServiceProvider.instance.hideLoader;

      if (!response.result && mounted) {
        await showOKDialog(
          context: context,
          labelTitle: "Error !",
          labelContent: response.message,
        );
      }
    } else {
      String message = "";
      if (!_isEmailValidate && !_isPasswordValidate) {
        setState(() {
          _isEmailError = true;
          _isPasswordError = true;
        });
        message =
            "◍ Email should be a valid email \n◍ Password must be at least 8 characters ";
      } else if (!_isEmailValidate) {
        setState(() => _isEmailError = true);
        message = "Please enter a valid email address";
      } else if (!_isPasswordValidate) {
        setState(() => _isPasswordError = true);
        message = "Password must be at least 8 characters";
      }
      await showOKDialog(
          context: context, labelTitle: "Error !", labelContent: message);
    }
  }
}
