import 'package:flutter/material.dart';
import 'package:wiki_events/utils/assets.dart';

class AppLogo extends StatelessWidget {
  const AppLogo({
    super.key,
  });

  @override
  Widget build(BuildContext context) {
    return const Hero(
      tag: "appLogo",
      child: Image(
        image: AssetImage(AppAssets.LOGO),
        fit: BoxFit.fitWidth,
      ),
    );
  }
}
