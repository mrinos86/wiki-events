import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:wiki_events/app/provider/app_state.dart';
import 'package:wiki_events/app/provider/theme.dart';
import 'package:wiki_events/auth/models/user.dart';
import 'package:wiki_events/auth/provider/authentication.dart';
import 'package:wiki_events/events/provider/event_provider.dart';
import 'package:wiki_events/explore/provider/explore_provider.dart';
import 'package:wiki_events/my_events/my_event.dart';
import 'package:wiki_events/profile/profile.dart';
import 'package:wiki_events/router/provider/router.dart';
import 'package:wiki_events/utils/loader.dart';

class WikiEventsApp extends StatefulWidget {
  const WikiEventsApp({super.key});

  @override
  State<WikiEventsApp> createState() => _WikiEventsAppState();
}

class _WikiEventsAppState extends State<WikiEventsApp> {
  late ThemeProvider _themeProvider;
  late AppRouter _appRouter;
  late AppStateServiceProvider _appStateServiceProvider;
  late AuthServiceProvider _authServiceProvider;
  late StreamSubscription<User?> _authSubscription;
  late ProfileProvider _profileProvider;
  late ExploreProvider _exploreProvider;
  late EventProvider _eventProvider;
  late MyEventProvider _myEventProvider;

  @override
  void initState() {
    _themeProvider = ThemeProvider();
    _appStateServiceProvider = AppStateServiceProvider.instance;
    _appRouter = AppRouter(_appStateServiceProvider);
    _authServiceProvider = AuthServiceProvider.instance;
    _authSubscription =
        _authServiceProvider.onAuthStateChanged.listen(onAuthStateChanged);
    _profileProvider = ProfileProvider.instance;
    _exploreProvider = ExploreProvider.instance;
    _eventProvider = EventProvider.instance;
    _myEventProvider = MyEventProvider.instance;
    super.initState();
  }

  void onAuthStateChanged(User? user) {
    _appStateServiceProvider.currentUser = user;
  }

  @override
  void dispose() {
    _authSubscription.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ThemeProvider>(create: (_) => _themeProvider),
        ChangeNotifierProvider<AppStateServiceProvider>(
            create: (_) => _appStateServiceProvider),
        ChangeNotifierProvider<AuthServiceProvider>(
            create: (_) => _authServiceProvider),
        ChangeNotifierProvider<ProfileProvider>(
            create: (_) => _profileProvider),
        ChangeNotifierProvider<ExploreProvider>(
            create: (_) => _exploreProvider),
        ChangeNotifierProvider<EventProvider>(create: (_) => _eventProvider),
        ChangeNotifierProvider<MyEventProvider>(
            create: (_) => _myEventProvider),
      ],
      builder: (context, child) => MaterialApp.router(
        debugShowCheckedModeBanner: false,
        title: 'Wiki Events',
        themeMode: ThemeMode.light,
        theme: context.watch<ThemeProvider>().theme,
        routerConfig: _appRouter.goRouter,
        builder: (BuildContext context, Widget? child) =>
            AppLoader(child: child),
      ),
    );
  }
}
