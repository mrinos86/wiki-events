import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:wiki_events/utils/colors.dart';
import 'package:wiki_events/utils/text_style.dart';

class ThemeProvider with ChangeNotifier {
  ThemeData get theme => _getThemeData();

  ThemeData _getThemeData() {
    return ThemeData(
      scaffoldBackgroundColor: backgroundColor,
      brightness: Brightness.light,
      fontFamily: GoogleFonts.poppins().fontFamily,
      useMaterial3: true,
      colorScheme: ColorScheme.fromSeed(
        brightness: Brightness.light,
        seedColor: primaryColor,
        primary: primaryColor,
        onPrimary: colorWhite,
        primaryContainer: backgroundColor,
        onPrimaryContainer: colorBlack,
        surface: colorWhite,
        onSurface: onSurfaceColor,
        onBackground: onBackGround,
        background: backgroundColor,
        outline: primaryColor,
        surfaceTint: backgroundColor,
      ),
      elevatedButtonTheme: ElevatedButtonThemeData(
        style: ElevatedButton.styleFrom(
          backgroundColor: primaryColor,
          foregroundColor: onSurfaceColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
          minimumSize: const Size(double.infinity, 48),
          textStyle: size14_M_medium(),
        ),
      ),
      outlinedButtonTheme: OutlinedButtonThemeData(
        style: OutlinedButton.styleFrom(
          backgroundColor: backgroundColor,
          foregroundColor: onSurfaceColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(12),
          ),
          side: const BorderSide(color: primaryColor),
          padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 8),
          minimumSize: const Size(double.infinity, 48),
          textStyle: size14_M_medium(),
        ),
      ),
      appBarTheme: AppBarTheme(
        elevation: 0,
        color: Colors.transparent,
        centerTitle: true,
        iconTheme: const IconThemeData(
          color: onBackGround,
        ),
        titleTextStyle: size16_M_medium(),
        systemOverlayStyle: const SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark,
          statusBarBrightness: Brightness.light,
          systemNavigationBarColor: Colors.transparent,
          systemNavigationBarIconBrightness: Brightness.dark,
        ),
      ),
      inputDecorationTheme: InputDecorationTheme(
        isDense: true,
        floatingLabelBehavior: FloatingLabelBehavior.never,
        filled: true,
        fillColor: colorWhite,
        hintStyle: size14_M_regular(textColor: colorGrey),
        labelStyle: size14_M_regular(textColor: colorGrey),
        border: OutlineInputBorder(
            borderSide:
                const BorderSide(color: textFieldOutlineColor, width: 1.0),
            borderRadius: BorderRadius.circular(8),
            gapPadding: 6.0),
        outlineBorder:
            const BorderSide(color: textFieldOutlineColor, width: 1.0),
        enabledBorder: OutlineInputBorder(
            borderSide:
                const BorderSide(color: textFieldOutlineColor, width: 1.0),
            borderRadius: BorderRadius.circular(8),
            gapPadding: 6.0),
        focusedBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: primaryColor, width: 1.0),
            borderRadius: BorderRadius.circular(8),
            gapPadding: 6.0),
        errorBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: colorRed, width: 1.0),
            borderRadius: BorderRadius.circular(8),
            gapPadding: 6.0),
        disabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: disabledBorderGrey, width: 1.0),
            borderRadius: BorderRadius.circular(8),
            gapPadding: 6.0),
      ),
      floatingActionButtonTheme: const FloatingActionButtonThemeData(
        foregroundColor: colorWhite,
        backgroundColor: backgroundColor,
      ),
      textTheme: GoogleFonts.poppinsTextTheme(),
      primaryTextTheme: GoogleFonts.poppinsTextTheme(),
    );
  }
}
