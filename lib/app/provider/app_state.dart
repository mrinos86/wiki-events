import 'package:flutter/material.dart';
import 'package:wiki_events/auth/models/user.dart';

class AppStateServiceProvider with ChangeNotifier {
  static AppStateServiceProvider? _instance;

  AppStateServiceProvider._() {
    _init();
  }

  static AppStateServiceProvider get instance {
    if (_instance == null) {
      throw Exception("AppState Service Provider not initialized");
    }
    return _instance!;
  }

  bool _isInitialized = false;
  bool _isLoading = false;
  User? _currentUser;
  bool _isStartWithNotification = false;

  static void initialize() {
    if (_instance == null) {
      _instance = AppStateServiceProvider._();
    } else {
      throw Exception("AppState Service provider already initialized");
    }
  }

  void _init() {}

  bool get isInitialized => _isInitialized;
  bool get isLoggedIn => _currentUser != null;
  bool get isEmailVerified =>
      _currentUser?.emailVerifiedAt != null ? true : false;
  bool get isProfileCompleted => _currentUser?.firstName != null ? true : false;
  bool get isLoading => _isLoading;
  User? get currentUser => _currentUser;
  bool get isStartWithNotification => _isStartWithNotification;

  set isInitialized(bool value) {
    _isInitialized = value;
    notifyListeners();
  }

  set currentUser(User? user) {
    _currentUser = user;
    notifyListeners();
  }

  set isLoading(bool value) {
    _isLoading = value;
    notifyListeners();
  }

  set isStartWithNotification(bool value) {
    _isStartWithNotification = value;
    notifyListeners();
  }

  void get showLoader {
    _isLoading = true;
    notifyListeners();
  }

  void get hideLoader {
    _isLoading = false;
    notifyListeners();
  }
}
