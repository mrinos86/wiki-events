import 'dart:async';
import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:wiki_events/app/provider/app_state.dart';
import 'package:wiki_events/auth/provider/authentication.dart';
import 'package:wiki_events/common/provider/device.dart';
import 'package:wiki_events/common/provider/shared_pref.dart';

Future<void> bootstrap(FutureOr<Widget> Function() builder) async {
  FlutterError.onError = (details) {
    log(details.exceptionAsString(), stackTrace: details.stack);
  };

  // Initialize Required Plugins and Services
  WidgetsFlutterBinding.ensureInitialized();
  await DeviceInfo.initialize();
  await StorageService.initialize();
  AppStateServiceProvider.initialize();
  AuthServiceProvider.initialize();

  // Run the app in a zone to catch all uncaught errors.
  await runZonedGuarded(
    () async => runApp(await builder()),
    (error, stackTrace) => log(error.toString(), stackTrace: stackTrace),
  );
}
