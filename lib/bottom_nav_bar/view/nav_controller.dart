import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class NavController extends StatefulWidget {
  final Widget? child;
  const NavController({
    Key? key,
    this.child,
  }) : super(key: key);

  @override
  State<NavController> createState() => _NavControllerState();
}

class _NavControllerState extends State<NavController> {
  int onRouteChangeCurrentIndex() {
    const String bottomNavPrefix = '/bottomNav';
    final location = GoRouter.of(context).location;
    if (location
        .startsWith(Pages.explore.toPath(pathPrefix: bottomNavPrefix))) {
      return 0;
    } else if (location
        .startsWith(Pages.saved.toPath(pathPrefix: bottomNavPrefix))) {
      return 1;
    } else if (location
        .startsWith(Pages.myEvents.toPath(pathPrefix: bottomNavPrefix))) {
      return 2;
    } else if (location
        .startsWith(Pages.notifications.toPath(pathPrefix: bottomNavPrefix))) {
      return 3;
    } else if (location
        .startsWith(Pages.more.toPath(pathPrefix: bottomNavPrefix))) {
      return 4;
    } else {
      return 0;
    }
  }

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
      animation: GoRouter.of(context),
      builder: (context, child) {
        return Scaffold(
          extendBody: true,
          body: widget.child,
          bottomNavigationBar: ClipRRect(
            borderRadius: const BorderRadius.only(
              topRight: Radius.circular(14),
              topLeft: Radius.circular(14),
            ),
            child: BottomNavigationBar(
              selectedItemColor: darkGreyColor,
              unselectedItemColor: Colors.grey,
              showSelectedLabels: true,
              showUnselectedLabels: true,
              selectedLabelStyle: size11_M_medium(),
              unselectedLabelStyle: size11_M_regular(),
              currentIndex: onRouteChangeCurrentIndex(),
              items: const [
                BottomNavigationBarItem(
                  icon: Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Icon(
                      Icons.travel_explore_outlined,
                      size: 25,
                    ),
                  ),
                  label: 'Explore',
                ),
                BottomNavigationBarItem(
                  icon: Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Icon(
                      Icons.book,
                      size: 25,
                    ),
                  ),
                  label: 'Saved',
                ),
                BottomNavigationBarItem(
                  icon: Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Icon(
                      Icons.calendar_today_sharp,
                      size: 25,
                    ),
                  ),
                  label: 'My Events',
                ),
                BottomNavigationBarItem(
                  icon: Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Icon(
                      Icons.notifications,
                      size: 25,
                    ),
                  ),
                  label: 'Notifications',
                ),
                BottomNavigationBarItem(
                  icon: Padding(
                    padding: EdgeInsets.only(bottom: 5),
                    child: Icon(
                      Icons.more_vert,
                      size: 25,
                    ),
                  ),
                  label: 'More',
                ),
              ],
              onTap: (index) {
                if (index == 0) {
                  Pages.explore.push(context);
                } else if (index == 1) {
                  Pages.saved.push(context);
                } else if (index == 2) {
                  Pages.myEvents.push(context);
                } else if (index == 3) {
                  Pages.notifications.push(context);
                } else if (index == 4) {
                  Pages.more.push(context);
                }
              },
            ),
          ),
        );
      },
    );
  }
}
