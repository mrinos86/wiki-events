import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

String? displayOnlyDate(DateTime? date,
    {String format = 'yyyy-MM-dd', bool toLocal = true}) {
  if (date != null) {
    if (toLocal) {
      date = date.toLocal();
    }
    return DateFormat(format).format(date);
  } else {
    return null;
  }
}

String? displayOnlyTime(TimeOfDay? time,
    {String format = 'hh:mm a', bool toLocal = true}) {
  if (time != null) {
    DateTime today = DateTime.now();
    if (toLocal) {
      today = today.toLocal();
    }
    DateTime displayDateTime =
        DateTime(today.year, today.month, today.day, time.hour, time.minute);

    return DateFormat(format).format(displayDateTime);
  } else {
    return null;
  }
}
