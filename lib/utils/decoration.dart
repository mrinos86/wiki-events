// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:wiki_events/utils/utils.dart';

BoxShadow containerShadow = BoxShadow(
  color: Colors.grey.withOpacity(0.5),
  blurRadius: 8,
  offset: const Offset(0, 1),
);

BoxDecoration common_dec(
        {Color? color, required double borderRadius, BoxShadow? boxShadow}) =>
    BoxDecoration(
      color: color ?? colorWhite,
      borderRadius: BorderRadius.circular(borderRadius),
      boxShadow: boxShadow != null ? [boxShadow] : [],
    );

BoxDecoration common_border_dec(
        {Color? color,
        required double borderRadius,
        Color? borderColor,
        double? borderWidth,
        BoxShadow? boxShadow}) =>
    BoxDecoration(
      color: color ?? colorWhite,
      borderRadius: BorderRadius.circular(borderRadius),
      border: Border.all(
          color: borderColor ?? Colors.transparent, width: borderWidth ?? 1),
      boxShadow: boxShadow != null ? [boxShadow] : [],
    );
