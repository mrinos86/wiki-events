import 'package:wiki_events/utils/utils.dart';

extension MapIconExtension on MapIcons {
  String? getMapNormalIcons() {
    String? value;
    switch (this) {
      case MapIcons.comedy:
        value = AppAssets.MAP_COMEDY_NORMAL;
        break;
      case MapIcons.community:
        value = AppAssets.MAP_COMMUNITY_NORMAL;
        break;
      case MapIcons.countryEvents:
        value = AppAssets.MAP_COUNTRY_EVENTS_NORMAL;
        break;
      case MapIcons.culturalVisualArts:
        value = AppAssets.MAP_CULTURAL_VISUAL_ARTS_NORMAL;
        break;
      case MapIcons.exposShows:
        value = AppAssets.MAP_EXPOS_SHOWS_NORMAL;
        break;
      case MapIcons.filmsMedia:
        value = AppAssets.MAP_FILMS_MEDIA_NORMAL;
        break;
      case MapIcons.foodDrink:
        value = AppAssets.MAP_FOOD_DRINKS_NORMAL;
        break;
      case MapIcons.music:
        value = AppAssets.MAP_MUSIC_NORMAL;
        break;
      case MapIcons.scienceTech:
        value = AppAssets.MAP_SCIENCE_TECH_NORMAL;
        break;
      case MapIcons.sports:
        value = AppAssets.MAP_SPORTS_NORMAL;
        break;
      case MapIcons.map:
        value = AppAssets.MAP_NORMAL;
        break;
    }
    return value;
  }

  String? getMapSelectedIcons() {
    String? value;
    switch (this) {
      case MapIcons.comedy:
        value = AppAssets.MAP_COMEDY_SELECTED;
        break;
      case MapIcons.community:
        value = AppAssets.MAP_COMMUNITY_SELECTED;
        break;
      case MapIcons.countryEvents:
        value = AppAssets.MAP_COUNTRY_EVENTS_SELECTED;
        break;
      case MapIcons.culturalVisualArts:
        value = AppAssets.MAP_CULTURAL_VISUAL_ARTS_SELECTED;
        break;
      case MapIcons.exposShows:
        value = AppAssets.MAP_EXPOS_SHOWS_SELECTED;
        break;
      case MapIcons.filmsMedia:
        value = AppAssets.MAP_FILMS_MEDIA_SELECTED;
        break;
      case MapIcons.foodDrink:
        value = AppAssets.MAP_FOOD_DRINKS_SELECTED;
        break;
      case MapIcons.music:
        value = AppAssets.MAP_MUSIC_SELECTED;
        break;
      case MapIcons.scienceTech:
        value = AppAssets.MAP_SCIENCE_TECH_SELECTED;
        break;
      case MapIcons.sports:
        value = AppAssets.MAP_SPORTS_SELECTED;
        break;
      case MapIcons.map:
        value = AppAssets.MAP_SELECTED;
        break;
    }
    return value;
  }
}
