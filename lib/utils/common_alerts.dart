import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:wiki_events/utils/text_style.dart';

showOKCancelDialog(
  context, {
  required String labelTitle,
  required String labelContent,
  String? labelCancel,
  String? labelYes,
  TextStyle? textStyleCancel,
  TextStyle? textStyleYes,
  Function? onPressedCancel,
  required Function() onPressedYes,
}) {
  showDialog(
    context: context,
    builder: (context) {
      return Theme(
        data: ThemeData.light(),
        child: CupertinoAlertDialog(
          title: Text(
            labelTitle,
            style: size17_M_medium(),
          ),
          content: Text(
            labelContent != ""
                ? labelContent
                : "Something went wrong\nPlease try again!",
            style: size12_M_regular(),
          ),
          actions: <Widget>[
            CupertinoDialogAction(
                isDefaultAction: true,
                onPressed: () {
                  onPressedCancel ?? Navigator.pop(context);
                },
                child: Text(
                  labelCancel ?? "Cancel",
                  style: textStyleCancel ??
                      size17_M_medium(textColor: Colors.blue),
                )),
            CupertinoDialogAction(
                isDefaultAction: true,
                onPressed: () {
                  onPressedYes();
                },
                child: Text(labelYes ?? "Yes",
                    style: textStyleYes ??
                        size17_M_medium(textColor: Colors.blue))),
          ],
        ),
      );
    },
  );
}

showOKDialog(
    {required context,
    required String labelTitle,
    required String labelContent,
    TextStyle? textStyleOk,
    Function()? onPressOk}) {
  showDialog(
    context: context,
    builder: (context) {
      return Theme(
        data: ThemeData.light(),
        child: CupertinoAlertDialog(
          title: Text(
            labelTitle,
            style: size17_M_medium(),
          ),
          content: Padding(
            padding: const EdgeInsets.only(top: 8),
            child: Text(
              labelContent != ""
                  ? labelContent
                  : "Something went wrong\nPlease try again!",
              style: size13_M_regular(),
            ),
          ),
          actions: <Widget>[
            CupertinoDialogAction(
                isDefaultAction: true,
                onPressed: onPressOk ??
                    () {
                      Navigator.of(context).pop();
                    },
                child: Text(
                  "OK",
                  style: textStyleOk ?? size17_M_medium(textColor: Colors.blue),
                )),
          ],
        ),
      );
    },
  );
}

void openSuccessDialog(
    {required BuildContext context,
    required String route,
    String? title,
    required String msg}) {
  showOKDialog(
      context: context,
      labelTitle: title ?? 'Alert!',
      labelContent: msg,
      onPressOk: () async {
        Navigator.pop(context);
        if (route.isNotEmpty) {
          Navigator.pushNamedAndRemoveUntil(
              context, route, (Route<dynamic> route) => false);
        } else {
          Navigator.pop(context);
        }
      });
}
