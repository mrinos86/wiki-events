import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:loading_animation_widget/loading_animation_widget.dart';
import 'package:wiki_events/app/provider/provider.dart';
import 'package:wiki_events/utils/utils.dart';

class AppLoader extends StatefulWidget {
  final Widget? child;

  const AppLoader({
    Key? key,
    this.child,
  }) : super(key: key);

  @override
  State<AppLoader> createState() => _AppLoaderState();
}

class _AppLoaderState extends State<AppLoader> with TickerProviderStateMixin {
  late AppStateServiceProvider _appStateProvider;
  late Animation<double> _animation;
  late AnimationController _animationController;
  bool showLoading = false;

  @override
  void initState() {
    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 500));
    _animation = Tween<double>(begin: 0, end: 1).animate(CurvedAnimation(
        parent: _animationController, curve: Curves.easeInOutCubic));
    _appStateProvider =
        Provider.of<AppStateServiceProvider>(context, listen: false);
    _appStateProvider
        .addListener(() => onLoadingStateChanged(_appStateProvider.isLoading));
    super.initState();
  }

  void onLoadingStateChanged(bool value) {
    if (mounted) {
      if (value && !_animationController.isAnimating) {
        setState(() {
          showLoading = true;
        });
        _animationController.forward();
      } else {
        _animationController
            .reverse()
            .then((value) => setState(() => showLoading = false));
      }
    }
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Positioned.fill(child: widget.child ?? const SizedBox()),
        if (showLoading)
          Positioned.fill(
            child: AnimatedBuilder(
              animation: _animation,
              builder: (context, child) {
                return Scaffold(
                  backgroundColor: Colors.transparent,
                  body: BackdropFilter(
                    filter: ImageFilter.blur(
                        sigmaX: _animation.value * 2,
                        sigmaY: _animation.value * 2),
                    child: Container(
                      decoration: BoxDecoration(
                        color: Colors.black
                            .withAlpha((_animation.value * 40).toInt()),
                      ),
                      child: Center(
                        child: FadeTransition(
                          opacity: _animation,
                          child: child,
                        ),
                      ),
                    ),
                  ),
                );
              },
              child: LoadingAnimationWidget.fourRotatingDots(
                  color: primaryColor,
                  size: ScreenSize.getWidth(context, 0.15)),
            ),
          ),
      ],
    );
  }
}

class OnPageLoader extends StatelessWidget {
  const OnPageLoader({super.key});

  @override
  Widget build(BuildContext context) {
    return BackdropFilter(
      filter: ImageFilter.blur(sigmaX: 5.0, sigmaY: 5.0),
      child: Container(
        clipBehavior: Clip.antiAlias,
        decoration: const BoxDecoration(
            borderRadius: BorderRadius.all(Radius.circular(10))),
        child: Center(
          child: LoadingAnimationWidget.fourRotatingDots(
              color: primaryColor, size: ScreenSize.getWidth(context, 0.15)),
        ),
      ),
    );
  }
}
