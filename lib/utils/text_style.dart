// ignore_for_file: non_constant_identifier_names

import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:wiki_events/utils/utils.dart';

TextStyle size08_M_semibold(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 8,
        height: height,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);

TextStyle size09_M_semibold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 9,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);

TextStyle size09_M_bold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 11,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w700);

TextStyle size10_M_regular300(
        {Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 10,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w300);

TextStyle size10_M_regular({Color? textColor, double? letterSpacing}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 10,
        letterSpacing: letterSpacing ?? 0.0,
        fontWeight: FontWeight.w400);

TextStyle size10_M_medium({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 10,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w500);

TextStyle size10_M_semibold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 10,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);

TextStyle size11_M_medium({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 11,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w500);

TextStyle size11_M_regular(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 11,
        height: height,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w400);

TextStyle size11_M_semibold(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 11,
        height: height,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);

TextStyle size11_M_bold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 11,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w700);

TextStyle size12_M_regular300(
        {Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 12,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w300);

TextStyle size12_M_regular(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 12,
        fontWeight: FontWeight.w400);

TextStyle size12_M_medium(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        height: height,
        fontSize: 12,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w500);

TextStyle size12_M_semibold(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 12,
        height: height,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);

TextStyle size12_M_bold(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 12,
        fontWeight: FontWeight.w700);

TextStyle size12_M_extraBold(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 12,
        fontWeight: FontWeight.w800);

TextStyle size13_M_light({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 13,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w400);

TextStyle size13_M_regular(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        letterSpacing: letterSpacing,
        height: height,
        fontWeight: FontWeight.w400,
        fontSize: 13);

TextStyle size13_M_medium(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        letterSpacing: letterSpacing,
        height: height,
        fontWeight: FontWeight.w500,
        fontSize: 13);

TextStyle size13_M_semiBold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 13,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);

TextStyle size13_M_bold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 13,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w700);

TextStyle size14_M_light({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 14,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w300);

TextStyle size14_M_regular(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 14,
        letterSpacing: letterSpacing,
        height: height,
        fontWeight: FontWeight.w400);

TextStyle size14_M_medium({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 14,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w500);

TextStyle size14_M_semibold(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 14,
        height: height,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);

TextStyle size14_M_bold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 14,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w700);

TextStyle size14_M_extraBold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 14,
        fontWeight: FontWeight.w800);

TextStyle size15_M_regular({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 15,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w400);

TextStyle size15_M_medium({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 15,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w500);

TextStyle size15_M_semibold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 15,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);

TextStyle size15_M_bold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 15,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w700);

TextStyle size15_M_extraBold(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 15,
        fontWeight: FontWeight.w800);

TextStyle size16_M_light({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 16,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w300);

TextStyle size16_M_regular({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 16,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w400);

TextStyle size16_M_medium({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 16,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w500);

TextStyle size16_M_semibold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 16,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);

TextStyle size16_M_bold({Color? textColor, double? letterSpacing}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 16,
        letterSpacing: letterSpacing ?? 0.0,
        fontWeight: FontWeight.w700);

TextStyle size16_M_extraBold({Color? textColor}) => GoogleFonts.poppins(
    color: textColor ?? colorBlack, fontSize: 16, fontWeight: FontWeight.w800);

TextStyle size17_M_regular({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 17,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w400);

TextStyle size17_M_medium({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 17,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w500);

TextStyle size17_M_semibold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 17,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);

TextStyle size17_M_bold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 17,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w700);

TextStyle size18_M_regular({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 18,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w400);

TextStyle size18_M_medium({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 18,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w500);

TextStyle size18_M_semiBold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 18,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);

TextStyle size18_M_bold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 18,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w700);

TextStyle size19_M_semibold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 19,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);

TextStyle size20_M_regular({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 20,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w400);

TextStyle size20_M_medium({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 20,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w500);

TextStyle size20_M_semibold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 20,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);

TextStyle size20_M_bold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 20,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w700);

TextStyle size21_M_medium(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        letterSpacing: letterSpacing,
        height: height,
        fontWeight: FontWeight.w500,
        fontSize: 21);

TextStyle size22_M_regular({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 22,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w400);

TextStyle size22_M_medium(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        letterSpacing: letterSpacing,
        height: height,
        fontWeight: FontWeight.w500,
        fontSize: 22);

TextStyle size22_M_semibold(
        {Color? textColor, double? letterSpacing = 0.0, double? height}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        letterSpacing: letterSpacing,
        height: height,
        fontWeight: FontWeight.w600,
        fontSize: 22);

TextStyle size22_M_bold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 22,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w700);

TextStyle size24_M_regular({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
      color: textColor ?? colorBlack,
      fontSize: 24,
      letterSpacing: letterSpacing,
      fontWeight: FontWeight.w400,
    );

TextStyle size24_M_medium({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 24,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w500);

TextStyle size24_M_semibold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 24,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);

TextStyle size24_M_bold({Color? textColor, double? letterSpacing}) =>
    GoogleFonts.poppins(
      color: textColor ?? colorBlack,
      fontSize: 24,
      letterSpacing: letterSpacing ?? 0.0,
      fontWeight: FontWeight.w700,
    );

TextStyle size25_M_semibold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 25,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);

TextStyle size26_M_medium({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 26,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w500);

TextStyle size44_M_bold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 44,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w700);

TextStyle size45_M_semibold({Color? textColor, double? letterSpacing = 0.0}) =>
    GoogleFonts.poppins(
        color: textColor ?? colorBlack,
        fontSize: 45,
        letterSpacing: letterSpacing,
        fontWeight: FontWeight.w600);
