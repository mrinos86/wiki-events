// ignore_for_file: constant_identifier_names, non_constant_identifier_names

class AppAssets {
  AppAssets._();

  // Logo
  static const String LOGO = 'assets/logo/logo.png';

  static const String PROFILE_IMAGE = 'assets/images/ph_profile_image.png';
  static const String COMPLETE_PROFILE =
      'assets/images/img_complete_profile.png';

  static const String LIST_ICON = 'assets/images/list_icon.png';

  static const String EVENT_DISTANCE = 'assets/images/ic_event_distance.png';

  static const String CATEGORY_AUTO_BOAT_AIR =
      'assets/images/ic_categories_auto_boat_air.png';
  static const String CATEGORY_COMEDY =
      'assets/images/ic_categories_comedy.png';
  static const String CATEGORY_COMMUNITY =
      'assets/images/ic_categories_community.png';
  static const String CATEGORY_COUNTRY_EVENTS =
      'assets/images/ic_categories_country_events.png';
  static const String CATEGORY_EXPOSE_SHOWS =
      'assets/images/ic_categories_expos_shows.png';
  static const String CATEGORY_FILMS_MEDIA =
      'assets/images/ic_categories_films_media.png';
  static const String CATEGORY_FOOD_DRINK =
      'assets/images/ic_categories_food_drink.png';
  static const String CATEGORY_MUSIC = 'assets/images/ic_categories_music.png';
  static const String CATEGORY_PERFORM_VISUAL_ART =
      'assets/images/ic_categories_performing_visual_arts.png';
  static const String CATEGORY_SCIENCE_TECH =
      'assets/images/ic_categories_science_tech.png';
  static const String CATEGORY_SPORTS =
      'assets/images/ic_categories_sports.png';

  static const String FEATURE_ACCOMMODATION_NEARBY =
      'assets/images/ic_features_accommodation_nearby.png';
  static const String FEATURE_AGE_18 = 'assets/images/ic_features_age_18+.png';
  static const String FEATURE_AIR_CONDITION =
      'assets/images/ic_features_air_conditioned.png';
  static const String FEATURE_ASL_INTERPRETATION =
      'assets/images/ic_features_asl_Interpretation.png';
  static const String FEATURE_ATM = 'assets/images/ic_features_atm.png';
  static const String FEATURE_BOOKING_REQUIRED =
      'assets/images/ic_features_booking_required.png';
  static const String FEATURE_BYO_CHAIRS =
      'assets/images/ic_features_byo_chairs.png';
  static const String FEATURE_BYO_DRINKS =
      'assets/images/ic_features_byo_drinks.png';
  static const String FEATURE_BYO_FOODS =
      'assets/images/ic_features_byo_foods.png';
  static const String FEATURE_CAMPING_AVAILABLE =
      'assets/images/ic_features_camping_availble.png';
  static const String FEATURE_DOG_ALLOWED =
      'assets/images/ic_features_dog_allowed.png';
  static const String FEATURE_DOG_NOT_ALLOWED =
      'assets/images/ic_features_dog_not_allowed.png';
  static const String FEATURE_DONATION =
      'assets/images/ic_features_donation.png';
  static const String FEATURE_FAMILY_FRIENDLY =
      'assets/images/ic_features_family_friendly.png';
  static const String FEATURE_FOOD_STALL =
      'assets/images/ic_features_food_stall.png';
  static const String FEATURE_FREE = 'assets/images/ic_features_free.png';
  static const String FEATURE_INDIGENOUS_CULTURE =
      'assets/images/ic_features_indigenous_culture.png';
  static const String FEATURE_KIDS_ACTIVITY =
      'assets/images/ic_features_kids_activity.png';
  static const String FEATURE_LICENCED =
      'assets/images/ic_features_licenced.png';
  static const String FEATURE_MEDICAL_SERVICE =
      'assets/images/ic_features_medical_services.png';
  static const String FEATURE_OPTUS_COVERAGE =
      'assets/images/ic_features_optus_coverage.png';
  static const String FEATURE_OUTDOORS =
      'assets/images/ic_features_outdoors.png';
  static const String FEATURE_PAID_PARKING =
      'assets/images/ic_features_paid_parking.png';
  static const String FEATURE_PARKING = 'assets/images/ic_features_parking.png';
  static const String FEATURE_SECURITY =
      'assets/images/ic_features_security.png';
  static const String FEATURE_SENSOR_FRIENDLY =
      'assets/images/ic_features_sensory_friendly.png';
  static const String FEATURE_SHADED_COVERED =
      'assets/images/ic_features_shaded_covered.png';
  static const String FEATURE_SOCIAL_DISTANCE =
      'assets/images/ic_features_social_distance.png';
  static const String FEATURE_TELSTRA_COVERAGE =
      'assets/images/ic_features_telstra_coverage.png';
  static const String FEATURE_TOILETS = 'assets/images/ic_features_toilets.png';
  static const String FEATURE_WHEELCHAIR =
      'assets/images/ic_features_wheelchair.png';
  static const String FEATURE_WIFI_AVAILABLE =
      'assets/images/ic_features_wifi_availble.png';

  static const String MAP_COMEDY_NORMAL =
      'assets/images/ic_map_comedy_normal.png';
  static const String MAP_COMEDY_SELECTED =
      'assets/images/ic_map_comedy_selected.png';
  static const String MAP_COMMUNITY_NORMAL =
      'assets/images/ic_map_community_normal.png';
  static const String MAP_COMMUNITY_SELECTED =
      'assets/images/ic_map_community_selected.png';
  static const String MAP_COUNTRY_EVENTS_NORMAL =
      'assets/images/ic_map_country_events_normal.png';
  static const String MAP_COUNTRY_EVENTS_SELECTED =
      'assets/images/ic_map_country_events_selected.png';
  static const String MAP_CULTURAL_VISUAL_ARTS_NORMAL =
      'assets/images/ic_map_cultural_visual_arts_normal.png';
  static const String MAP_CULTURAL_VISUAL_ARTS_SELECTED =
      'assets/images/ic_map_cultural_visual_arts_selected.png';
  static const String MAP_EXPOS_SHOWS_NORMAL =
      'assets/images/ic_map_expos_shows_normal.png';
  static const String MAP_EXPOS_SHOWS_SELECTED =
      'assets/images/ic_map_expos_shows_selected.png';
  static const String MAP_FILMS_MEDIA_NORMAL =
      'assets/images/ic_map_films_media_normal.png';
  static const String MAP_FILMS_MEDIA_SELECTED =
      'assets/images/ic_map_films_media_selected.png';
  static const String MAP_FOOD_DRINKS_NORMAL =
      'assets/images/ic_map_food_drink_normal.png';
  static const String MAP_FOOD_DRINKS_SELECTED =
      'assets/images/ic_map_food_drink_selected.png';
  static const String MAP_MUSIC_NORMAL =
      'assets/images/ic_map_music_normal.png';
  static const String MAP_MUSIC_SELECTED =
      'assets/images/ic_map_music_selected.png';
  static const String MAP_NORMAL = 'assets/images/ic_map_normal.png';
  static const String MAP_SCIENCE_TECH_NORMAL =
      'assets/images/ic_map_science_tech_normal.png';
  static const String MAP_SCIENCE_TECH_SELECTED =
      'assets/images/ic_map_science_tech_selected.png';
  static const String MAP_SELECTED = 'assets/images/ic_map_selected.png';
  static const String MAP_SPORTS_NORMAL =
      'assets/images/ic_map_sports_normal.png';
  static const String MAP_SPORTS_SELECTED =
      'assets/images/ic_map_sports_selected.png';
}
