import 'package:flutter/material.dart';
import 'package:wiki_events/utils/utils.dart';

class CommonMoreScreenBox extends StatefulWidget {
  const CommonMoreScreenBox(
      {super.key, required this.icon, required this.label, this.onTap});

  final String label;
  final Icon icon;
  final Function()? onTap;

  @override
  State<CommonMoreScreenBox> createState() => _CommonMoreScreenBoxState();
}

class _CommonMoreScreenBoxState extends State<CommonMoreScreenBox> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15),
      child: OutlinedButton(
          onPressed: widget.onTap,
          child: Container(
            padding: const EdgeInsets.all(5.0),
            child: Row(children: [
              widget.icon,
              sizedBoxWidth(15),
              Text(
                widget.label,
                style: size14_M_medium(),
              )
            ]),
          )),
    );
  }
}
