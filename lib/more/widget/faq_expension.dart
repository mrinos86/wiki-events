import 'package:flutter/material.dart';
import 'package:wiki_events/utils/utils.dart';

class FAQExpension extends StatefulWidget {
  const FAQExpension({super.key, required this.title, required this.anwser});

  final String title;
  final String anwser;

  @override
  State<FAQExpension> createState() => _FAQExpensionState();
}

bool isClicked = true;

class _FAQExpensionState extends State<FAQExpension> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        InkWell(
          onTap: () {
            setState(() {
              isClicked = !isClicked;
            });
          },
          child: Container(
            padding: const EdgeInsets.all(10),
            decoration: common_border_dec(
                borderRadius: 8, borderColor: primaryColor, borderWidth: 0.5),
            child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(widget.title, style: size14_M_regular()),
                  Icon(
                      isClicked == true ? Icons.expand_more : Icons.expand_less,
                      color: primaryColor,
                      size: 24)
                ]),
          ),
        ),
        isClicked == false
            ? Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
                child: Text(widget.anwser, style: size14_M_regular()),
              )
            : const SizedBox(height: 0),
        sizedBoxHeight(15)
      ],
    );
  }
}
