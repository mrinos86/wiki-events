import 'package:flutter/material.dart';
import 'package:wiki_events/explore/explore.dart';
import 'package:wiki_events/utils/utils.dart';

class AppSettingsPage extends StatefulWidget {
  const AppSettingsPage({super.key});

  @override
  State<AppSettingsPage> createState() => _AppSettingsPageState();
}

class _AppSettingsPageState extends State<AppSettingsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColorECF4F1,
      appBar: AppBar(
        title: const Text("App Settings"),
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          child: const Icon(
            Icons.arrow_back_ios,
            size: 22,
          ),
          onTap: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Column(
        children: [
          sizedBoxHeight(20),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: FilterButtonWidget(
              title: "Delete Account",
              borderColor: colorGrey,
              onPressedCallBack: () async {
                await showOKCancelDialog(
                  context,
                  labelTitle: "Delete Account",
                  labelContent:
                      "Are you sure you want delete account?\nOnce done this cannot be undone",
                  labelYes: "Delete",
                  textStyleYes: size17_M_medium(textColor: errorColor),
                  onPressedYes: () {
                    Navigator.pop(context);
                  },
                );
              },
            ),
          ),
        ],
      ),
    );
  }
}
