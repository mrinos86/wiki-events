import 'package:flutter/material.dart';
import 'package:wiki_events/utils/utils.dart';

class PrivacyPolicyPage extends StatefulWidget {
  const PrivacyPolicyPage({super.key});

  @override
  State<PrivacyPolicyPage> createState() => _PrivacyPolicyPageState();
}

class _PrivacyPolicyPageState extends State<PrivacyPolicyPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColorECF4F1,
      appBar: AppBar(
        title: const Text("Privacy Policy"),
        automaticallyImplyLeading: false,
        leading: GestureDetector(
          child: const Icon(
            Icons.arrow_back_ios,
            size: 22,
          ),
          onTap: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16),
            child: Text(
                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis vel eros donec ac odio. Imperdiet sed euismod nisi porta. \n\nSagittis aliquam malesuada bibendum arcu vitae elementum curabitur vitae. Pellentesque nec nam aliquam sem et tortor. Etiam tempor orci eu lobortis elementum nibh tellus. Dis parturient montes nascetur ridiculus mus mauris vitae ultricies leo. Suspendisse in est ante in nibh mauris cursus. Ut etiam sit amet nisl. \n\nVelit aliquet sagittis id consectetur purus ut faucibus. Lacinia quis vel eros donec ac odio tempor. Quam nulla porttitor massa id neque aliquam vestibulum. Sit amet nisl suscipit adipiscing bibendum est. Pellentesque habitant morbi tristique senectus et netus et. Quis eleifend quam adipiscing vitae. Ridiculus mus mauris vitae ultricies leo integer. Scelerisque felis imperdiet proin fermentum leo.",
                style: size14_M_regular()),
          ),
        ],
      ),
    );
  }
}
