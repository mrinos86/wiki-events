import 'package:flutter/material.dart';
import 'package:wiki_events/app/app.dart';

import 'package:wiki_events/auth/auth.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/more/more.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class MorePage extends StatefulWidget {
  const MorePage({super.key});

  @override
  State<MorePage> createState() => _MorePageState();
}

class _MorePageState extends State<MorePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColorECF4F1,
      appBar: AppBar(
          title: const Text("More"),
          automaticallyImplyLeading: false,
          leading: null),
      body: Padding(
        padding:
            const EdgeInsets.only(bottom: 80, left: 16, right: 16, top: 16),
        child: Column(
          children: [
            CommonMoreScreenBox(
                icon: const Icon(Icons.account_circle,
                    size: 25, color: primaryColor),
                label: "Account",
                onTap: () {}),
            CommonMoreScreenBox(
                icon: const Icon(Icons.call, size: 25, color: primaryColor),
                label: "Contact Us",
                onTap: () {}),
            CommonMoreScreenBox(
                icon: const Icon(Icons.description,
                    size: 25, color: primaryColor),
                label: "Terms and Conditions",
                onTap: () {
                  Pages.termsAndConditions.go(context);
                }),
            CommonMoreScreenBox(
                icon: const Icon(Icons.shield, size: 25, color: primaryColor),
                label: "Privacy Policy",
                onTap: () {
                  Pages.privacyPolicy.go(context);
                }),
            CommonMoreScreenBox(
                icon: const Icon(Icons.info, color: primaryColor, size: 25),
                label: "About Us",
                onTap: () {
                  Pages.aboutUs.go(context);
                }),
            CommonMoreScreenBox(
                icon: const Icon(Icons.quiz, size: 25, color: primaryColor),
                label: "FAQ",
                onTap: () {
                  Pages.faqs.go(context);
                }),
            CommonMoreScreenBox(
                icon: const Icon(Icons.settings, size: 25, color: primaryColor),
                label: "App Settings",
                onTap: () {
                  Pages.appSettings.go(context);
                }),
            const Spacer(),
            CommonMoreScreenBox(
              icon: const Icon(Icons.logout, size: 25, color: Colors.red),
              label: "Logout",
              onTap: () async {
                AppStateServiceProvider.instance.showLoader;
                ApiResponse response =
                    await AuthServiceProvider.instance.signOut();
                AppStateServiceProvider.instance.hideLoader;
                if (!response.result && mounted) {
                  await showOKDialog(
                    context: context,
                    labelTitle: "Error !",
                    labelContent: "",
                  );
                }
              },
            ),
            sizedBoxHeight(15)
          ],
        ),
      ),
    );
  }
}
