import 'package:flutter/material.dart';
import 'package:wiki_events/explore/explore.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class SliderWidget extends StatefulWidget {
  bool isValueChanged;
  double currentValue;
  Function onChanged;

  SliderWidget(
      {super.key,
      required this.isValueChanged,
      required this.currentValue,
      required this.onChanged});

  @override
  State<SliderWidget> createState() => _SliderWidgetState();
}

class _SliderWidgetState extends State<SliderWidget> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 12, right: 12),
      child: SliderTheme(
        data: SliderTheme.of(context).copyWith(
            trackShape: CustomTrackShape(),
            trackHeight: 5,
            thumbShape: widget.isValueChanged
                ? PolygonSliderThumb(
                    thumbRadius: 15,
                    sliderValue: widget.currentValue,
                  )
                : const RoundSliderThumbShape(
                    enabledThumbRadius: 15,
                    pressedElevation: 12,
                  ),
            valueIndicatorShape: const RectangularSliderValueIndicatorShape(),
            showValueIndicator: ShowValueIndicator.always,
            valueIndicatorColor: colorGrey.withOpacity(0.3),
            valueIndicatorTextStyle: size13_M_medium()),
        child: Slider(
          max: 500,
          min: 0,
          divisions: 500,
          value: widget.currentValue,
          label: "${widget.currentValue.round()}",
          onChanged: (double value) {
            setState(() => widget.currentValue = value);
            widget.onChanged(value);
          },
          onChangeStart: (value) {
            setState(() => widget.isValueChanged = false);
          },
          onChangeEnd: (value) {
            setState(() => widget.isValueChanged = true);
          },
        ),
      ),
    );
  }
}
