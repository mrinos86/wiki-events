import 'package:flutter/material.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class SortButtonWidget extends StatelessWidget {
  String title;
  Function onPressedCallBack;
  bool isSelected;
  SortButtonWidget({
    super.key,
    required this.title,
    required this.onPressedCallBack,
    required this.isSelected,
  });

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: ElevatedButton.styleFrom(
          minimumSize: Size(ScreenSize.getWidth(context, 1), 50),
          side: const BorderSide(color: colorGrey),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          backgroundColor: colorWhite,
          foregroundColor: darkGreyColor),
      onPressed: () {
        onPressedCallBack();
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(title, style: size14_M_regular()),
          isSelected
              ? const Icon(
                  Icons.check_circle,
                  size: 25,
                  color: primaryColor,
                )
              : Container()
        ],
      ),
    );
  }
}

enum SortButtonEnums { popular, alphabetical, top, favourite }
