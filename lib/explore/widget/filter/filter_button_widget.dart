import 'package:flutter/material.dart';
import 'package:wiki_events/utils/utils.dart';

class FilterButtonWidget extends StatelessWidget {
  String title;
  String? subTitle;
  Function onPressedCallBack;
  Color? titleTextColor;
  Color? borderColor;
  double? borderWidth;
  FilterButtonWidget({
    super.key,
    required this.title,
    this.subTitle,
    required this.onPressedCallBack,
    this.titleTextColor,
    this.borderColor,
    this.borderWidth,
  });

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      style: ElevatedButton.styleFrom(
          minimumSize: Size(ScreenSize.getWidth(context, 1), 50),
          side: BorderSide(
            color: borderColor ?? colorGrey,
            width: borderWidth ?? 1,
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8),
          ),
          backgroundColor: colorWhite),
      onPressed: () {
        onPressedCallBack();
      },
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(title,
              style: size14_M_regular(textColor: titleTextColor ?? colorBlack)),
          subTitle != null
              ? Row(
                  children: [
                    Text(
                      'Relevance',
                      style: size14_M_regular(textColor: colorGrey),
                    ),
                    sizedBoxWidth(8),
                    const Icon(
                      Icons.navigate_next,
                      size: 25,
                      color: primaryColor,
                    )
                  ],
                )
              : const Icon(
                  Icons.navigate_next,
                  size: 25,
                  color: primaryColor,
                )
        ],
      ),
    );
  }
}
