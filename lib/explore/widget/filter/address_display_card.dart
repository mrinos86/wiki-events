import 'package:flutter/material.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class AddressDisplayCard extends StatelessWidget {
  List<DropdownItems> list;
  Function removeButtonPressed;
  AddressDisplayCard({
    super.key,
    required this.list,
    required this.removeButtonPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: common_border_dec(borderRadius: 8, borderColor: colorGrey),
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: GridView.builder(
            shrinkWrap: true,
            itemCount: list.length,
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                mainAxisSpacing: 8,
                mainAxisExtent: 45,
                crossAxisSpacing: 8,
                crossAxisCount: 2),
            itemBuilder: (_, index) {
              return Container(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                decoration: common_dec(borderRadius: 8, color: primaryColor),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Text(list[index].title,
                          style: size12_M_semibold(textColor: colorWhite)),
                    ),
                    InkWell(
                      onTap: () {
                        removeButtonPressed(index);
                      },
                      child:
                          const Icon(Icons.cancel, color: colorWhite, size: 20),
                    )
                  ],
                ),
              );
            }),
      ),
    );
  }
}
