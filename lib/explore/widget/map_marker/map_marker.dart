// ignore_for_file: implementation_imports

import 'package:flutter/material.dart';
import 'package:wiki_events/events/model/selectable_model.dart';
import 'package:wiki_events/utils/utils.dart';

class MapMarker extends StatelessWidget {
  final ExploreSelectableEventModel exploreEvent;
  const MapMarker(this.exploreEvent, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        // HttpExceptionNotifyUser.showMessage('tapped');
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.all(8),
                decoration: exploreEvent.isSelected
                    ? BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: primaryColor, width: 2.5),
                        color: colorWhite.withOpacity(0.4))
                    : const BoxDecoration(shape: BoxShape.circle),
                child: ClipRRect(
                  child: Column(
                    children: [
                      if (exploreEvent.event.id?.isNotEmpty ?? false)
                        ClipRRect(
                          borderRadius: BorderRadius.circular(14),
                          child: Image.network(
                            exploreEvent.isSelected
                                ? exploreEvent
                                    .event.eventTypes![0].imageUrlSelect!
                                : exploreEvent
                                    .event.eventTypes![0].imageUrlNormal!,
                            // post.images!.first.imageThumbPath!,
                            height: 45,
                            width: 40,
                            fit: BoxFit.fitWidth,
                            loadingBuilder: (BuildContext context, Widget child,
                                ImageChunkEvent? loadingProgress) {
                              if (loadingProgress == null) return child;
                              return Center(
                                child: CircularProgressIndicator(
                                  value: loadingProgress.expectedTotalBytes !=
                                          null
                                      ? loadingProgress.cumulativeBytesLoaded /
                                          loadingProgress.expectedTotalBytes!
                                      : null,
                                ),
                              );
                            },
                          ),
                        ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}

class CustomClipPath extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width / 3, 0.0);
    path.lineTo(size.width / 2, size.height / 3);
    path.lineTo(size.width - size.width / 3, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
