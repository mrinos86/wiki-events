import 'package:flutter/material.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class CustomAppBar extends StatelessWidget {
  String title;
  Widget? leadingWidget;
  Widget? actionWidget;
  CustomAppBar(
      {Key? key, required this.title, this.leadingWidget, this.actionWidget})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.topCenter,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          leadingWidget ??
              const SizedBox(
                width: 22,
              ),
          Text(title, style: size16_M_medium(textColor: onBackGround)),
          actionWidget ??
              const SizedBox(
                width: 22,
              ),
        ],
      ),
    );
  }
}
