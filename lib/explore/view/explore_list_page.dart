import 'package:flutter/material.dart';
import 'package:ionicons/ionicons.dart';
import 'package:wiki_events/app/app.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/explore/provider/explore_provider.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class ExploreListPage extends StatefulWidget {
  const ExploreListPage({super.key});

  @override
  State<ExploreListPage> createState() => _ExploreListPageState();
}

class _ExploreListPageState extends State<ExploreListPage> {
  final TextEditingController _searchEditingController =
      TextEditingController();

  late ExploreProvider exploreProvider;

  List<EventModel> eventList = [];

  @override
  void initState() {
    loadEventList();
    super.initState();
  }

  Future<void> loadEventList() async {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      exploreProvider = Provider.of<ExploreProvider>(context, listen: false);
      AppStateServiceProvider.instance.showLoader;
      await exploreProvider.getEventList();
      AppStateServiceProvider.instance.hideLoader;
    });
  }

  @override
  Widget build(BuildContext context) {
    exploreProvider = Provider.of<ExploreProvider>(context, listen: true);
    eventList = exploreProvider.eventList;
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: bgColorECF4F1,
        appBar: AppBar(
          title: const Text('Explore'),
          leading: Container(),
          actions: [
            GestureDetector(
              onTap: () {
                Pages.explore.push(context);
              },
              child: Row(children: [
                const Icon(
                  Icons.map,
                  size: 25,
                ),
                sizedBoxWidth(15),
              ]),
            )
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: TextFieldPrefixImage(
                        hintText: "Start typing here",
                        controller: _searchEditingController,
                        validator: (String? value) {},
                        contentPadding: const EdgeInsets.all(11),
                        prefixIcon: const Padding(
                          padding: EdgeInsets.only(right: 10, left: 10),
                          child: Icon(
                            Icons.search,
                            size: 20,
                          ),
                        ),
                        onChanged: (value) {},
                        onFieldSubmitted: (value) {}),
                  ),
                  GestureDetector(
                    onTap: () {
                      Pages.exploreFilter.push(context);
                    },
                    child: Container(
                      decoration: common_border_dec(
                          borderRadius: 10,
                          color: primaryColor,
                          borderColor: primaryColor,
                          boxShadow: containerShadow),
                      alignment: Alignment.center,
                      margin: const EdgeInsets.only(left: 5),
                      padding: const EdgeInsets.all(8),
                      child: const Icon(
                        Icons.tune_rounded,
                        color: colorBlack,
                        size: 25,
                      ),
                    ),
                  ),
                ],
              ),
              sizedBoxHeight(8),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                      padding: const EdgeInsets.all(7),
                      decoration: common_border_dec(
                          borderRadius: 8, color: primaryColor),
                      child: Row(
                        children: [
                          const Icon(Ionicons.navigate,
                              color: grey495057, size: 20),
                          const SizedBox(
                            height: 22,
                            child: VerticalDivider(
                              color: grey495057,
                              thickness: 0.25,
                            ),
                          ),
                          Text("Melbourne, Australia",
                              style: size13_M_medium(textColor: grey495057)),
                          const Icon(Icons.navigate_next,
                              color: grey495057, size: 22),
                        ],
                      )),
                  // sizedBoxWidth(5),
                  Container(
                      padding: const EdgeInsets.all(7),
                      decoration: common_border_dec(
                        color: Colors.white.withOpacity(0.6),
                        borderColor: darkGreyColor,
                        borderRadius: 8,
                      ),
                      child: Row(
                        children: [
                          const Icon(Icons.add, color: darkGreyColor, size: 22),
                          sizedBoxWidth(3),
                          Text("50km",
                              style: size14_M_medium(textColor: darkGreyColor)),
                          sizedBoxWidth(3),
                          const Icon(Icons.remove,
                              color: darkGreyColor, size: 22),
                        ],
                      )),
                ],
              ),
              eventList.isNotEmpty
                  ? Expanded(
                      child: Container(
                        padding: const EdgeInsets.only(top: 10),
                        child: ListView.builder(
                            padding: const EdgeInsets.only(bottom: 85),
                            shrinkWrap: true,
                            itemCount: eventList.length,
                            itemBuilder: (context, i) {
                              return Padding(
                                padding: const EdgeInsets.only(bottom: 12),
                                child: EventDetailCard(
                                  event: eventList[i],
                                  callBackViewEventButton: (String id) {
                                    Pages.viewEvent.push(
                                      context,
                                      extra: ViewEventPageParam(
                                        viewEventFrom: ViewEventFrom.explore,
                                        eventId: int.parse(id),
                                      ),
                                    );
                                  },
                                ),
                              );
                            }),
                      ),
                    )
                  : Padding(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height / 3.5),
                      child: Center(
                        child: Text(
                          "No event available",
                          style: size13_M_regular(),
                        ),
                      ),
                    )
            ],
          ),
        ),
      ),
    );
  }
}
