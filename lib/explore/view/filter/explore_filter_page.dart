import 'package:flutter/material.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/explore/explore.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class ExploreFilterPage extends StatefulWidget {
  const ExploreFilterPage({super.key});

  @override
  State<ExploreFilterPage> createState() => _ExploreFilterPageState();
}

class _ExploreFilterPageState extends State<ExploreFilterPage> {
  double currentValue = 50;

  bool isSliderValueChanged = true;

  DropdownItems? _currentSelectedValue;

  List<DropdownItems> dropdownItemList = [
    DropdownItems(id: 1, title: "New South Wales"),
    DropdownItems(id: 2, title: "Victoria"),
    DropdownItems(id: 3, title: "Queensland"),
    DropdownItems(id: 4, title: "South Australia"),
    DropdownItems(id: 5, title: "Western Australia"),
    DropdownItems(id: 6, title: "Tasmania"),
    DropdownItems(id: 7, title: "Northern Territory"),
    DropdownItems(id: 8, title: "Australian Capital Territory"),
  ];

  List<DropdownItems> selectedCountryList = [];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColorECF4F1,
      appBar: AppBar(
        title: const Text('Filter'),
        leading: GestureDetector(
          child: const Icon(
            Icons.arrow_back_ios,
            size: 22,
          ),
          onTap: () {
            Navigator.of(context).pop();
          },
        ),
        actions: [
          GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
              //clear all
            },
            child: Row(children: [
              InkWell(
                child: Text("Clear All", style: size15_M_regular()),
                onTap: () {},
              ),
              sizedBoxWidth(15),
            ]),
          )
        ],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          width: double.infinity,
          child: Column(
            children: [
              FilterButtonWidget(
                title: "Sort By",
                subTitle: "Relevance",
                onPressedCallBack: () {
                  Pages.sortOption.push(context);
                },
              ),
              sizedBoxHeight(15),
              Container(
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.all(10),
                decoration:
                    common_border_dec(borderRadius: 8, borderColor: colorGrey),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Distance (km)",
                      style: size14_M_regular(),
                    ),
                    sizedBoxHeight(isSliderValueChanged ? 35 : 35),
                    SliderWidget(
                      currentValue: currentValue,
                      isValueChanged: isSliderValueChanged,
                      onChanged: (value) {
                        setState(() {
                          currentValue = value;
                        });
                      },
                    ),
                  ],
                ),
              ),
              sizedBoxHeight(15),
              FilterButtonWidget(
                title: "Event Type",
                onPressedCallBack: () {
                  Pages.eventType.push(context, extra: []);
                },
              ),
              sizedBoxHeight(15),
              FilterButtonWidget(
                title: "Event Feature",
                onPressedCallBack: () {
                  Pages.eventFeatures.push(context, extra: []);
                },
              ),
              sizedBoxHeight(15),
              CommonDropdown(
                dropdownList: dropdownItemList,
                currentItem: _currentSelectedValue,
                hintText: "State",
                hintStyle: size14_M_regular(),
                dropdownMenuHeight: 300,
                onChangedCallBack: (DropdownItems? newValue) async {
                  if (selectedCountryList.isNotEmpty) {
                    {
                      bool canAdd = true;
                      for (var element in selectedCountryList) {
                        if (element.id == newValue!.id) {
                          await showOKDialog(
                            context: context,
                            labelTitle: "Alert !",
                            labelContent: "This state is already selected",
                          );
                          canAdd = false;
                          break;
                        }
                      }
                      if (canAdd) {
                        setState(() => selectedCountryList.add(newValue!));
                      }
                    }
                  } else {
                    setState(() => selectedCountryList.add(newValue!));
                  }
                },
              ),
              sizedBoxHeight(15),
              selectedCountryList.isNotEmpty
                  ? AddressDisplayCard(
                      list: selectedCountryList,
                      removeButtonPressed: (value) {
                        setState(() {
                          selectedCountryList.removeAt(value);
                        });
                      },
                    )
                  : Container(),
              sizedBoxHeight(40),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(ScreenSize.getWidth(context, 0.6), 48),
                ),
                onPressed: () async {},
                child: const Text('Filter Results'),
              ),
              sizedBoxHeight(50),
            ],
          ),
        ),
      ),
    );
  }
}
