import 'package:flutter/material.dart';
import 'package:wiki_events/explore/explore.dart';
import 'package:wiki_events/utils/utils.dart';
import 'package:intl/intl.dart';

class SortOptionPage extends StatefulWidget {
  const SortOptionPage({super.key});

  @override
  State<SortOptionPage> createState() => _SortOptionPageState();
}

class _SortOptionPageState extends State<SortOptionPage> {
  List<String> timeList = ["Today", "Tomorrow", "Next week", "Next month"];
  List<String> checkedTimes = [];

  List<String> dateSortType = [
    "Post date from Newest to Oldest",
    "Post date from Oldest to Newest"
  ];

  late String selectedDateSortType;

  DateTime? pickedFromDate = DateTime.now();
  String formattedFromDate = "Select Date";

  DateTime? pickedToDate = DateTime.now();
  String formattedToDate = "Select Date";

  SortButtonEnums selectedSortOption = SortButtonEnums.popular;

  @override
  void initState() {
    // TODO: implement initState
    selectedDateSortType = dateSortType[0];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColorECF4F1,
      appBar: AppBar(
        title: const Text('Sort'),
        leading: GestureDetector(
          child: const Icon(
            Icons.arrow_back_ios,
            size: 22,
          ),
          onTap: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 16),
          width: double.infinity,
          child: Column(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                padding: const EdgeInsets.all(12),
                decoration:
                    common_border_dec(borderRadius: 8, borderColor: colorGrey),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Events Happening",
                      style: size16_M_semibold(textColor: primaryColor),
                    ),
                    sizedBoxHeight(8),
                    ListView.builder(
                      shrinkWrap: true,
                      padding: EdgeInsets.zero,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: timeList.length,
                      itemBuilder: (context, i) {
                        return Padding(
                          padding: const EdgeInsets.only(top: 8),
                          child: Row(
                            children: [
                              Transform.scale(
                                scale: 1,
                                child: Checkbox(
                                  visualDensity: const VisualDensity(
                                    vertical: VisualDensity.minimumDensity,
                                  ),
                                  activeColor: colorBlack,
                                  value: checkedTimes.contains(timeList[i]),
                                  onChanged: (value) {
                                    setState(() {
                                      _onSelected(value!, timeList[i]);
                                    });
                                  },
                                ),
                              ),
                              sizedBoxWidth(3),
                              Text(
                                timeList[i],
                                style: size14_M_regular(),
                              ),
                            ],
                          ),
                        );
                      },
                    ),
                    sizedBoxHeight(8),
                    dividerView(color: colorGrey),
                    Column(
                      children: [
                        ListView.builder(
                          padding: EdgeInsets.zero,
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          itemCount: dateSortType.length,
                          itemBuilder: (context, index) {
                            return Padding(
                              padding: const EdgeInsets.only(top: 10),
                              child: Row(
                                children: [
                                  Transform.scale(
                                    scale: 1,
                                    child: Radio(
                                      activeColor: colorBlack,
                                      visualDensity: const VisualDensity(
                                        vertical: VisualDensity.minimumDensity,
                                      ),
                                      value: dateSortType[index],
                                      groupValue: selectedDateSortType,
                                      onChanged: (value) {
                                        setState(() {
                                          selectedDateSortType =
                                              value.toString();
                                        });
                                      },
                                    ),
                                  ),
                                  sizedBoxWidth(3),
                                  Text(
                                    dateSortType[index],
                                    style: size14_M_regular(),
                                  ),
                                ],
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                    sizedBoxHeight(8),
                    dividerView(color: colorGrey),
                    sizedBoxHeight(8),
                    Text(
                      "Date Posted",
                      style: size16_M_semibold(textColor: primaryColor),
                    ),
                    sizedBoxHeight(15),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("From", style: size14_M_regular()),
                        sizedBoxHeight(6),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 15, vertical: 12),
                          decoration: common_border_dec(
                              borderRadius: 8, borderColor: colorGrey),
                          child: Row(
                            children: [
                              Expanded(
                                  child: Text(formattedFromDate,
                                      style: size14_M_regular(
                                          textColor:
                                              formattedFromDate == "Select Date"
                                                  ? colorGrey
                                                  : colorBlack))),
                              GestureDetector(
                                onTap: () {
                                  dateSelection(true);
                                },
                                child: const Icon(Icons.calendar_today,
                                    size: 20, color: primaryColor),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    sizedBoxHeight(15),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text("To", style: size14_M_regular()),
                        sizedBoxHeight(6),
                        Container(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 15, vertical: 12),
                          decoration: common_border_dec(
                              borderRadius: 8, borderColor: colorGrey),
                          child: Row(
                            children: [
                              Expanded(
                                  child: Text(formattedToDate,
                                      style: size14_M_regular(
                                          textColor:
                                              formattedToDate == "Select Date"
                                                  ? colorGrey
                                                  : colorBlack))),
                              GestureDetector(
                                onTap: () {
                                  dateSelection(false);
                                },
                                child: const Icon(Icons.calendar_today,
                                    size: 20, color: primaryColor),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                    sizedBoxHeight(4),
                  ],
                ),
              ),
              sizedBoxHeight(30),
              SortButtonWidget(
                title: "Most Popular",
                onPressedCallBack: () {
                  setState(() {
                    selectedSortOption = SortButtonEnums.popular;
                  });
                },
                isSelected: selectedSortOption == SortButtonEnums.popular
                    ? true
                    : false,
              ),
              sizedBoxHeight(15),
              SortButtonWidget(
                title: "Alphabetically",
                onPressedCallBack: () {
                  setState(() {
                    selectedSortOption = SortButtonEnums.alphabetical;
                  });
                },
                isSelected: selectedSortOption == SortButtonEnums.alphabetical
                    ? true
                    : false,
              ),
              sizedBoxHeight(15),
              SortButtonWidget(
                title: "Top Rated",
                onPressedCallBack: () {
                  setState(() {
                    selectedSortOption = SortButtonEnums.top;
                  });
                },
                isSelected:
                    selectedSortOption == SortButtonEnums.top ? true : false,
              ),
              sizedBoxHeight(15),
              SortButtonWidget(
                title: "Favourite First",
                onPressedCallBack: () {
                  setState(() {
                    selectedSortOption = SortButtonEnums.favourite;
                  });
                },
                isSelected: selectedSortOption == SortButtonEnums.favourite
                    ? true
                    : false,
              ),
              sizedBoxHeight(26),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(ScreenSize.getWidth(context, 0.6), 48),
                ),
                onPressed: () async {},
                child: const Text('Select'),
              ),
              sizedBoxHeight(50),
            ],
          ),
        ),
      ),
    );
  }

  void _onSelected(bool selected, String dataName) {
    if (selected == true) {
      setState(() {
        checkedTimes.add(dataName);
      });
    } else {
      setState(() {
        checkedTimes.remove(dataName);
      });
    }
  }

  Future<void> dateSelection(bool isFromData) async {
    DateTime? date = await showDatePicker(
      context: context,
      lastDate: DateTime(2101),
      initialDate: DateTime.now(),
      firstDate: DateTime.now().subtract(const Duration(days: 0)),
      builder: (context, child) {
        return Theme(
          data: ThemeData.dark().copyWith(
              colorScheme: const ColorScheme.light(
                  primary: primaryColor,
                  onPrimary: colorWhite,
                  surface: colorBlack,
                  onSurface: colorBlack),
              dialogBackgroundColor: backgroundColor,
              textButtonTheme: TextButtonThemeData(
                  style: TextButton.styleFrom(foregroundColor: colorBlack))),
          child: child!,
        );
      },
    );
    if (isFromData) {
      if (date != null) {
        setState(() {
          pickedFromDate = date;
          formattedFromDate =
              DateFormat('yyyy-MM-dd').format(pickedFromDate!).toString();
        });
      } else {
        setState(() {
          pickedFromDate = null;
          formattedFromDate = "Select Date";
        });
      }
    } else {
      if (date != null) {
        setState(() {
          pickedToDate = date;
          formattedToDate =
              DateFormat('yyyy-MM-dd').format(pickedToDate!).toString();
        });
      } else {
        setState(() {
          pickedToDate = null;
          formattedToDate = "Select Date";
        });
      }
    }
  }
}
