import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:ionicons/ionicons.dart';
import 'package:wiki_events/app/app.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/events/model/selectable_model.dart';
import 'package:wiki_events/explore/explore.dart';
import 'package:wiki_events/explore/provider/explore_provider.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class ExplorePage extends StatefulWidget {
  const ExplorePage({super.key});

  @override
  State<ExplorePage> createState() => _ExplorePageState();
}

class _ExplorePageState extends State<ExplorePage> {
  late ExploreProvider exploreProvider;

  final TextEditingController _searchEditingController =
      TextEditingController();

  late GoogleMapController googleMapController;

  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 4,
  );

  @override
  void initState() {
    loadExploreEvents();
    super.initState();
  }

  Future<void> loadExploreEvents() async {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      exploreProvider = Provider.of<ExploreProvider>(context, listen: false);
      AppStateServiceProvider.instance.showLoader;
      await exploreProvider.getExploreEventList();
      await setmarkerdata(exploreProvider);
      await loadMapMarker();
      AppStateServiceProvider.instance.hideLoader;
    });
  }

  Future<void> setmarkerdata(ExploreProvider exploreProvider) async {
    exploreProvider.setMapMarkersList = [];
    for (var p in exploreProvider.exploreEventList) {
      exploreProvider.markers.add(MapMarker(p));
    }
    exploreProvider.setMapMarkersList = exploreProvider.markers;
  }

  Future<void> loadMapMarker() async {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      if (mounted) {
        if (exploreProvider.exploreEventList.isNotEmpty) {
          await Future.forEach(exploreProvider.exploreEventList,
              (ExploreSelectableEventModel event) async {
            if (event.isSelected) {
              await precacheImage(
                  NetworkImage(event.event.eventTypes![0].imageUrlSelect!),
                  context);
            } else {
              await precacheImage(
                  NetworkImage(event.event.eventTypes![0].imageUrlNormal!),
                  context);
            }
            return null;
          });
        }

        // ignore: use_build_context_synchronously
        MarkerGenerator(exploreProvider.markers.toList(), (bitmaps) async {
          debugPrint('bitmaps found: ${bitmaps.length}');
          await mapBitmapsToMarkers(bitmaps, exploreProvider);
        }).generate(context);
      }
    });
  }

  Future<void> mapBitmapsToMarkers(
      List<Uint8List> bitmaps, ExploreProvider exploreProvider) async {
    exploreProvider.customMarkers.clear();
    bitmaps.asMap().forEach((i, bmp) {
      exploreProvider.customMarkers.add(
        Marker(
            markerId: MarkerId("$i"),
            position: LatLng(
              double.parse(exploreProvider.exploreEventList[i].event.latitude!),
              double.parse(
                  exploreProvider.exploreEventList[i].event.longitude!),
            ),
            icon: BitmapDescriptor.fromBytes(bmp),
            onTap: () async {
              exploreProvider.setSelectedMarker(index: i);
              await loadMapMarker();
            }),
      );
    });
    exploreProvider.setCustomMarkersList = exploreProvider.customMarkers;
  }

  @override
  Widget build(BuildContext context) {
    exploreProvider = Provider.of<ExploreProvider>(context, listen: true);

    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: bgColorECF4F1,
        body: Stack(
          children: [
            GoogleMap(
              mapType: MapType.normal,
              markers: exploreProvider.customMarkers,
              initialCameraPosition: _kGooglePlex,
              myLocationEnabled: true,
              myLocationButtonEnabled: false,
              zoomControlsEnabled: false,
              onTap: (v) async {
                exploreProvider.setUnselectAllMarker();
                await loadMapMarker();
              },
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 16, bottom: 16, left: 12, right: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SafeArea(
                    maintainBottomViewPadding: true,
                    child: CustomAppBar(
                      title: "Explore",
                      actionWidget: GestureDetector(
                        onTap: () {
                          Pages.exploreList.go(context);
                        },
                        child: Row(
                          children: [
                            const SizedBox(
                              width: 20,
                              height: 16,
                              child: Image(
                                image: AssetImage(AppAssets.LIST_ICON),
                                fit: BoxFit.fill,
                              ),
                            ),
                            sizedBoxWidth(4),
                          ],
                        ),
                      ),
                    ),
                  ),
                  sizedBoxHeight(15),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Expanded(
                        child: TextFieldPrefixImage(
                            hintText: "Start typing here",
                            controller: _searchEditingController,
                            validator: (String? value) {},
                            contentPadding: const EdgeInsets.all(11),
                            prefixIcon: const Padding(
                              padding: EdgeInsets.only(right: 10, left: 10),
                              child: Icon(
                                Icons.search,
                                size: 20,
                              ),
                            ),
                            onChanged: (value) {},
                            onFieldSubmitted: (value) {}),
                      ),
                      GestureDetector(
                        onTap: () {
                          Pages.exploreFilter.push(context);
                        },
                        child: Container(
                          decoration: common_border_dec(
                              boxShadow: containerShadow,
                              borderRadius: 8,
                              color: primaryColor,
                              borderColor: primaryColor),
                          alignment: Alignment.center,
                          margin: const EdgeInsets.only(left: 5),
                          padding: const EdgeInsets.all(8),
                          child: const Icon(
                            Icons.tune_rounded,
                            color: colorBlack,
                            size: 25,
                          ),
                        ),
                      ),
                    ],
                  ),
                  sizedBoxHeight(8),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                          padding: const EdgeInsets.all(7),
                          decoration: common_border_dec(
                              borderRadius: 8, color: primaryColor),
                          child: Row(
                            children: [
                              const Icon(Ionicons.navigate,
                                  color: grey495057, size: 20),
                              const SizedBox(
                                height: 22,
                                child: VerticalDivider(
                                  color: grey495057,
                                  thickness: 0.25,
                                ),
                              ),
                              Text("Melbourne, Australia",
                                  style:
                                      size13_M_medium(textColor: grey495057)),
                              const Icon(Icons.navigate_next,
                                  color: grey495057, size: 22),
                            ],
                          )),
                      // sizedBoxWidth(5),
                      Container(
                          padding: const EdgeInsets.all(7),
                          decoration: common_border_dec(
                            borderRadius: 8,
                            color: Colors.white.withOpacity(0.6),
                            borderColor: darkGreyColor,
                          ),
                          child: Row(
                            children: [
                              const Icon(Icons.add,
                                  color: darkGreyColor, size: 22),
                              sizedBoxWidth(3),
                              Text("50km",
                                  style: size14_M_medium(
                                      textColor: darkGreyColor)),
                              sizedBoxWidth(3),
                              const Icon(Icons.remove,
                                  color: darkGreyColor, size: 22),
                            ],
                          )),
                    ],
                  ),
                  const Spacer(),
                  exploreProvider.isViewEvent &&
                          exploreProvider.exploreSelectedEvent != null
                      ? Padding(
                          padding: const EdgeInsets.only(bottom: 85),
                          child: EventDetailCard(
                            event: exploreProvider.exploreSelectedEvent!,
                            callBackViewEventButton: (String id) {
                              Pages.viewEvent.push(
                                context,
                                extra: ViewEventPageParam(
                                  viewEventFrom: ViewEventFrom.explore,
                                  eventId: int.parse(id),
                                ),
                              );
                            },
                          ),
                        )
                      : Container()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
