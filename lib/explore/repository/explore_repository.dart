import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/explore/model/event_list_res.dart';
import 'package:wiki_events/explore/model/model.dart';

class ExploreRepsitory extends BaseApi {
  ExploreRepsitory._()
      : super(interceptors: [
          ClientKeyInterceptor(),
          PrettyDioLogger(),
          AccessTokenInterceptor()
        ]);

  static ExploreRepsitory? _instance;

  static ExploreRepsitory get instance {
    _instance ??= ExploreRepsitory._();
    return _instance!;
  }

  Future<ApiResponse<List<EventModel>?>> getAllEvents() async {
    try {
      Response response = await get('events');
      EventListRes eventListResModel = EventListRes.fromJson(response.data);
      if (eventListResModel.result != null && eventListResModel.result!) {
        if (eventListResModel.eventList != null) {
          return ApiResponse<List<EventModel>?>(
            result: true,
            message: eventListResModel.message ?? 'Success',
            payload: eventListResModel.eventList,
          );
        }
      }
      return ApiResponse<List<EventModel>?>(
        result: false,
        message: eventListResModel.message ?? 'Failed',
        payload: null,
      );
    } catch (e) {
      return ApiResponse<List<EventModel>?>(
        result: false,
        message: e.toString(),
        payload: null,
      );
    }
  }
}
