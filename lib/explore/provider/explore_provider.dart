import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/events/model/selectable_model.dart';
import 'package:wiki_events/explore/explore.dart';

class ExploreProvider with ChangeNotifier {
  ExploreProvider._() : super();

  static ExploreProvider? _instance;

  static ExploreProvider get instance {
    _instance ??= ExploreProvider._();
    return _instance!;
  }

  ExploreRepsitory exploreRepsitory = ExploreRepsitory.instance;

  List<EventModel> eventList = [];
  List<ExploreSelectableEventModel> exploreEventList = [];

  EventModel? exploreSelectedEvent;

  List<MapMarker> markers = [];
  Set<Marker> customMarkers = {};

  bool isViewEvent = false;

  set setSelectedEventView(EventModel? event) {
    exploreSelectedEvent = event;
    notifyListeners();
  }

  set setViewEvent(bool value) {
    isViewEvent = value;
    notifyListeners();
  }

  set setEventList(List<EventModel> list) {
    eventList = list;
    notifyListeners();
  }

  set setExploreEventList(List<ExploreSelectableEventModel> list) {
    exploreEventList = list;
    notifyListeners();
  }

  set setMapMarkersList(List<MapMarker> list) {
    markers = list;
    notifyListeners();
  }

  set setCustomMarkersList(Set<Marker> list) {
    customMarkers = list;
    notifyListeners();
  }

  Future<void> getEventList() async {
    var response = await exploreRepsitory.getAllEvents();
    setEventList = response.payload ?? [];
  }

  Future<void> getExploreEventList() async {
    exploreEventList = [];
    await getEventList(); //loading event list
    for (var element in eventList) {
      exploreEventList
          .add(ExploreSelectableEventModel(event: element, isSelected: false));
    }
    setExploreEventList = exploreEventList;
  }

  void setUnselectAllMarker() {
    setViewEvent = false;
    for (int i = 0; i < exploreEventList.length; i++) {
      exploreEventList[i].isSelected = false;
    }
    setSelectedEventView = null;
    setExploreEventList = exploreEventList;
    notifyListeners();
  }

  void setSelectedMarker({required int index}) {
    for (int i = 0; i < exploreEventList.length; i++) {
      exploreEventList[i].isSelected = false;
    }
    exploreEventList[index].isSelected = true;
    setSelectedEventView = exploreEventList[index].event;
    setViewEvent = true;
    setExploreEventList = exploreEventList;
    notifyListeners();
  }
}
