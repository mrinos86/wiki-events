import 'package:flutter/material.dart';
import 'package:wiki_events/common/widgets/text_field/text_field_with_prefix_image.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class SavedListPage extends StatefulWidget {
  const SavedListPage({super.key});

  @override
  State<SavedListPage> createState() => _SavedListPageState();
}

class _SavedListPageState extends State<SavedListPage> {
  final TextEditingController _searchEditingController =
      TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: bgColorECF4F1,
        appBar: AppBar(
          title: const Text('Saved'),
          leading: Container(),
          actions: [
            GestureDetector(
              onTap: () {
                Pages.saved.push(context);
              },
              child: Row(children: [
                const Icon(
                  Icons.map,
                  size: 25,
                ),
                sizedBoxWidth(15),
              ]),
            )
          ],
        ),
        body: Padding(
          padding: const EdgeInsets.fromLTRB(12, 0, 12, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Expanded(
                    child: TextFieldPrefixImage(
                        hintText: "Start typing here",
                        controller: _searchEditingController,
                        validator: (String? value) {},
                        contentPadding: const EdgeInsets.all(11),
                        prefixIcon: const Padding(
                          padding: EdgeInsets.only(right: 10, left: 10),
                          child: Icon(
                            Icons.search,
                            size: 20,
                          ),
                        ),
                        onChanged: (value) {},
                        onFieldSubmitted: (value) {}),
                  ),
                  GestureDetector(
                    onTap: () {
                      Pages.exploreFilter.push(context);
                    },
                    child: Container(
                      decoration: common_border_dec(
                          borderRadius: 10,
                          color: primaryColor,
                          borderColor: primaryColor,
                          boxShadow: containerShadow),
                      alignment: Alignment.center,
                      margin: const EdgeInsets.only(left: 5),
                      padding: const EdgeInsets.all(8),
                      child: const Icon(
                        Icons.tune_rounded,
                        color: colorBlack,
                        size: 25,
                      ),
                    ),
                  ),
                ],
              ),
              sizedBoxHeight(8),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.only(top: 10),
                  child: ListView.builder(
                      // primary: false,
                      padding: const EdgeInsets.only(bottom: 85),
                      shrinkWrap: true,
                      itemCount: 3,
                      itemBuilder: (context, index) {
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 12),
                          child: EventDetailCard(
                            event: EventModel(
                              id: "100",
                              name: "Event Name",
                              amILiked: true,
                              location: "Max Ground, 134 Clayton",
                              distance: "10.5KM",
                              reviewCount: 5,
                              reviewRate: "4.0",
                              date: "2023-06-06",
                              startTime: "10:30 AM",
                              images: [
                                EventImageModel(
                                  id: 1,
                                  url: "assets/images/image1.jpg",
                                )
                              ],
                              eventTypes: [
                                EventTypeModel(
                                    id: "",
                                    name: "Sport & Fitness",
                                    imageUrl: "",
                                    imageUrlNormal: "",
                                    imageUrlSelect: "")
                              ],
                              eventFeatures: [
                                EventFeatureModel(
                                  id: '',
                                  name: '',
                                  imageUrl:
                                      "https://WikiEvents.sandbox23.preview.cx/images/features/Paid_Parking.png",
                                ),
                                EventFeatureModel(
                                  id: '',
                                  name: '',
                                  imageUrl:
                                      "https://WikiEvents.sandbox23.preview.cx/images/features/Paid_Parking.png",
                                ),
                                EventFeatureModel(
                                  id: '',
                                  name: '',
                                  imageUrl:
                                      "https://WikiEvents.sandbox23.preview.cx/images/features/Paid_Parking.png",
                                ),
                                EventFeatureModel(
                                  id: '',
                                  name: '',
                                  imageUrl:
                                      "https://WikiEvents.sandbox23.preview.cx/images/features/Paid_Parking.png",
                                )
                              ],
                            ),
                            callBackViewEventButton: (String id) {
                              Pages.viewEvent.push(
                                context,
                                extra: ViewEventPageParam(
                                    viewEventFrom: ViewEventFrom.explore),
                              );
                            },
                          ),
                        );
                      }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
