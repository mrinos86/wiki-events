import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:wiki_events/common/widgets/text_field/text_field_with_prefix_image.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/explore/widget/custom_app_bar.dart';
import 'package:wiki_events/explore/widget/map_marker/marker_generator.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/saved/saved_events.dart';
import 'package:wiki_events/utils/utils.dart';

class SavedPage extends StatefulWidget {
  const SavedPage({super.key});

  @override
  State<SavedPage> createState() => _SavedPageState();
}

class _SavedPageState extends State<SavedPage> {
  final TextEditingController _searchEditingController =
      TextEditingController();

  bool isViewEvent = false;

  late GoogleMapController googleMapController;

  static const CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(37.42796133580664, -122.085749655962),
    zoom: 4,
  );

  List<SavedMapMarker> markers = [];

  Set<Marker> customMarkers = {};

  List<TestEventData> testData = [
    TestEventData(
      eventType: MapIcons.map,
      lat: "37.42796133580664",
      long: "-122.085749655962",
      isSelected: false,
    ),
    TestEventData(
      eventType: MapIcons.comedy,
      lat: "41.42796133580664",
      long: "-118.085749655962",
      isSelected: false,
    ),
    TestEventData(
      eventType: MapIcons.community,
      lat: "37.42796133580664",
      long: "-114.085749655962",
      isSelected: false,
    ),
    TestEventData(
      eventType: MapIcons.filmsMedia,
      lat: "37.42796133580664",
      long: "-118.085749655962",
      isSelected: false,
    ),
    TestEventData(
      eventType: MapIcons.foodDrink,
      lat: "41.42796133580664",
      long: "-110.085749655962",
      isSelected: false,
    )
  ];

  late BitmapDescriptor markerCustomIcon = BitmapDescriptor.defaultMarker;

  @override
  void initState() {
    Future.delayed(Duration.zero, () {
      setmarkerdata();
    });

    super.initState();
  }

  void mapBitmapsToMarkers(List<Uint8List> bitmaps) {
    customMarkers.clear();
    bitmaps.asMap().forEach((i, bmp) {
      customMarkers.add(
        Marker(
            markerId: MarkerId("$i"),
            position: LatLng(
              double.parse(testData[i].lat),
              double.parse(testData[i].long),
            ),
            icon: BitmapDescriptor.fromBytes(bmp),
            onTap: () async {
              setSelectedEvent(index: i);
            }),
      );
    });
    setState(() {
      customMarkers = customMarkers;
    });
  }

  setmarkerdata() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      for (var p in testData) {
        markers.add(SavedMapMarker(p));
      }
      setState(() {
        markers = markers;
      });

      loadMapMarker();
    });
  }

  loadMapMarker() async {
    if (testData.isNotEmpty) {
      await Future.forEach(testData, (TestEventData event) async {
        if (event.isSelected) {
          await precacheImage(
              AssetImage(event.eventType.getMapSelectedIcons()!), context);
        } else {
          await precacheImage(
              AssetImage(event.eventType.getMapNormalIcons()!), context);
        }

        return null;
      });
    }
    // ignore: use_build_context_synchronously
    MarkerGenerator(markers.toList(), (bitmaps) {
      debugPrint('bitmaps found: ${bitmaps.length}');
      mapBitmapsToMarkers(bitmaps);
    }).generate(context);
  }

  setSelectedEvent({int? index, bool allUnselected = false}) {
    isViewEvent = false;
    for (int i = 0; i < testData.length; i++) {
      testData[i].isSelected = false;
    }
    if (!allUnselected) {
      testData[index!].isSelected = true;
      isViewEvent = true;
    }

    setState(() {
      testData = testData;
      isViewEvent = isViewEvent;
    });

    loadMapMarker();
  }

  @override
  Widget build(BuildContext context) {
    // loadMapMarker();
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: bgColorECF4F1,
        body: Stack(
          children: [
            GoogleMap(
              mapType: MapType.normal,
              markers: customMarkers,
              initialCameraPosition: _kGooglePlex,
              myLocationEnabled: true,
              myLocationButtonEnabled: false,
              zoomControlsEnabled: false,
              onTap: (v) {
                setSelectedEvent(allUnselected: true);
              },
            ),
            Padding(
              padding: const EdgeInsets.only(
                  top: 16, bottom: 16, left: 12, right: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SafeArea(
                    maintainBottomViewPadding: true,
                    child: CustomAppBar(
                      title: "Saved",
                      actionWidget: GestureDetector(
                        onTap: () {
                          Pages.savedList.push(context);
                        },
                        child: Row(
                          children: [
                            const SizedBox(
                              width: 20,
                              height: 16,
                              child: Image(
                                image: AssetImage(AppAssets.LIST_ICON),
                                fit: BoxFit.fill,
                              ),
                            ),
                            sizedBoxWidth(4),
                          ],
                        ),
                      ),
                    ),
                  ),
                  sizedBoxHeight(15),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Expanded(
                        child: TextFieldPrefixImage(
                            hintText: "Start typing here",
                            controller: _searchEditingController,
                            validator: (String? value) {},
                            contentPadding: const EdgeInsets.all(11),
                            prefixIcon: const Padding(
                              padding: EdgeInsets.only(right: 10, left: 10),
                              child: Icon(
                                Icons.search,
                                size: 20,
                              ),
                            ),
                            onChanged: (value) {},
                            onFieldSubmitted: (value) {}),
                      ),
                      GestureDetector(
                        onTap: () {
                          Pages.exploreFilter.push(context);
                        },
                        child: Container(
                          decoration: common_border_dec(
                              boxShadow: containerShadow,
                              borderRadius: 8,
                              color: primaryColor,
                              borderColor: primaryColor),
                          alignment: Alignment.center,
                          margin: const EdgeInsets.only(left: 5),
                          padding: const EdgeInsets.all(8),
                          child: const Icon(
                            Icons.tune_rounded,
                            color: colorBlack,
                            size: 25,
                          ),
                        ),
                      ),
                    ],
                  ),
                  sizedBoxHeight(8),
                  const Spacer(),
                  isViewEvent
                      ? Padding(
                          padding: const EdgeInsets.only(bottom: 85),
                          child: EventDetailCard(
                            callBackViewEventButton: (String id) {
                              Pages.viewEvent.push(
                                context,
                                extra: ViewEventPageParam(
                                    viewEventFrom: ViewEventFrom.explore),
                              );
                            },
                            event: EventModel(
                              id: "100",
                              name: "Event Name",
                              amILiked: true,
                              location: "Max Ground, 134 Clayton",
                              distance: "10.5KM",
                              reviewCount: 5,
                              reviewRate: "4.0",
                              date: "2023-06-06",
                              startTime: "10:30 AM",
                              images: [
                                EventImageModel(
                                  id: 1,
                                  url: "assets/images/image1.jpg",
                                )
                              ],
                              eventTypes: [
                                EventTypeModel(
                                    id: "",
                                    name: "Sport & Fitness",
                                    imageUrl: "",
                                    imageUrlNormal: "",
                                    imageUrlSelect: "")
                              ],
                              eventFeatures: [
                                EventFeatureModel(
                                  id: '',
                                  name: '',
                                  imageUrl:
                                      "https://WikiEvents.sandbox23.preview.cx/images/features/Paid_Parking.png",
                                ),
                                EventFeatureModel(
                                  id: '',
                                  name: '',
                                  imageUrl:
                                      "https://WikiEvents.sandbox23.preview.cx/images/features/Paid_Parking.png",
                                ),
                                EventFeatureModel(
                                  id: '',
                                  name: '',
                                  imageUrl:
                                      "https://WikiEvents.sandbox23.preview.cx/images/features/Paid_Parking.png",
                                ),
                                EventFeatureModel(
                                  id: '',
                                  name: '',
                                  imageUrl:
                                      "https://WikiEvents.sandbox23.preview.cx/images/features/Paid_Parking.png",
                                )
                              ],
                            ),
                          ),
                        )
                      : Container()
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class TestEventData {
  MapIcons eventType;
  String lat;
  String long;
  bool isSelected;

  TestEventData({
    required this.eventType,
    required this.lat,
    required this.long,
    required this.isSelected,
  });
}
