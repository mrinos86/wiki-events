// ignore_for_file: implementation_imports

import 'package:flutter/material.dart';
import 'package:wiki_events/saved/saved_events.dart';
import 'package:wiki_events/utils/utils.dart';

class SavedMapMarker extends StatelessWidget {
  final TestEventData event;
  const SavedMapMarker(this.event, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        // HttpExceptionNotifyUser.showMessage('tapped');
      },
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                padding: const EdgeInsets.all(8),
                decoration: event.isSelected
                    ? BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: primaryColor, width: 2.5),
                        color: colorWhite.withOpacity(0.4))
                    : const BoxDecoration(shape: BoxShape.circle),
                child: ClipRRect(
                  child: Column(
                    children: [
                      Image.asset(
                        event.isSelected
                            ? event.eventType.getMapSelectedIcons()!
                            : event.eventType.getMapNormalIcons()!,
                        height: 45,
                        width: 40,
                        fit: BoxFit.fitWidth,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ],
      ),
    );
  }
}

class CustomClipPath extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(size.width / 3, 0.0);
    path.lineTo(size.width / 2, size.height / 3);
    path.lineTo(size.width - size.width / 3, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
