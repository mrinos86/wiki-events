import 'package:flutter/material.dart';
import 'package:google_place/google_place.dart';
import 'package:wiki_events/events/model/event_model.dart';
import 'package:wiki_events/my_events/my_event.dart';

class MyEventProvider with ChangeNotifier {
  MyEventProvider._() : super();

  static MyEventProvider? _instance;

  static MyEventProvider get instance {
    _instance ??= MyEventProvider._();
    return _instance!;
  }

  List<String> pickedImageList = [];
  bool is18Plus = false;

  List<EventTypeModel> selectedEventType = [];

  List<AutocompletePrediction>? predictions = [];
  AutocompletePrediction? pickedLocation;

  DateTime? pickedDate;
  TimeOfDay? pickedStartTime;
  TimeOfDay? pickedEndTime;

  List<EventFeatureModel> selectedEventFeatures = [];
  AddEventRequestParam? addEventRequestParam;

  bool isLoading = false;

  void initProvider() {
    pickedImageList = [];
    is18Plus = false;
    selectedEventType = [];
    predictions = [];
    pickedLocation = null;
    pickedDate = null;
    pickedStartTime = null;
    pickedEndTime = null;
    selectedEventFeatures = [];
    addEventRequestParam = null;
    isLoading = false;
    clearValidation();
    notifyListeners();
  }

  set setLoading(bool value) {
    isLoading = value;
    notifyListeners();
  }

  set setPickedImageList(List<String> value) {
    pickedImageList = value;
    notifyListeners();
  }

  set setIs18Plus(bool value) {
    is18Plus = value;
    notifyListeners();
  }

  set setEventTypes(List<EventTypeModel> value) {
    selectedEventType = value;
    notifyListeners();
  }

  set setSearchedPredictions(List<AutocompletePrediction>? value) {
    predictions = value;
    notifyListeners();
  }

  set setPickedLocation(AutocompletePrediction? value) {
    pickedLocation = value;
    notifyListeners();
  }

  set setPickedDate(DateTime? value) {
    pickedDate = value;
    notifyListeners();
  }

  set setPickedStartTime(TimeOfDay? value) {
    pickedStartTime = value;
    notifyListeners();
  }

  set setPickedEndTime(TimeOfDay? value) {
    pickedEndTime = value;
    notifyListeners();
  }

  set setEventFeatures(List<EventFeatureModel> value) {
    selectedEventFeatures = value;
    notifyListeners();
  }

  set setAddEventRequestParam(AddEventRequestParam value) {
    addEventRequestParam = value;
    notifyListeners();
  }

  //--------- Validations -----------//
  bool isImageError = false;
  bool isNameError = false;
  bool isTypeError = false;
  bool isLocationError = false;
  bool isDateError = false;
  bool isStartTimeError = false;
  bool isEndTimeError = false;

  bool isDescriptionError = false;
  bool isEventFeatureError = false;
  bool isLinkError = false;

  String errorMessage = "";

  set setImageError(bool value) {
    isImageError = value;
    notifyListeners();
  }

  bool validationCheckStep1({required String eventName}) {
    bool validationError = false;

    if (pickedEndTime == null) {
      isEndTimeError = true;
      errorMessage = "Please select end time";
      validationError = true;
    }
    if (pickedStartTime == null) {
      isStartTimeError = true;
      errorMessage = "Please select start time";
      validationError = true;
    }
    if (pickedDate == null) {
      isDateError = true;
      errorMessage = "Please select Date";
      validationError = true;
    }
    if (pickedLocation == null) {
      isLocationError = true;
      errorMessage = "Please select Venue / Location";
      validationError = true;
    }
    if (selectedEventType.isEmpty) {
      isTypeError = true;
      errorMessage = "Please select Event Type ";
      validationError = true;
    }
    if (eventName.isEmpty) {
      isNameError = true;
      errorMessage = "Please enter Event Name ";
      validationError = true;
    }
    if (pickedImageList.isEmpty) {
      isImageError = true;
      errorMessage = "Please upload atleast one Image ";
      validationError = true;
    }
    notifyListeners();
    return validationError;
  }

  bool validationCheckStep2({required String eventDes}) {
    bool validationError = false;

    // TODO: need to check link validation

    if (selectedEventFeatures.isEmpty) {
      isEventFeatureError = true;
      errorMessage = "Please select Event Feature ";
      validationError = true;
    }
    if (eventDes.isEmpty) {
      isDescriptionError = true;
      errorMessage = "Please enter Event description ";
      validationError = true;
    }
    notifyListeners();
    return validationError;
  }

  void clearValidation() {
    isImageError = false;
    isNameError = false;
    isTypeError = false;
    isLocationError = false;
    isDateError = false;
    isStartTimeError = false;
    isEndTimeError = false;
    isDescriptionError = false;
    isEventFeatureError = false;
    isLinkError = false;
    errorMessage = "";
    notifyListeners();
  }
  //------------------------------//
}
