import 'dart:io';

import 'package:wiki_events/auth/auth.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/events/model/event_model.dart';
import 'package:wiki_events/events/model/response_model/event_res_model.dart';
import 'package:wiki_events/my_events/my_event.dart';
import 'package:wiki_events/profile/profile.dart';

class MyEventRepository extends BaseApi {
  MyEventRepository._()
      : super(interceptors: [
          ClientKeyInterceptor(),
          PrettyDioLogger(),
          AccessTokenInterceptor()
        ]);

  static MyEventRepository? _instance;

  static MyEventRepository get instance {
    _instance ??= MyEventRepository._();
    return _instance!;
  }

  Future<ApiResponse<EventModel?>> addNewEvent(
      AddEventRequestParam request) async {
    try {
      Response response = await post('events', data: request.toJson());
      EventResModel eventResModel = EventResModel.fromJson(response.data);
      if (eventResModel.result != null && eventResModel.result!) {
        if (eventResModel.event != null) {
          return ApiResponse<EventModel>(
            result: true,
            message: eventResModel.message ?? 'Success',
            payload: eventResModel.event,
          );
        }
      }
      return ApiResponse<EventModel>(
        result: false,
        message: eventResModel.message ?? 'Failed',
        payload: null,
      );
    } catch (e) {
      return ApiResponse<EventModel>(
        result: false,
        message: e.toString(),
        payload: null,
      );
    }
  }

  // Future<ApiResponse<User?>> updateAvatar(File file) async {
  //   try {
  //     Response response = await post('profile/avatar',
  //         data: FormData.fromMap({
  //           "image": await MultipartFile.fromFile(file.path),
  //         }));
  //     AuthUserResponse authUserResponse =
  //         AuthUserResponse.fromJson(response.data);
  //     if (authUserResponse.result != null && authUserResponse.result!) {
  //       if (authUserResponse.payload != null) {
  //         return ApiResponse<User?>(
  //           result: true,
  //           message: authUserResponse.message ?? 'Success',
  //           payload: authUserResponse.payload,
  //         );
  //       }
  //     }
  //     return ApiResponse<User?>(
  //       result: false,
  //       message: authUserResponse.message ?? 'Failed',
  //       payload: null,
  //     );
  //   } catch (e) {
  //     return ApiResponse<User?>(
  //       result: false,
  //       message: e.toString(),
  //       payload: null,
  //     );
  //   }
  // }
}
