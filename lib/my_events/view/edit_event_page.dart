import 'package:flutter/material.dart';
import 'package:wiki_events/my_events/my_event.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class EditEventPage extends StatefulWidget {
  const EditEventPage({super.key});

  @override
  State<EditEventPage> createState() => _EditEventPageState();
}

class _EditEventPageState extends State<EditEventPage> {
  late PageController _pageController;
  int _selectedPageIndex = 0;

  @override
  void initState() {
    _pageController = PageController(initialPage: _selectedPageIndex);
    super.initState();
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        backgroundColor: backgroundColor,
        appBar: AppBar(
          title: const Text('Edit Event'),
          automaticallyImplyLeading: false,
          leading: IconButton(
            onPressed: () {
              if (_selectedPageIndex > 0) {
                _pageController.previousPage(
                  duration: const Duration(milliseconds: 300),
                  curve: Curves.easeInOut,
                );
              } else {
                Navigator.pop(context);
              }
            },
            icon: const Icon(Icons.arrow_back_ios, size: 22),
          ),
        ),
        body: Builder(
          builder: (context) {
            // if (isClient) {
            //   return const ClientBasicDetails();
            // } else {
            return Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    children: [
                      for (int i = 0; i < 2; i++)
                        SizedBox(
                          width: 50,
                          child: AnimatedContainer(
                            duration: const Duration(milliseconds: 300),
                            margin: const EdgeInsets.symmetric(horizontal: 2),
                            height: 4,
                            decoration: ShapeDecoration(
                              shape: const StadiumBorder(),
                              color: _selectedPageIndex >= i
                                  ? primaryColor
                                  : colorGrey.withOpacity(0.3),
                            ),
                          ),
                        ),
                    ],
                  ),
                  Expanded(
                    child: PageView(
                      controller: _pageController,
                      onPageChanged: (index) {
                        setState(() {
                          _selectedPageIndex = index;
                        });
                      },
                      physics: const NeverScrollableScrollPhysics(),
                      children: [
                        EditEventStep1(
                          pageController: _pageController,
                        ),
                        EditEventStep2(
                          pageController: _pageController,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            );
            // }
          },
        ),
      ),
    );
  }
}
