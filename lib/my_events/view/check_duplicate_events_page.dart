import 'package:flutter/material.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class CheckDuplicateEventPage extends StatefulWidget {
  const CheckDuplicateEventPage({super.key});

  @override
  State<CheckDuplicateEventPage> createState() =>
      _CheckDuplicateEventPageState();
}

class _CheckDuplicateEventPageState extends State<CheckDuplicateEventPage> {
  List<EventModel> dupilcateEvents = [];
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: bgColorECF4F1,
      appBar: AppBar(
        title: const Text('Check For Duplicates'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              decoration: common_border_dec(
                  borderRadius: 8,
                  borderWidth: 0.6,
                  color: primaryColor.withOpacity(0.23),
                  // ignore: prefer_const_constructors
                  borderColor: Color(0xffFF7300)),
              padding: const EdgeInsets.all(8),
              child: Row(
                children: [
                  const Icon(
                    Icons.info,
                    color: primaryColor,
                    size: 25,
                  ),
                  sizedBoxWidth(12),
                  Expanded(
                    child: Text(
                      "Below is a list of events which is already exist. Please make sure the event you are adding is not listed below.",
                      style:
                          size12_M_medium(textColor: const Color(0xffFF7300)),
                    ),
                  ),
                ],
              ),
            ),
            sizedBoxHeight(15),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    dupilcateEvents.isNotEmpty
                        ? ListView.builder(
                            padding: EdgeInsets.zero,
                            shrinkWrap: true,
                            physics: const NeverScrollableScrollPhysics(),
                            itemCount: 3,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.only(bottom: 12),
                                child: EventDetailCard(
                                  event: EventModel(
                                    id: "100",
                                    name: "Event Name",
                                    amILiked: false,
                                    location: "Max Ground, 134 Clayton",
                                    distance: "10.5KM",
                                    reviewCount: 5,
                                    reviewRate: "4.0",
                                    date: "2023-06-06",
                                    startTime: "10:30 AM",
                                    images: [
                                      EventImageModel(
                                        id: 1,
                                        url: "assets/images/image1.jpg",
                                      )
                                    ],
                                    eventTypes: [
                                      EventTypeModel(
                                          id: "",
                                          name: "Sport & Fitness",
                                          imageUrl: "",
                                          imageUrlNormal: "",
                                          imageUrlSelect: "")
                                    ],
                                    eventFeatures: [
                                      EventFeatureModel(
                                        id: '',
                                        name: '',
                                        imageUrl:
                                            "https://WikiEvents.sandbox23.preview.cx/images/features/Paid_Parking.png",
                                      ),
                                      EventFeatureModel(
                                        id: '',
                                        name: '',
                                        imageUrl:
                                            "https://WikiEvents.sandbox23.preview.cx/images/features/Paid_Parking.png",
                                      )
                                    ],
                                  ),
                                  callBackViewEventButton: (String id) {
                                    Pages.viewEvent.push(
                                      context,
                                      extra: ViewEventPageParam(
                                          viewEventFrom: ViewEventFrom.explore),
                                    );
                                  },
                                ),
                              );
                            },
                          )
                        : Column(
                            children: [
                              Padding(
                                padding: EdgeInsets.only(
                                    top: MediaQuery.of(context).size.height /
                                        3.5),
                                child: Center(
                                  child: Text(
                                    "No events available",
                                    style: size13_M_regular(),
                                  ),
                                ),
                              ),
                              sizedBoxHeight(
                                  MediaQuery.of(context).size.height / 4.5),
                            ],
                          ),
                    sizedBoxHeight(15),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize:
                            Size(ScreenSize.getWidth(context, 0.6), 48),
                      ),
                      onPressed: () async {
                        // Pages.myEvents.push(context);
                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                      },
                      child: const Text("It is listed"),
                    ),
                    sizedBoxHeight(15),
                    OutlinedButton(
                      style: OutlinedButton.styleFrom(
                        backgroundColor: Colors.transparent,
                        minimumSize:
                            Size(ScreenSize.getWidth(context, 0.6), 48),
                      ),
                      onPressed: () async {
                        Pages.addEvent.push(context, extra: 1);
                      },
                      child: const Text("It is not listed"),
                    ),
                    sizedBoxHeight(50),
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
