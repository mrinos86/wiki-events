import 'package:flutter/material.dart';
import 'package:wiki_events/common/widgets/text_field/text_field_with_prefix_image.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class MyEventsPage extends StatefulWidget {
  const MyEventsPage({super.key});

  @override
  State<MyEventsPage> createState() => _MyEventsPageState();
}

class _MyEventsPageState extends State<MyEventsPage> {
  final TextEditingController _searchEditingController =
      TextEditingController();

  List<EventModel> myEvents = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        backgroundColor: bgColorECF4F1,
        appBar: AppBar(
          title: const Text('My Events'),
          leading: Container(),
        ),
        body: Padding(
          padding: const EdgeInsets.fromLTRB(12, 10, 12, 0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFieldPrefixImage(
                  hintText: "Start typing here",
                  controller: _searchEditingController,
                  validator: (String? value) {},
                  contentPadding: const EdgeInsets.all(11),
                  prefixIcon: const Padding(
                    padding: EdgeInsets.only(right: 10, left: 10),
                    child: Icon(
                      Icons.search,
                      size: 22,
                    ),
                  ),
                  onChanged: (value) {},
                  onFieldSubmitted: (value) {}),
              sizedBoxHeight(12),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  minimumSize: Size(ScreenSize.width(context), 48),
                ),
                onPressed: () {
                  Pages.addEvent.push(context, extra: 0);
                },
                child: const Text('Add a New Event'),
              ),
              sizedBoxHeight(5),
              myEvents.isNotEmpty
                  ? Expanded(
                      child: Container(
                        padding: const EdgeInsets.only(top: 10),
                        child: ListView.builder(
                            // primary: false,
                            padding: const EdgeInsets.only(bottom: 90),
                            shrinkWrap: true,
                            itemCount: 4,
                            itemBuilder: (context, index) {
                              return Padding(
                                padding: const EdgeInsets.only(bottom: 12),
                                child: EventDetailCard(
                                  event: EventModel(
                                    id: "100",
                                    name: "Event Name",
                                    amILiked: false,
                                    location: "Max Ground, 134 Clayton",
                                    distance: "10.5KM",
                                    reviewCount: 5,
                                    reviewRate: "4.0",
                                    date: "2023-06-06",
                                    startTime: "10:30 AM",
                                    images: [
                                      EventImageModel(
                                        id: 1,
                                        url: "assets/images/image1.jpg",
                                      )
                                    ],
                                    eventTypes: [
                                      EventTypeModel(
                                          id: "",
                                          name: "Sport & Fitness",
                                          imageUrl: "",
                                          imageUrlNormal: "",
                                          imageUrlSelect: "")
                                    ],
                                    eventFeatures: [
                                      EventFeatureModel(
                                        id: '',
                                        name: '',
                                        imageUrl:
                                            "https://WikiEvents.sandbox23.preview.cx/images/features/Paid_Parking.png",
                                      ),
                                      EventFeatureModel(
                                        id: '',
                                        name: '',
                                        imageUrl:
                                            "https://WikiEvents.sandbox23.preview.cx/images/features/Paid_Parking.png",
                                      ),
                                      EventFeatureModel(
                                        id: '',
                                        name: '',
                                        imageUrl:
                                            "https://WikiEvents.sandbox23.preview.cx/images/features/Paid_Parking.png",
                                      ),
                                      EventFeatureModel(
                                        id: '',
                                        name: '',
                                        imageUrl:
                                            "https://WikiEvents.sandbox23.preview.cx/images/features/Paid_Parking.png",
                                      )
                                    ],
                                  ),
                                  callBackViewEventButton: (String id) {
                                    Pages.viewEvent.push(
                                      context,
                                      extra: ViewEventPageParam(
                                          viewEventFrom:
                                              ViewEventFrom.myEvents),
                                    );
                                  },
                                ),
                              );
                            }),
                      ),
                    )
                  : Padding(
                      padding: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height / 3.5),
                      child: Center(
                        child: Text(
                          "No event available",
                          style: size13_M_regular(),
                        ),
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
