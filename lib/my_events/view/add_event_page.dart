import 'package:flutter/material.dart';
import 'package:wiki_events/my_events/my_event.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class AddEventPage extends StatefulWidget {
  int step;
  AddEventPage({super.key, required this.step});

  @override
  State<AddEventPage> createState() => _AddEventPageState();
}

class _AddEventPageState extends State<AddEventPage> {
  late PageController _pageController;
  late int _selectedPageIndex;

  late MyEventProvider myEventProvider;

  @override
  void initState() {
    setSelectedPage();
    _pageController = PageController(initialPage: _selectedPageIndex);
    super.initState();
  }

  setSelectedPage() {
    setState(() {
      _selectedPageIndex = widget.step;
    });
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    myEventProvider = Provider.of<MyEventProvider>(context, listen: true);
    return Stack(
      children: [
        GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Scaffold(
            backgroundColor: backgroundColor,
            appBar: AppBar(
              title: const Text('Add A New Event'),
              leading: GestureDetector(
                child: const Icon(
                  Icons.arrow_back_ios,
                  size: 22,
                ),
                onTap: () {
                  Navigator.of(context).pop();
                },
              ),
            ),
            body: Builder(
              builder: (context) {
                // if (isClient) {
                //   return const ClientBasicDetails();
                // } else {
                return Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          for (int i = 0; i < 2; i++)
                            SizedBox(
                              width: 50,
                              child: AnimatedContainer(
                                duration: const Duration(milliseconds: 300),
                                margin:
                                    const EdgeInsets.symmetric(horizontal: 2),
                                height: 4,
                                decoration: ShapeDecoration(
                                  shape: const StadiumBorder(),
                                  color: _selectedPageIndex >= i
                                      ? primaryColor
                                      : colorGrey.withOpacity(0.3),
                                ),
                              ),
                            ),
                        ],
                      ),
                      Expanded(
                        child: PageView(
                          controller: _pageController,
                          onPageChanged: (index) {
                            setState(() {
                              _selectedPageIndex = index;
                            });
                          },
                          physics: const NeverScrollableScrollPhysics(),
                          children: [
                            AddEventStep1(
                              pageController: _pageController,
                            ),
                            AddEventStep2(
                              pageController: _pageController,
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
                // }
              },
            ),
          ),
        ),
        myEventProvider.isLoading ? const OnPageLoader() : Container()
      ],
    );
  }
}
