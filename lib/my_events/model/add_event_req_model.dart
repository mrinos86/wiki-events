import 'dart:io';

import 'package:dio/dio.dart';

class AddEventRequestParam {
  String? name;
  int? ageRestricted;
  String? date;
  String? startTime;
  String? endTime;
  String? description;
  String? location;
  String? lat;
  String? long;
  String? typeIds;
  String? featureIds;
  List<File>? eventImages;

  AddEventRequestParam({
    this.name,
    this.ageRestricted,
    this.date,
    this.startTime,
    this.endTime,
    this.description,
    this.location,
    this.lat,
    this.long,
    this.typeIds,
    this.featureIds,
    this.eventImages,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['name'] = name;
    data['age_restricted'] = ageRestricted;
    data['date'] = date;
    data['start_time'] = startTime;
    data['end_time'] = endTime;
    data['description'] = description;
    data['location'] = location;
    data['latitude'] = lat;
    data['longitude'] = long;
    data['type_ids'] = typeIds;
    data['feature_ids'] = featureIds;
    data['images'] = eventImages;

    return data;
  }

  FormData toFormData() =>
      FormData.fromMap(toJson(), ListFormat.multiCompatible);
}
