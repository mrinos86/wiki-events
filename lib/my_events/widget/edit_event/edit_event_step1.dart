import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:google_place/google_place.dart';
import 'package:intl/intl.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/common/provider/image_picker.dart';
import 'package:wiki_events/explore/explore.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class EditEventStep1 extends StatefulWidget {
  final PageController pageController;
  const EditEventStep1({Key? key, required this.pageController})
      : super(key: key);

  @override
  State<EditEventStep1> createState() => _EditEventStep1State();
}

class _EditEventStep1State extends State<EditEventStep1> {
  late TextEditingController _nameController;
  late TextEditingController _venueController;

  String selectedEvent = "Musical Event";

  late FocusNode addressFocusNode;

  late GooglePlace googlePlace;
  static const String placeAPIKey = 'AIzaSyC1i_5c6ZcxMrN3oJdVqTSesN6guLUf33k';

  List<String> pickedImages = [];

  bool isMustBe18 = true;
  bool isRecurringEvent = true;

  List<AutocompletePrediction>? predictions = [];
  AutocompletePrediction? pickedLocation;

  DateTime? pickedDate;
  String displayDate = "22/01/2023";

  DateTime? startDateTime;
  TimeOfDay? pickedStartTime = const TimeOfDay(hour: 8, minute: 00);
  String? displayStartTime = "08:00 AM";

  DateTime? endDateTime;
  TimeOfDay? pickedEndTime = const TimeOfDay(hour: 17, minute: 00);
  String? displayEndTime = "05:00 PM";

  String errorMessage = "";
  bool isImageError = false;
  bool isNameError = false;
  bool isVenueError = false;
  bool isDateError = false;
  bool isStartTimeError = false;
  bool isEndTimeError = false;

  @override
  void initState() {
    addressFocusNode = FocusNode();
    _nameController = TextEditingController();
    _venueController = TextEditingController();

    _nameController.text = "Backstreet Boys";
    _venueController.text = "Melbourne VIC, Australia";

    googlePlace = GooglePlace(placeAPIKey);
    super.initState();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _venueController.dispose();
    addressFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: CustomScrollView(
        slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            fillOverscroll: true,
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 3),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  sizedBoxHeight(25),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Upload images',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(15),
                      SizedBox(
                        height: 100,
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: () async {
                                setState(() {
                                  isImageError = false;
                                });
                                if (FocusScope.of(context).hasFocus) {
                                  FocusScope.of(context).unfocus();
                                  await Future.delayed(
                                      const Duration(milliseconds: 400));
                                }
                                // ignore: use_build_context_synchronously
                                await _pickImagess(context);
                              },
                              child: DottedBorder(
                                radius: const Radius.circular(12),
                                strokeWidth: 1.5,
                                dashPattern: const [8, 8],
                                borderType: BorderType.RRect,
                                color: isImageError ? colorRed : primaryColor,
                                child: Container(
                                  color: colorWhite,
                                  height: 100,
                                  width: 100,
                                  child: Icon(
                                    Icons.file_upload_outlined,
                                    size: 35,
                                    color: isImageError
                                        ? Colors.red
                                        : primaryColor,
                                  ),
                                ),
                              ),
                            ),
                            pickedImages.isNotEmpty
                                ? Expanded(
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: pickedImages.length,
                                      shrinkWrap: true,
                                      itemBuilder: (context, index) {
                                        return Padding(
                                          padding:
                                              const EdgeInsets.only(left: 20),
                                          child: Container(
                                            decoration: common_border_dec(
                                                borderRadius: 10,
                                                borderColor: colorBlack,
                                                borderWidth: 0.5),
                                            height: 100,
                                            width: 100,
                                            child: Stack(
                                              children: [
                                                ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  child: Image.file(
                                                    File(pickedImages[index]),
                                                    height: 100,
                                                    width: 100,
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                                GestureDetector(
                                                  onTap: () {
                                                    setState(() {
                                                      pickedImages
                                                          .removeAt(index);
                                                    });
                                                  },
                                                  child: Align(
                                                    alignment:
                                                        Alignment.topRight,
                                                    child: Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              5),
                                                      decoration:
                                                          common_border_dec(
                                                        borderRadius: 5,
                                                      ),
                                                      child: const Icon(
                                                        Icons.close_sharp,
                                                        color: Colors.red,
                                                        size: 25,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  )
                                : Container()
                          ],
                        ),
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Event Name',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(6),
                      CustomTextField(
                        textController: _nameController,
                        isError: isNameError,
                        keybordType: TextInputType.name,
                        onTapped: () {
                          isNameError = false;
                        },
                        hintText: "",
                        labelText: "Enter event name here",
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Row(
                    children: [
                      SizedBox(
                        height: 20,
                        width: 20,
                        child: Transform.scale(
                          scale: 1,
                          child: Checkbox(
                            activeColor: colorBlack,
                            value: isMustBe18,
                            onChanged: (value) {
                              setState(() {
                                isMustBe18 = value!;
                              });
                            },
                          ),
                        ),
                      ),
                      sizedBoxWidth(10),
                      Text(
                        "Participants must be 18+",
                        style: size14_M_regular(),
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Event Type',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(6),
                      FilterButtonWidget(
                        title: selectedEvent,
                        borderColor: colorGrey,
                        titleTextColor: colorBlack,
                        onPressedCallBack: () {
                          Pages.eventType.push(context, extra: []);
                        },
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Venue / Location',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(8),
                      CustomTextField(
                        hintText: "",
                        labelText: "Enter Venue / Location here",
                        maxline: null,
                        focusNode: addressFocusNode,
                        isError: isVenueError,
                        textController: _venueController,
                        keybordType: TextInputType.text,
                        textInputAction: TextInputAction.done,
                        onChanged: (value) {
                          setState(() {
                            if (value.isNotEmpty) {
                              autoCompleteSearch(value);
                            } else {
                              predictions = [];
                            }
                            pickedLocation = null;
                          });
                        },
                        onTapped: () {
                          setState(() {
                            isVenueError = false;
                          });
                        },
                      ),
                      sizedBoxHeight(8),
                      Container(
                        height: predictions!.isEmpty ||
                                _venueController.text.isEmpty
                            ? 0
                            : 280,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.transparent,
                          border: Border.all(
                            color: colorGrey,
                          ),
                        ),
                        child: ListView.builder(
                          primary: true,
                          shrinkWrap: true,
                          padding: EdgeInsets.zero,
                          itemCount: predictions!.length,
                          itemBuilder: (context, index) {
                            return ListTile(
                              title: Text(predictions![index].description!,
                                  style:
                                      size12_M_medium(textColor: colorBlack)),
                              leading: const CircleAvatar(
                                  radius: 12,
                                  backgroundColor: primaryColor,
                                  child: Icon(Icons.pin_drop,
                                      size: 22, color: colorBlack)),
                              onTap: () {
                                setState(() {
                                  _venueController.text =
                                      predictions![index].description!;

                                  pickedLocation = predictions![index];
                                  predictions = [];
                                  isVenueError = false;
                                });
                              },
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Select Date',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(8),
                      Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 15, vertical: 12),
                        decoration: common_border_dec(
                            borderRadius: 8,
                            borderColor: isDateError ? colorRed : colorGrey),
                        child: Row(
                          children: [
                            Expanded(
                                child: Text(displayDate,
                                    style: size14_M_regular(
                                        textColor: displayDate == "Select Date"
                                            ? colorGrey
                                            : colorBlack))),
                            GestureDetector(
                              onTap: () {
                                setState(() {
                                  isDateError = false;
                                });
                                dateSelection();
                              },
                              child: Icon(Icons.calendar_today,
                                  size: 22,
                                  color: isDateError ? colorRed : primaryColor),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Row(
                    children: [
                      SizedBox(
                          height: 20,
                          width: 20,
                          child: Transform.scale(
                            scale: 1,
                            child: Checkbox(
                              activeColor: colorBlack,
                              value: isRecurringEvent,
                              onChanged: (value) {
                                setState(() {
                                  isRecurringEvent = value!;
                                });
                              },
                            ),
                          )),
                      sizedBoxWidth(10),
                      Text(
                        "Recurring Event",
                        style: size14_M_regular(),
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Start Time",
                              style: size14_M_regular(),
                            ),
                            sizedBoxHeight(8),
                            CommonTimePicker(
                              displayText: displayStartTime,
                              isTimePicked: pickedStartTime != null,
                              isError: isStartTimeError,
                              onSelectedCallBack: (value) async {
                                setState(() {
                                  isStartTimeError = false;
                                });
                                timeSelection(
                                  isStartTime: true,
                                  pickedTime: value,
                                );
                              },
                            )
                          ],
                        ),
                      ),
                      sizedBoxWidth(10),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "End Time",
                              style: size14_M_regular(),
                            ),
                            sizedBoxHeight(8),
                            CommonTimePicker(
                              displayText: displayEndTime,
                              isTimePicked: pickedEndTime != null,
                              isError: isEndTimeError,
                              onSelectedCallBack: (value) {
                                setState(() {
                                  isEndTimeError = false;
                                });
                                timeSelection(
                                  isStartTime: false,
                                  pickedTime: value,
                                );
                              },
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  sizedBoxHeight(30),
                  const Spacer(),
                  Align(
                    alignment: Alignment.center,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize:
                            Size(ScreenSize.getWidth(context, 0.6), 48),
                      ),
                      onPressed: () async {
                        // validation checking
                        // bool isError =
                        //     validationCheck(eventName: _nameController.text);

                        // if (isError) {
                        //   await showOKDialog(
                        //     context: context,
                        //     labelTitle: "Error !",
                        //     labelContent: errorMessage,
                        //   );
                        // } else {
                        widget.pageController.nextPage(
                          duration: const Duration(milliseconds: 300),
                          curve: Curves.easeInOut,
                        );
                        // }
                      },
                      child: const Text("Next"),
                    ),
                  ),
                  sizedBoxHeight(35),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _pickImagess(BuildContext context) async {
    String? imagePath = await ImageUtil().getImage(
      context: context,
    );
    if (imagePath != null && imagePath.isNotEmpty) {
      setState(() {
        pickedImages.add(imagePath);
      });
    }
  }

  void autoCompleteSearch(String value) async {
    var result = await googlePlace.autocomplete.get(value);
    // for check error msg
    // var details = await googlePlace.autocomplete.getJson(value);
    // debugPrint(details.toString());
    //
    if (result != null && result.predictions != null && mounted) {
      setState(() {
        predictions = result.predictions!;
      });
    }
  }

  Future<void> dateSelection() async {
    DateTime? date = await showDatePicker(
      context: context,
      lastDate: DateTime(2101),
      initialDate: DateTime.now(),
      firstDate: DateTime.now().subtract(const Duration(days: 0)),
      builder: (context, child) {
        return Theme(
          data: ThemeData.dark().copyWith(
              colorScheme: const ColorScheme.light(
                  primary: primaryColor,
                  onPrimary: colorWhite,
                  surface: colorBlack,
                  onSurface: colorBlack),
              dialogBackgroundColor: backgroundColor,
              textButtonTheme: TextButtonThemeData(
                  style: TextButton.styleFrom(foregroundColor: colorBlack))),
          child: child!,
        );
      },
    );

    if (date != null) {
      setState(() {
        pickedDate = date;
        displayDate = DateFormat('yyyy-MM-dd').format(pickedDate!).toString();
      });
    } else {
      setState(() {
        pickedDate = null;
        displayDate = "Select Date";
      });
    }
  }

  Future<void> timeSelection(
      {required bool isStartTime, required TimeOfDay? pickedTime}) async {
    DateTime today = DateTime.now();
    if (pickedTime != null) {
      DateTime pickedDatetime = DateTime(today.year, today.month, today.day,
          pickedTime.hour, pickedTime.minute);

      if (pickedDatetime.isBefore(DateTime.now()) ||
          pickedDatetime.isAtSameMomentAs(DateTime.now())) {
        await showOKDialog(
          context: context,
          labelTitle: "Error !",
          labelContent: "Please picked the valid time",
        );
      } else {
        if (isStartTime) {
          if (endDateTime != null && pickedDatetime.isAfter(endDateTime!)) {
            await showOKDialog(
              context: context,
              labelTitle: "Error !",
              labelContent: "Start time must be before end time",
            );
          } else {
            setState(() {
              pickedStartTime = pickedTime;
              startDateTime = pickedDatetime;
              displayStartTime = DateFormat('hh:mm a').format(pickedDatetime);
            });
          }
        } else {
          if (startDateTime != null &&
              pickedDatetime.isBefore(startDateTime!)) {
            await showOKDialog(
              context: context,
              labelTitle: "Error !",
              labelContent: "End time must be after start time",
            );
          } else {
            setState(() {
              pickedEndTime = pickedTime;
              endDateTime = pickedDatetime;
              displayEndTime = DateFormat('hh:mm a').format(pickedDatetime);
            });
          }
        }
      }
    } else {
      if (isStartTime) {
        setState(() {
          pickedStartTime = null;
          displayStartTime = null;
          startDateTime = null;
        });
      } else {
        setState(() {
          endDateTime = null;
          displayEndTime = null;
          pickedEndTime = null;
        });
      }
    }
  }

  bool validationCheck({required String eventName}) {
    bool validationError = false;
    setState(() {
      if (pickedEndTime == null) {
        isEndTimeError = true;
        errorMessage = "Please select end time";
        validationError = true;
      }
      if (pickedStartTime == null) {
        isStartTimeError = true;
        errorMessage = "Please select start time";
        validationError = true;
      }
      if (pickedDate == null) {
        isDateError = true;
        errorMessage = "Please select Date";
        validationError = true;
      }
      if (pickedLocation == null) {
        isVenueError = true;
        errorMessage = "Please select Venue / Location";
        validationError = true;
      }
      if (eventName.isEmpty) {
        isNameError = true;
        errorMessage = "Please enter Event Name ";
        validationError = true;
      }
      if (pickedImages.isEmpty) {
        isImageError = true;
        errorMessage = "Please upload atleast one Image ";
        validationError = true;
      }
    });

    return validationError;
  }
}
