import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/explore/explore.dart';
import 'package:wiki_events/my_events/widget/widget.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class EditEventStep2 extends StatefulWidget {
  final PageController pageController;
  const EditEventStep2({Key? key, required this.pageController})
      : super(key: key);

  @override
  State<EditEventStep2> createState() => _EditEventStep2State();
}

class _EditEventStep2State extends State<EditEventStep2> {
  late TextEditingController _descriptionController;

  String errorMessage = "";
  bool isDescriptionError = false;

  List<String> ticketList = ['www.abc.com'];
  List<String> moreInfoList = ['www.abcd.com', 'www.new.com'];
  List<String> venueInfoList = ['www.old.com', 'www.version.com', 'www.em.com'];

  List<EventFeatureModel> eventFeatureList = [];

  @override
  void initState() {
    _descriptionController = TextEditingController();
    _descriptionController.text =
        "Lorem ipsum dolor sitamet, conseter adipsicing elit, de to edisune fmagna aliwura,";
    super.initState();
  }

  @override
  void dispose() {
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: CustomScrollView(
        slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            fillOverscroll: true,
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 3),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  sizedBoxHeight(25),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Description',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(8),
                      TextFormField(
                        controller: _descriptionController,
                        maxLines: 3,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        keyboardType: TextInputType.text,
                        style: size14_M_medium(),
                        textCapitalization: TextCapitalization.sentences,
                        decoration: InputDecoration(
                          alignLabelWithHint: true,
                          label: const Text(
                            "Type your description",
                            textAlign: TextAlign.start,
                          ),
                          hintText: "",
                          border: OutlineInputBorder(
                              borderSide: const BorderSide(color: colorGrey),
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: const BorderSide(color: colorGrey),
                              borderRadius: BorderRadius.circular(8)),
                        ),
                        onFieldSubmitted: (value) =>
                            FocusScope.of(context).unfocus(),
                        onSaved: (value) => FocusScope.of(context).unfocus(),
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Event Features',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(8),
                      FilterButtonWidget(
                        title: "Select",
                        onPressedCallBack: () async {
                          // Pages.eventFeatures.push(context, extra: []);
                          // Pages.eventFeatures.push(context);
                          final List<
                              EventFeatureModel>? result = await context.push<
                                  List<EventFeatureModel>>(
                              "/bottomNav/explore${Pages.eventFeatures.toPath()}",
                              extra: eventFeatureList);
                          if (result != null && result.isNotEmpty) {
                            setState(() {
                              eventFeatureList = [];
                              eventFeatureList = result;
                              eventFeatureList = eventFeatureList;
                            });
                          }
                        },
                      ),
                    ],
                  ),
                  eventFeatureList.isNotEmpty
                      ? Column(
                          children: [
                            sizedBoxHeight(12),
                            Container(
                              height: eventFeatureList.length > 10
                                  ? 400
                                  : eventFeatureList.length > 8
                                      ? 340
                                      : eventFeatureList.length > 6
                                          ? 270
                                          : eventFeatureList.length > 4
                                              ? 210
                                              : eventFeatureList.length > 2
                                                  ? 150
                                                  : 80,
                              decoration: common_border_dec(
                                  borderRadius: 8,
                                  borderColor: textFieldOutlineColor),
                              child: Padding(
                                padding: const EdgeInsets.symmetric(
                                    horizontal: 6, vertical: 8),
                                child: GridView.builder(
                                    shrinkWrap: true,
                                    itemCount: eventFeatureList.length,
                                    gridDelegate:
                                        const SliverGridDelegateWithFixedCrossAxisCount(
                                            mainAxisSpacing: 8,
                                            crossAxisSpacing: 8,
                                            childAspectRatio: 3,
                                            crossAxisCount: 2),
                                    itemBuilder: (_, index) {
                                      return Container(
                                        padding: const EdgeInsets.symmetric(
                                            horizontal: 8),
                                        decoration: common_dec(
                                            borderRadius: 8,
                                            color: primaryColor),
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.center,
                                          children: [
                                            EventFeatureImage(
                                                size: 26,
                                                imagePath:
                                                    eventFeatureList[index]
                                                        .imageUrl!),
                                            sizedBoxWidth(4),
                                            Expanded(
                                              child: Text(
                                                  eventFeatureList[index].name!,
                                                  style: size11_M_semibold(
                                                      textColor: colorWhite)),
                                            ),
                                            InkWell(
                                              onTap: () {
                                                setState(() {
                                                  eventFeatureList
                                                      .removeAt(index);
                                                });
                                              },
                                              child: const Icon(Icons.cancel,
                                                  color: colorWhite, size: 20),
                                            )
                                          ],
                                        ),
                                      );
                                    }),
                              ),
                            ),
                          ],
                        )
                      : Container(),
                  sizedBoxHeight(30),
                  Container(
                    padding: const EdgeInsets.all(15),
                    width: MediaQuery.of(context).size.width,
                    decoration: common_border_dec(
                        borderRadius: 8, borderColor: darkGreyColor),
                    child: Column(
                      children: [
                        LinkDetailWidget(
                          title: "Ticket Links",
                          linkList: ticketList,
                          linkSaveCallBack: (List<String> list) {
                            if (list.isNotEmpty) {
                              setState(() {
                                ticketList = list;
                              });
                            }
                          },
                        ),
                        LinkDetailWidget(
                          title: "More Info Links",
                          linkList: moreInfoList,
                          linkSaveCallBack: (List<String> list) {
                            if (list.isNotEmpty) {
                              setState(() {
                                moreInfoList = list;
                              });
                            }
                          },
                        ),
                        LinkDetailWidget(
                          title: "Venue Info Link",
                          linkList: venueInfoList,
                          isDeviderAvailable: false,
                          linkSaveCallBack: (List<String> list) {
                            if (list.isNotEmpty) {
                              setState(() {
                                venueInfoList = list;
                              });
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                  sizedBoxHeight(30),
                  Align(
                    alignment: Alignment.center,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize:
                            Size(ScreenSize.getWidth(context, 0.6), 48),
                      ),
                      onPressed: () async {
                        // validation checking
                        // bool isError =
                        //     validationCheck(eventName: _nameController.text);

                        // if (isError) {
                        //   await showOKDialog(
                        //     context: context,
                        //     labelTitle: "Error !",
                        //     labelContent: errorMessage,
                        //   );
                        // } else {
                        Pages.myEvents.push(context);
                        // }
                      },
                      child: const Text("Save Event"),
                    ),
                  ),
                  sizedBoxHeight(35),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  bool validationCheck({required String eventName}) {
    bool validationError = false;
    setState(() {
      // if (pickedEndTime == null) {
      //   isEndTimeError = true;
      //   errorMessage = "Please select end time";
      //   validationError = true;
      // }
    });

    return validationError;
  }
}
