import 'package:flutter/material.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/utils/utils.dart';

// ignore: must_be_immutable
class LinkDetailWidget extends StatefulWidget {
  String title;
  List<String> linkList;
  bool isDeviderAvailable;
  Function linkSaveCallBack;

  LinkDetailWidget({
    super.key,
    required this.title,
    required this.linkList,
    required this.linkSaveCallBack,
    this.isDeviderAvailable = true,
  });

  @override
  State<LinkDetailWidget> createState() => _LinkDetailWidgetState();
}

class _LinkDetailWidgetState extends State<LinkDetailWidget> {
  late TextEditingController textEditingController1;
  late TextEditingController textEditingController2;
  late TextEditingController textEditingController3;

  @override
  void initState() {
    textEditingController1 = TextEditingController();
    textEditingController2 = TextEditingController();
    textEditingController3 = TextEditingController();
    super.initState();
  }

  checkAndSetLinks() {
    if (widget.linkList.isNotEmpty) {
      textEditingController1.text =
          widget.linkList[0].isNotEmpty ? widget.linkList[0] : "";
      if (widget.linkList.length > 1) {
        textEditingController2.text =
            widget.linkList[1].isNotEmpty ? widget.linkList[1] : "";
      }
      if (widget.linkList.length > 2) {
        textEditingController3.text =
            widget.linkList[2].isNotEmpty ? widget.linkList[2] : "";
      }
    }
  }

  @override
  void dispose() {
    textEditingController1.dispose();
    textEditingController2.dispose();
    textEditingController3.dispose();

    super.dispose();
  }

  void closePopup() {
    setState(() {
      textEditingController1.text = "";
      textEditingController2.text = "";
      textEditingController3.text = "";
    });
    checkAndSetLinks();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          widget.title,
          style: size14_M_regular(),
        ),
        sizedBoxHeight(10),
        GestureDetector(
          onTap: () {
            showAddLinkPopup();
          },
          child: Row(
            children: [
              const Icon(Icons.add_circle, color: primaryColor, size: 25),
              sizedBoxWidth(10),
              Text(
                "Add Links",
                style: size14_M_regular(textColor: primaryColor),
              ),
            ],
          ),
        ),
        sizedBoxHeight(10),
        widget.linkList.isNotEmpty
            ? SizedBox(
                height: widget.linkList.length == 1
                    ? 20
                    : widget.linkList.length == 2
                        ? 40
                        : widget.linkList.length == 3
                            ? 60
                            : 0,
                child: ListView.builder(
                    physics: const NeverScrollableScrollPhysics(),
                    shrinkWrap: true,
                    itemCount: widget.linkList.length,
                    itemBuilder: (context, index) {
                      return Text(widget.linkList[index]);
                    }),
              )
            : Container(),
        sizedBoxHeight(10),
        widget.isDeviderAvailable ? const Divider() : Container(),
        widget.isDeviderAvailable ? sizedBoxHeight(10) : Container(),
      ],
    );
  }

  void showAddLinkPopup() {
    checkAndSetLinks();
    showModalBottomSheet(
        context: context,
        builder: (builder) {
          return Container(
            padding: const EdgeInsets.all(20),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height * 0.515,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      "Add Links",
                      style: size16_M_semibold(),
                    ),
                    GestureDetector(
                      onTap: () {
                        closePopup();
                        Navigator.pop(context);
                      },
                      child: const Icon(
                        Icons.close,
                        color: colorRed,
                        size: 25,
                      ),
                    )
                  ],
                ),
                sizedBoxHeight(30),
                CustomTextField(
                    textController: textEditingController1,
                    keybordType: TextInputType.text,
                    isError: false,
                    labelText: "Enter Website URL here",
                    onTapped: () {}),
                sizedBoxHeight(20),
                CustomTextField(
                    textController: textEditingController2,
                    keybordType: TextInputType.text,
                    isError: false,
                    labelText: "Enter Website URL here",
                    onTapped: () {}),
                sizedBoxHeight(20),
                CustomTextField(
                  textController: textEditingController3,
                  keybordType: TextInputType.text,
                  isError: false,
                  labelText: "Enter Website URL here",
                  onTapped: () {},
                ),
                const Spacer(),
                Align(
                  alignment: Alignment.center,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      minimumSize: Size(ScreenSize.getWidth(context, 0.6), 48),
                    ),
                    onPressed: () async {
                      String? isValidate = hasValidUrl();

                      if (isValidate != null) {
                        await showOKDialog(
                          context: context,
                          labelTitle: "Error !",
                          labelContent: isValidate,
                        );
                      } else {
                        setState(() {
                          widget.linkList.clear();
                          if (textEditingController1.text.isNotEmpty) {
                            widget.linkList.add(textEditingController1.text);
                          }
                          if (textEditingController2.text.isNotEmpty) {
                            widget.linkList.add(textEditingController2.text);
                          }
                          if (textEditingController3.text.isNotEmpty) {
                            widget.linkList.add(textEditingController3.text);
                          }
                        });

                        widget.linkSaveCallBack(widget.linkList);
                        Navigator.pop(context);
                      }
                    },
                    child: const Text("Save "),
                  ),
                ),
                sizedBoxHeight(35),
              ],
            ),
          );
        });
  }

  String? hasValidUrl() {
    String pattern =
        '^www.(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]';
    RegExp regExp = RegExp(pattern);
    if (textEditingController1.text.isNotEmpty &&
        !regExp.hasMatch(textEditingController1.text)) {
      return 'Please enter valid URLs';
    }
    if (textEditingController2.text.isNotEmpty &&
        !regExp.hasMatch(textEditingController2.text)) {
      return 'Please enter valid URLs';
    }
    if (textEditingController3.text.isNotEmpty &&
        !regExp.hasMatch(textEditingController3.text)) {
      return 'Please enter valid URLs';
    }
    return null;
  }
}
