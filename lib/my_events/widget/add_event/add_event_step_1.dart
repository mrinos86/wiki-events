import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:google_place/google_place.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/common/provider/image_picker.dart';
import 'package:wiki_events/events/model/event_model.dart';
import 'package:wiki_events/explore/explore.dart';
import 'package:wiki_events/my_events/my_event.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class AddEventStep1 extends StatefulWidget {
  final PageController pageController;
  const AddEventStep1({Key? key, required this.pageController})
      : super(key: key);

  @override
  State<AddEventStep1> createState() => _AddEventStep1State();
}

class _AddEventStep1State extends State<AddEventStep1> {
  late TextEditingController _nameController;
  late TextEditingController _venueController;

  late FocusNode addressFocusNode;

  late GooglePlace googlePlace;
  static const String placeAPIKey = 'AIzaSyC1i_5c6ZcxMrN3oJdVqTSesN6guLUf33k';

  bool isRecurringEvent = false;

  late MyEventProvider myEventProvider;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      Provider.of<MyEventProvider>(context, listen: false).initProvider();
    });
    addressFocusNode = FocusNode();
    _nameController = TextEditingController();
    _venueController = TextEditingController();
    googlePlace = GooglePlace(placeAPIKey);
    super.initState();
  }

  @override
  void dispose() {
    _nameController.dispose();
    _venueController.dispose();
    addressFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    myEventProvider = Provider.of<MyEventProvider>(context, listen: true);
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: CustomScrollView(
        slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            fillOverscroll: true,
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 3),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  sizedBoxHeight(25),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Upload images',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(15),
                      SizedBox(
                        height: 100,
                        child: Row(
                          children: [
                            GestureDetector(
                              onTap: () async {
                                myEventProvider.setImageError = false;
                                if (FocusScope.of(context).hasFocus) {
                                  FocusScope.of(context).unfocus();
                                  await Future.delayed(
                                      const Duration(milliseconds: 400));
                                }
                                // ignore: use_build_context_synchronously
                                await _pickImagess(context);
                              },
                              child: DottedBorder(
                                radius: const Radius.circular(10),
                                strokeWidth: 1.5,
                                dashPattern: const [8, 8],
                                borderType: BorderType.RRect,
                                color: myEventProvider.isImageError
                                    ? colorRed
                                    : primaryColor,
                                child: Container(
                                  color: colorWhite,
                                  height: 100,
                                  width: 100,
                                  child: Icon(
                                    Icons.file_upload_outlined,
                                    size: 35,
                                    color: myEventProvider.isImageError
                                        ? Colors.red
                                        : primaryColor,
                                  ),
                                ),
                              ),
                            ),
                            myEventProvider.pickedImageList.isNotEmpty
                                ? Expanded(
                                    child: ListView.builder(
                                      scrollDirection: Axis.horizontal,
                                      itemCount: myEventProvider
                                          .pickedImageList.length,
                                      shrinkWrap: true,
                                      itemBuilder: (context, index) {
                                        return Padding(
                                          padding:
                                              const EdgeInsets.only(left: 20),
                                          child: Container(
                                            decoration: common_border_dec(
                                                borderRadius: 10,
                                                borderColor: colorBlack,
                                                borderWidth: 0.5),
                                            height: 100,
                                            width: 100,
                                            child: Stack(
                                              children: [
                                                ClipRRect(
                                                  borderRadius:
                                                      BorderRadius.circular(10),
                                                  child: Image.file(
                                                    File(myEventProvider
                                                            .pickedImageList[
                                                        index]),
                                                    height: 100,
                                                    width: 100,
                                                    fit: BoxFit.cover,
                                                  ),
                                                ),
                                                GestureDetector(
                                                  onTap: () {
                                                    myEventProvider
                                                        .pickedImageList
                                                        .removeAt(index);
                                                    myEventProvider
                                                            .setPickedImageList =
                                                        myEventProvider
                                                            .pickedImageList;
                                                  },
                                                  child: Align(
                                                    alignment:
                                                        Alignment.topRight,
                                                    child: Container(
                                                      margin:
                                                          const EdgeInsets.all(
                                                              5),
                                                      decoration:
                                                          common_border_dec(
                                                        borderRadius: 5,
                                                      ),
                                                      child: const Icon(
                                                        Icons.close_sharp,
                                                        color: Colors.red,
                                                        size: 25,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        );
                                      },
                                    ),
                                  )
                                : Container()
                          ],
                        ),
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Event Name',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(6),
                      CustomTextField(
                        textController: _nameController,
                        isError: myEventProvider.isNameError,
                        keybordType: TextInputType.name,
                        onTapped: () {
                          myEventProvider.isNameError = false;
                        },
                        hintText: "",
                        labelText: "Enter event name here",
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Row(
                    children: [
                      SizedBox(
                        height: 20,
                        width: 20,
                        child: Transform.scale(
                          scale: 1,
                          child: Checkbox(
                            activeColor: colorBlack,
                            value: myEventProvider.is18Plus,
                            onChanged: (value) {
                              myEventProvider.setIs18Plus = value!;
                            },
                          ),
                        ),
                      ),
                      sizedBoxWidth(10),
                      Text(
                        "Participants must be 18+",
                        style: size14_M_regular(),
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Event Type',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(6),
                      FilterButtonWidget(
                        title: myEventProvider.selectedEventType.isNotEmpty
                            ? myEventProvider.selectedEventType[0].name!
                            : "Select event type",
                        borderColor: myEventProvider.isTypeError
                            ? errorColor
                            : colorGrey,
                        titleTextColor:
                            myEventProvider.selectedEventType.isNotEmpty
                                ? colorBlack
                                : colorGrey,
                        onPressedCallBack: () async {
                          myEventProvider.isTypeError = false;
                          final List<
                              EventTypeModel>? result = await context.push<
                                  List<EventTypeModel>>(
                              "/bottomNav${Pages.explore.toPath()}${Pages.eventType.toPath()}",
                              extra: myEventProvider.selectedEventType);
                          if (result != null && result.isNotEmpty) {
                            myEventProvider.setEventTypes = [];
                            myEventProvider.setEventTypes = result;
                          }
                        },
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Venue / Location',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(8),
                      CustomTextField(
                        hintText: "",
                        labelText: "Enter Venue / Location here",
                        maxline: null,
                        focusNode: addressFocusNode,
                        isError: myEventProvider.isLocationError,
                        textController: _venueController,
                        keybordType: TextInputType.text,
                        textInputAction: TextInputAction.done,
                        onChanged: (value) {
                          if (value.isNotEmpty) {
                            autoCompleteSearch(value);
                          } else {
                            myEventProvider.setSearchedPredictions = [];
                          }
                          myEventProvider.pickedLocation = null;
                        },
                        onTapped: () {
                          myEventProvider.isLocationError = false;
                        },
                      ),
                      sizedBoxHeight(8),
                      Container(
                        height: myEventProvider.predictions!.isEmpty ||
                                _venueController.text.isEmpty
                            ? 0
                            : 280,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Colors.transparent,
                          border: Border.all(
                            color: colorGrey,
                          ),
                        ),
                        child: ListView.builder(
                          primary: true,
                          shrinkWrap: true,
                          padding: EdgeInsets.zero,
                          itemCount: myEventProvider.predictions!.length,
                          itemBuilder: (context, index) {
                            return ListTile(
                              title: Text(
                                  myEventProvider
                                      .predictions![index].description!,
                                  style:
                                      size12_M_medium(textColor: colorBlack)),
                              leading: const CircleAvatar(
                                  radius: 12,
                                  backgroundColor: primaryColor,
                                  child: Icon(Icons.pin_drop,
                                      size: 22, color: colorBlack)),
                              onTap: () {
                                _venueController.text = myEventProvider
                                    .predictions![index].description!;

                                myEventProvider.pickedLocation =
                                    myEventProvider.predictions![index];

                                myEventProvider.setSearchedPredictions = [];

                                myEventProvider.isLocationError = false;
                              },
                            );
                          },
                        ),
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Select Date',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(8),
                      Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 15, vertical: 12),
                        decoration: common_border_dec(
                            borderRadius: 8,
                            borderColor: myEventProvider.isDateError
                                ? colorRed
                                : colorGrey),
                        child: Row(
                          children: [
                            Expanded(
                                child: Text(
                                    displayOnlyDate(
                                            myEventProvider.pickedDate) ??
                                        "Select Date",
                                    style: size14_M_regular(
                                        textColor:
                                            myEventProvider.pickedDate == null
                                                ? colorGrey
                                                : colorBlack))),
                            GestureDetector(
                              onTap: () {
                                myEventProvider.isDateError = false;

                                dateSelection();
                              },
                              child: Icon(Icons.calendar_today,
                                  size: 22,
                                  color: myEventProvider.isDateError
                                      ? colorRed
                                      : primaryColor),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Row(
                    children: [
                      SizedBox(
                          height: 20,
                          width: 20,
                          child: Transform.scale(
                            scale: 1,
                            child: Checkbox(
                              activeColor: colorBlack,
                              value: isRecurringEvent,
                              onChanged: (value) {
                                setState(() {
                                  isRecurringEvent = value!;
                                });
                              },
                            ),
                          )),
                      sizedBoxWidth(10),
                      Text(
                        "Recurring Event",
                        style: size14_M_regular(),
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "Start Time",
                              style: size14_M_regular(),
                            ),
                            sizedBoxHeight(8),
                            CommonTimePicker(
                              displayText: displayOnlyTime(
                                  myEventProvider.pickedStartTime),
                              isTimePicked:
                                  myEventProvider.pickedStartTime != null,
                              isError: myEventProvider.isStartTimeError,
                              onSelectedCallBack: (value) async {
                                myEventProvider.isStartTimeError = false;
                                timeSelection(
                                  isStartTime: true,
                                  pickedTime: value,
                                );
                              },
                            )
                          ],
                        ),
                      ),
                      sizedBoxWidth(10),
                      Expanded(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              "End Time",
                              style: size14_M_regular(),
                            ),
                            sizedBoxHeight(8),
                            CommonTimePicker(
                              displayText: displayOnlyTime(
                                  myEventProvider.pickedEndTime),
                              isTimePicked:
                                  myEventProvider.pickedEndTime != null,
                              isError: myEventProvider.isEndTimeError,
                              onSelectedCallBack: (value) {
                                myEventProvider.isEndTimeError = false;
                                timeSelection(
                                  isStartTime: false,
                                  pickedTime: value,
                                );
                              },
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                  sizedBoxHeight(30),
                  const Spacer(),
                  Align(
                    alignment: Alignment.center,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize:
                            Size(ScreenSize.getWidth(context, 0.6), 48),
                      ),
                      onPressed: onPressedCheckForDuplicateButton,
                      child: const Text("Check for Duplicates"),
                    ),
                  ),
                  sizedBoxHeight(35),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> _pickImagess(BuildContext context) async {
    String? imagePath = await ImageUtil().getImage(
      context: context,
    );
    if (imagePath != null && imagePath.isNotEmpty) {
      myEventProvider.pickedImageList.add(imagePath);
      myEventProvider.setPickedImageList = myEventProvider.pickedImageList;
    }
  }

  void autoCompleteSearch(String value) async {
    var result = await googlePlace.autocomplete.get(value);
    // for check error msg
    // var details = await googlePlace.autocomplete.getJson(value);
    // debugPrint(details.toString());
    //
    if (result != null && result.predictions != null && mounted) {
      Provider.of<MyEventProvider>(context, listen: false)
          .setSearchedPredictions = result.predictions!;
    }
  }

  Future<DetailsResult?> getDetails(String placeId) async {
    var result = await googlePlace.details.get(placeId);
    if (result != null && result.result != null && mounted) {
      DetailsResult? locationDetailsResult = result.result!;
      return locationDetailsResult;
    }
    return null;
  }

  Future<void> dateSelection() async {
    DateTime? date = await showDatePicker(
      context: context,
      lastDate: DateTime(2101),
      initialDate: DateTime.now(),
      firstDate: DateTime.now().subtract(const Duration(days: 0)),
      builder: (context, child) {
        return Theme(
          data: ThemeData.dark().copyWith(
              colorScheme: const ColorScheme.light(
                  primary: primaryColor,
                  onPrimary: colorWhite,
                  surface: colorBlack,
                  onSurface: colorBlack),
              dialogBackgroundColor: backgroundColor,
              textButtonTheme: TextButtonThemeData(
                  style: TextButton.styleFrom(foregroundColor: colorBlack))),
          child: child!,
        );
      },
    );

    if (date != null) {
      myEventProvider.setPickedDate = date;
      myEventProvider.setPickedStartTime = null;
      myEventProvider.setPickedEndTime = null;
    } else {
      myEventProvider.setPickedDate = null;
    }
  }

  Future<void> timeSelection(
      {required bool isStartTime, required TimeOfDay? pickedTime}) async {
    late DateTime pickedDate;
    if (myEventProvider.pickedDate != null) {
      pickedDate = myEventProvider.pickedDate!;

      if (pickedTime != null) {
        DateTime pickedDatetime = DateTime(pickedDate.year, pickedDate.month,
            pickedDate.day, pickedTime.hour, pickedTime.minute);

        if (pickedDatetime.isBefore(DateTime.now()) ||
            pickedDatetime.isAtSameMomentAs(DateTime.now())) {
          await showOKDialog(
            context: context,
            labelTitle: "Error !",
            labelContent: "Please picked the valid time",
          );
        } else {
          if (isStartTime) {
            if (myEventProvider.pickedEndTime != null) {
              DateTime endDateTime = DateTime(
                  pickedDate.year,
                  pickedDate.month,
                  pickedDate.day,
                  myEventProvider.pickedEndTime!.hour,
                  myEventProvider.pickedEndTime!.minute);
              if (myEventProvider.pickedEndTime != null &&
                  pickedDatetime.isAfter(endDateTime)) {
                await showOKDialog(
                  context: context,
                  labelTitle: "Error !",
                  labelContent: "Start time must be before end time",
                );
                return;
              }
            }
            myEventProvider.setPickedStartTime = pickedTime;
          } else {
            if (myEventProvider.pickedStartTime != null) {
              DateTime startDateTime = DateTime(
                  pickedDate.year,
                  pickedDate.month,
                  pickedDate.day,
                  myEventProvider.pickedStartTime!.hour,
                  myEventProvider.pickedStartTime!.minute);
              if (myEventProvider.pickedStartTime != null &&
                  pickedDatetime.isBefore(startDateTime)) {
                await showOKDialog(
                  context: context,
                  labelTitle: "Error !",
                  labelContent: "End time must be after start time",
                );
                return;
              }
            }
            myEventProvider.setPickedEndTime = pickedTime;
          }
        }
      } else {
        if (isStartTime) {
          myEventProvider.setPickedStartTime = null;
        } else {
          myEventProvider.setPickedEndTime = null;
        }
      }
    } else if (myEventProvider.pickedDate == null && pickedTime != null) {
      await showOKDialog(
        context: context,
        labelTitle: "Error !",
        labelContent: "Please pick date first",
      );
    }
  }

  void onPressedCheckForDuplicateButton() async {
    bool isError =
        myEventProvider.validationCheckStep1(eventName: _nameController.text);

    if (isError) {
      await showOKDialog(
        context: context,
        labelTitle: "Error !",
        labelContent: myEventProvider.errorMessage,
      );
    } else {
      try {
        AutocompletePrediction? location = myEventProvider.pickedLocation;
        DetailsResult? locationDetails =
            await getDetails(location!.placeId ?? "");

        List<File> imageList = [];
        for (var image in myEventProvider.pickedImageList) {
          imageList.add(File(image));
        }

        List<String> typeIds = [];
        for (var element in myEventProvider.selectedEventType) {
          typeIds.add(element.id!);
        }

        AddEventRequestParam request = AddEventRequestParam(
          eventImages: imageList,
          name: _nameController.text,
          ageRestricted: myEventProvider.is18Plus ? 1 : 0,
          typeIds: typeIds.join(','),
          location: location.description,
          lat: locationDetails!.geometry!.location!.lat!.toString(),
          long: locationDetails.geometry!.location!.lng!.toString(),
          date: displayOnlyDate(myEventProvider.pickedDate),
          startTime: displayOnlyTime(myEventProvider.pickedStartTime),
          endTime: displayOnlyTime(myEventProvider.pickedEndTime),
        );

        myEventProvider.setAddEventRequestParam = request;
        // ignore: use_build_context_synchronously
        Pages.checkDuplicateEvent.push(context);
      } catch (e) {
        await showOKDialog(
          context: context,
          labelTitle: "Error !",
          labelContent: "",
        );
      }
    }
  }
}
