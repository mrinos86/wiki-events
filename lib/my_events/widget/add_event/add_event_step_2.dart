import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:wiki_events/app/app.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/events/events.dart';
import 'package:wiki_events/explore/explore.dart';
import 'package:wiki_events/my_events/my_event.dart';
import 'package:wiki_events/router/router.dart';
import 'package:wiki_events/utils/utils.dart';

class AddEventStep2 extends StatefulWidget {
  final PageController pageController;
  const AddEventStep2({Key? key, required this.pageController})
      : super(key: key);

  @override
  State<AddEventStep2> createState() => _AddEventStep2State();
}

class _AddEventStep2State extends State<AddEventStep2> {
  late TextEditingController _descriptionController;

  List<EventFeatureModel> eventFeatureList = [];

  List<String> ticketList = [];
  List<String> moreInfoList = [];
  List<String> venueInfoList = [];

  late MyEventProvider myEventProvider;

  @override
  void initState() {
    _descriptionController = TextEditingController();
    super.initState();
  }

  @override
  void dispose() {
    _descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    myEventProvider = Provider.of<MyEventProvider>(context, listen: true);
    eventFeatureList = myEventProvider.selectedEventFeatures;
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: CustomScrollView(
        slivers: [
          SliverFillRemaining(
            hasScrollBody: false,
            fillOverscroll: true,
            child: Container(
              width: double.infinity,
              padding: const EdgeInsets.symmetric(horizontal: 3),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  sizedBoxHeight(25),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Description',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(8),
                      TextFormField(
                        controller: _descriptionController,
                        maxLines: 3,
                        autovalidateMode: AutovalidateMode.onUserInteraction,
                        keyboardType: TextInputType.text,
                        style: size14_M_medium(),
                        textCapitalization: TextCapitalization.sentences,
                        decoration: InputDecoration(
                          alignLabelWithHint: true,
                          label: const Text(
                            "Type your description",
                            textAlign: TextAlign.start,
                          ),
                          hintText: "",
                          border: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: myEventProvider.isDescriptionError
                                      ? errorColor
                                      : colorGrey),
                              borderRadius: BorderRadius.circular(8)),
                          enabledBorder: OutlineInputBorder(
                              borderSide: BorderSide(
                                  color: myEventProvider.isDescriptionError
                                      ? errorColor
                                      : colorGrey),
                              borderRadius: BorderRadius.circular(8)),
                        ),
                        onFieldSubmitted: (value) =>
                            FocusScope.of(context).unfocus(),
                        onSaved: (value) => FocusScope.of(context).unfocus(),
                        onTap: () {
                          myEventProvider.isDescriptionError = false;
                        },
                      ),
                    ],
                  ),
                  sizedBoxHeight(25),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'Event Features',
                        style: size14_M_regular(),
                      ),
                      sizedBoxHeight(8),
                      FilterButtonWidget(
                        title: "Select",
                        borderColor: myEventProvider.isEventFeatureError
                            ? errorColor
                            : colorGrey,
                        onPressedCallBack: () async {
                          myEventProvider.isEventFeatureError = false;
                          final List<
                              EventFeatureModel>? result = await context.push<
                                  List<EventFeatureModel>>(
                              "/bottomNav${Pages.explore.toPath()}${Pages.eventFeatures.toPath()}",
                              extra: myEventProvider.selectedEventFeatures);
                          if (result != null && result.isNotEmpty) {
                            myEventProvider.setEventFeatures = [];
                            myEventProvider.setEventFeatures = result;
                          }
                        },
                      ),
                      eventFeatureList.isNotEmpty
                          ? Column(
                              children: [
                                sizedBoxHeight(12),
                                Container(
                                  height: eventFeatureList.length > 10
                                      ? 400
                                      : eventFeatureList.length > 8
                                          ? 340
                                          : eventFeatureList.length > 6
                                              ? 270
                                              : eventFeatureList.length > 4
                                                  ? 210
                                                  : eventFeatureList.length > 2
                                                      ? 150
                                                      : 80,
                                  decoration: common_border_dec(
                                      borderRadius: 8,
                                      borderColor: textFieldOutlineColor),
                                  child: Padding(
                                    padding: const EdgeInsets.symmetric(
                                        horizontal: 6, vertical: 8),
                                    child: GridView.builder(
                                        shrinkWrap: true,
                                        itemCount: eventFeatureList.length,
                                        gridDelegate:
                                            const SliverGridDelegateWithFixedCrossAxisCount(
                                                mainAxisSpacing: 8,
                                                mainAxisExtent: 55,
                                                crossAxisSpacing: 8,
                                                crossAxisCount: 2),
                                        itemBuilder: (_, index) {
                                          return Container(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 8),
                                            decoration: common_dec(
                                                borderRadius: 8,
                                                color: primaryColor),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                EventFeatureImage(
                                                    size: 26,
                                                    imagePath:
                                                        eventFeatureList[index]
                                                            .imageUrl!),
                                                sizedBoxWidth(4),
                                                Expanded(
                                                  child: Text(
                                                      eventFeatureList[index]
                                                          .name!,
                                                      style: size11_M_semibold(
                                                          textColor:
                                                              colorWhite)),
                                                ),
                                                InkWell(
                                                  onTap: () {
                                                    eventFeatureList
                                                        .removeAt(index);
                                                    myEventProvider
                                                            .setEventFeatures =
                                                        eventFeatureList;
                                                  },
                                                  child: const Icon(
                                                      Icons.cancel,
                                                      color: colorWhite,
                                                      size: 20),
                                                )
                                              ],
                                            ),
                                          );
                                        }),
                                  ),
                                ),
                              ],
                            )
                          : Container()
                    ],
                  ),
                  sizedBoxHeight(30),
                  Container(
                    padding: const EdgeInsets.all(15),
                    width: MediaQuery.of(context).size.width,
                    decoration: common_border_dec(
                        borderRadius: 8, borderColor: darkGreyColor),
                    child: Column(
                      children: [
                        LinkDetailWidget(
                          title: "Ticket Links",
                          linkList: ticketList,
                          linkSaveCallBack: (List<String> list) {
                            if (list.isNotEmpty) {
                              setState(() {
                                ticketList = list;
                              });
                            }
                          },
                        ),
                        LinkDetailWidget(
                          title: "More Info Links",
                          linkList: moreInfoList,
                          linkSaveCallBack: (List<String> list) {
                            if (list.isNotEmpty) {
                              setState(() {
                                moreInfoList = list;
                              });
                            }
                          },
                        ),
                        LinkDetailWidget(
                          title: "Venue Info Link",
                          linkList: venueInfoList,
                          isDeviderAvailable: false,
                          linkSaveCallBack: (List<String> list) {
                            if (list.isNotEmpty) {
                              setState(() {
                                venueInfoList = list;
                              });
                            }
                          },
                        ),
                      ],
                    ),
                  ),
                  sizedBoxHeight(30),
                  Align(
                    alignment: Alignment.center,
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize:
                            Size(ScreenSize.getWidth(context, 0.6), 48),
                      ),
                      onPressed: onPressedSaveButton,
                      child: const Text("Save Event"),
                    ),
                  ),
                  sizedBoxHeight(35),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  onPressedSaveButton() async {
    bool isError = myEventProvider.validationCheckStep2(
        eventDes: _descriptionController.text);

    if (isError) {
      await showOKDialog(
        context: context,
        labelTitle: "Error !",
        labelContent: myEventProvider.errorMessage,
      );
    } else {
      List<String> featureIds = [];
      for (var element in myEventProvider.selectedEventFeatures) {
        featureIds.add(element.id!);
      }

      AddEventRequestParam providerRequest =
          myEventProvider.addEventRequestParam!;

      AddEventRequestParam request = AddEventRequestParam(
        eventImages: providerRequest.eventImages,
        name: providerRequest.name,
        ageRestricted: providerRequest.ageRestricted,
        typeIds: providerRequest.typeIds,
        location: providerRequest.location,
        lat: providerRequest.lat,
        long: providerRequest.long,
        date: providerRequest.date,
        startTime: providerRequest.startTime,
        endTime: providerRequest.endTime,
        description: _descriptionController.text,
        featureIds: featureIds.join(','),
      );
      myEventProvider.setAddEventRequestParam = request;

      myEventProvider.setLoading = true;
      ApiResponse response =
          await MyEventRepository.instance.addNewEvent(request);
      myEventProvider.setLoading = false;

      if (!response.result && mounted) {
        await showOKDialog(
          context: context,
          labelTitle: "Error !",
          labelContent: response.message,
        );
      } else if (response.result && mounted) {
        Pages.myEvents.go(context);
      }
    }
  }
}
