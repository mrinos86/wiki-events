import 'package:dio/dio.dart';

class ProfileUpdateRequestParam {
  String? firstName;
  String? lastName;
  String? areaCode;
  String? phone;
  String? about;
  int? categoryId;
  String? homeLocation;
  String? homeLat;
  String? homeLong;

  ProfileUpdateRequestParam({
    required this.firstName,
    required this.lastName,
    required this.areaCode,
    required this.phone,
    this.about,
    required this.categoryId,
    required this.homeLocation,
    required this.homeLat,
    required this.homeLong,
  });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};

    data['first_name'] = firstName;
    data['last_name'] = lastName;
    data['area_code'] = areaCode;
    data['phone'] = phone;
    data['about'] = about;
    data['category_id'] = categoryId;
    data['home_location'] = homeLocation;
    data['home_latitude'] = homeLat;
    data['home_longitude'] = homeLong;
    return data;
  }

  FormData toFormData() =>
      FormData.fromMap(toJson(), ListFormat.multiCompatible);
}
