import 'package:flutter/material.dart';
import 'package:wiki_events/app/app.dart';
import 'package:wiki_events/auth/auth.dart';
import 'package:wiki_events/profile/profile.dart';
import 'package:wiki_events/utils/utils.dart';

class AllSet extends StatefulWidget {
  final PageController pageController;
  const AllSet({Key? key, required this.pageController}) : super(key: key);

  @override
  State<AllSet> createState() => _AllSetState();
}

class _AllSetState extends State<AllSet> {
  @override
  Widget build(BuildContext context) {
    return CustomScrollView(
      slivers: [
        SliverFillRemaining(
          hasScrollBody: false,
          fillOverscroll: true,
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(horizontal: 3),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                sizedBoxHeight(60),
                // ignore: prefer_const_constructors
                SizedBox(
                  height: 250,
                  width: 270,
                  child: const Image(
                    image: AssetImage(AppAssets.COMPLETE_PROFILE),
                    fit: BoxFit.fitWidth,
                  ),
                ),
                sizedBoxHeight(20),
                Text(
                  "You're all set",
                  textAlign: TextAlign.center,
                  style: size20_M_semibold(textColor: primaryColor),
                ),
                sizedBoxHeight(15),
                Text(
                  "Welcome to WikiEvents. \nContribute and share events everywhere around Australia",
                  textAlign: TextAlign.center,
                  style: size16_M_regular(),
                ),
                const Spacer(),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    minimumSize: Size(ScreenSize.getWidth(context, 0.6), 48),
                  ),
                  onPressed: () async {
                    Provider.of<ProfileProvider>(context, listen: false)
                        .clearProfileProviderData();
                    AppStateServiceProvider.instance.showLoader;
                    await AuthServiceProvider.instance.refreshUser(null);
                    AppStateServiceProvider.instance.hideLoader;
                  },
                  child: const Text("Explore"),
                ),
                sizedBoxHeight(ScreenSize.getHeight(context, 0.05)),
              ],
            ),
          ),
        ),
      ],
    );
  }
}
