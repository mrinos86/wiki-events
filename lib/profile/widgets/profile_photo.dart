import 'dart:io';

import 'package:flutter/material.dart';
import 'package:wiki_events/app/provider/provider.dart';
import 'package:wiki_events/common/models/models.dart';
import 'package:wiki_events/common/provider/image_picker.dart';
import 'package:wiki_events/profile/profile.dart';
import 'package:wiki_events/utils/utils.dart';

class ProfilePhoto extends StatefulWidget {
  final PageController pageController;
  const ProfilePhoto({
    Key? key,
    required this.pageController,
  }) : super(key: key);

  @override
  State<ProfilePhoto> createState() => _ProfilePhotoState();
}

class _ProfilePhotoState extends State<ProfilePhoto> {
  late ProfileProvider profileProvider;

  @override
  Widget build(BuildContext context) {
    profileProvider = Provider.of<ProfileProvider>(context, listen: true);
    return CustomScrollView(
      slivers: [
        SliverFillRemaining(
          hasScrollBody: false,
          fillOverscroll: true,
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(horizontal: 3),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                sizedBoxHeight(15),
                Text(
                  'Upload a profile photo',
                  style: size16_M_semibold(),
                ),
                sizedBoxHeight(50),
                Center(
                  child: GestureDetector(
                    onTap: () async {
                      await _uploadProfileImage(context);
                    },
                    child: SizedBox(
                      height: 120,
                      width: 120,
                      child: Stack(
                        children: [
                          Align(
                            alignment: Alignment.center,
                            child: CircleAvatar(
                              radius: 120,
                              child: ClipOval(
                                child: profileProvider.pickedImageFile == null
                                    ? const Image(
                                        image: AssetImage(
                                          AppAssets.PROFILE_IMAGE,
                                        ),
                                        width: 120,
                                        height: 120,
                                        fit: BoxFit.fill,
                                      )
                                    : Image.file(
                                        File(profileProvider.pickedImageFile!),
                                        fit: BoxFit.fill,
                                        width: 120,
                                        height: 120,
                                      ),
                              ),
                            ),
                          ),
                          const Align(
                            alignment: Alignment.bottomRight,
                            child: CircleAvatar(
                                backgroundColor: primaryColor,
                                radius: 18,
                                child: ClipOval(
                                    child: Icon(
                                  Icons.add,
                                  size: 25,
                                  color: colorWhite,
                                ))),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                const Spacer(),
                Align(
                  alignment: Alignment.center,
                  child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      minimumSize: Size(ScreenSize.getWidth(context, 0.6), 48),
                    ),
                    onPressed: onPressedContinueButton,
                    child: const Text("Continue"),
                  ),
                ),
                sizedBoxHeight(ScreenSize.getHeight(context, 0.05)),
              ],
            ),
          ),
        ),
      ],
    );
  }

  onPressedContinueButton() async {
    if (profileProvider.pickedImageFile != null) {
      AppStateServiceProvider.instance.showLoader;
      ApiResponse response = await ProfileRepository.instance
          .updateAvatar(File(profileProvider.pickedImageFile!));
      AppStateServiceProvider.instance.hideLoader;
      if (!response.result && mounted) {
        await showOKDialog(
          context: context,
          labelTitle: "Error !",
          labelContent: response.message,
        );
      } else if (response.result) {
        widget.pageController.nextPage(
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeInOut,
        );
      }
    } else {
      await showOKDialog(
        context: context,
        labelTitle: "Error !",
        labelContent: "Please pick image to continue\n or Skip this process",
      );
    }
  }

  Future<void> _uploadProfileImage(BuildContext context) async {
    String? imagePath = await ImageUtil()
        .getImage(context: context, title: "Select Profile Picture");
    if (imagePath != null && imagePath.isNotEmpty) {
      profileProvider.setPickedImageFile = imagePath;
    }
  }
}
