import 'package:country_pickers/country.dart';
import 'package:country_pickers/country_picker_dialog.dart';
import 'package:country_pickers/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:google_place/google_place.dart';
import 'package:wiki_events/app/app.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/profile/profile.dart';
import 'package:wiki_events/utils/utils.dart';

class PersonalDetails extends StatefulWidget {
  final PageController pageController;
  const PersonalDetails({Key? key, required this.pageController})
      : super(key: key);

  @override
  State<PersonalDetails> createState() => _PersonalDetailsState();
}

class _PersonalDetailsState extends State<PersonalDetails> {
  late TextEditingController _firstNameController;
  late TextEditingController _lastNameController;
  late TextEditingController _phoneNumberController;
  late TextEditingController _homeLocationController;
  late TextEditingController _aboutController;

  final GlobalKey<FormState> _formKey = GlobalKey();

  Country? selectedCountry = CountryPickerUtils.getCountryByPhoneCode("61");

  late FocusNode phoneFocus;
  late FocusNode addressFocusNode;

  late GooglePlace googlePlace;
  static const String placeAPIKey = 'AIzaSyC1i_5c6ZcxMrN3oJdVqTSesN6guLUf33k';

  DetailsResult? detailsResult = DetailsResult();

  late ProfileProvider profileProvider;

  @override
  void initState() {
    phoneFocus = FocusNode();
    addressFocusNode = FocusNode();
    _firstNameController = TextEditingController();
    _lastNameController = TextEditingController();
    _phoneNumberController = TextEditingController();
    _homeLocationController = TextEditingController();
    _aboutController = TextEditingController();

    googlePlace = GooglePlace(placeAPIKey);
    setUpFilledData();
    super.initState();
  }

  @override
  void dispose() {
    _firstNameController.dispose();
    _lastNameController.dispose();
    _phoneNumberController.dispose();
    _homeLocationController.dispose();
    _aboutController.dispose();
    phoneFocus.dispose();
    addressFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    profileProvider = Provider.of<ProfileProvider>(context, listen: true);
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Padding(
        padding: const EdgeInsets.only(top: 20.0),
        child: CustomScrollView(
          slivers: [
            SliverFillRemaining(
              hasScrollBody: false,
              fillOverscroll: true,
              child: Container(
                width: double.infinity,
                padding: const EdgeInsets.symmetric(horizontal: 3),
                child: Form(
                  key: _formKey,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        'What should we call you?',
                        style: size16_M_semibold(),
                      ),
                      sizedBoxHeight(25),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'First Name',
                            style: size14_M_regular(),
                          ),
                          sizedBoxHeight(4),
                          CustomTextField(
                            textController: _firstNameController,
                            isError: profileProvider.isFNameError,
                            keybordType: TextInputType.name,
                            textCapitalization: TextCapitalization.sentences,
                            onTapped: () {
                              profileProvider.isFNameError = false;
                            },
                            hintText: "",
                            labelText: "Enter first name here",
                          ),
                        ],
                      ),
                      sizedBoxHeight(20),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Last Name',
                            style: size14_M_regular(),
                          ),
                          sizedBoxHeight(4),
                          CustomTextField(
                            textController: _lastNameController,
                            isError: profileProvider.isLNameError,
                            keybordType: TextInputType.name,
                            textCapitalization: TextCapitalization.sentences,
                            onTapped: () {
                              profileProvider.isLNameError = false;
                            },
                            hintText: "",
                            labelText: "Enter last name here",
                          ),
                        ],
                      ),
                      sizedBoxHeight(20),
                      profileProvider.categoryList.isNotEmpty
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'Which best describes you?',
                                  style: size16_M_semibold(),
                                ),
                                Column(
                                  children: [
                                    SizedBox(
                                      height: 235,
                                      child: ListView.builder(
                                        padding: EdgeInsets.zero,
                                        shrinkWrap: true,
                                        physics:
                                            const NeverScrollableScrollPhysics(),
                                        itemCount:
                                            profileProvider.categoryList.length,
                                        itemBuilder: (context, index) {
                                          return Padding(
                                            padding:
                                                const EdgeInsets.only(top: 10),
                                            child: Row(
                                              children: [
                                                Transform.scale(
                                                  scale: 1.1,
                                                  child: Radio(
                                                    activeColor: colorBlack,
                                                    visualDensity:
                                                        const VisualDensity(
                                                      vertical: VisualDensity
                                                          .minimumDensity,
                                                    ),
                                                    value: profileProvider
                                                        .categoryList[index]
                                                        .id!,
                                                    groupValue: profileProvider
                                                        .selectedCategoryValue,
                                                    onChanged: (value) {
                                                      profileProvider
                                                              .setSelectedCategoryValue =
                                                          value as int;
                                                    },
                                                  ),
                                                ),
                                                sizedBoxWidth(3),
                                                Text(
                                                  profileProvider
                                                      .categoryList[index]
                                                      .name!,
                                                  style: size16_M_regular(),
                                                ),
                                              ],
                                            ),
                                          );
                                        },
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            )
                          : Container(),
                      // sizedBoxHeight(10),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Phone Number',
                            style: size14_M_regular(),
                          ),
                          sizedBoxHeight(4),
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              GestureDetector(
                                onTap: () {
                                  FocusScope.of(context)
                                      .requestFocus(phoneFocus);
                                  _openCountryPickerDialog();
                                },
                                child: Container(
                                  height: 48,
                                  padding: const EdgeInsets.all(10),
                                  decoration: BoxDecoration(
                                      color: colorWhite,
                                      border: Border.all(
                                        color: colorGrey,
                                      ),
                                      borderRadius: BorderRadius.circular(8)),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    children: <Widget>[
                                      ClipOval(
                                        child: SizedBox.fromSize(
                                            size: const Size.fromRadius(11),
                                            child: CountryPickerUtils
                                                .getDefaultFlagImage(
                                                    selectedCountry!)),
                                      ),
                                      sizedBoxWidth(6),
                                      Text("(+${selectedCountry!.phoneCode})",
                                          style: size14_M_medium()),
                                      sizedBoxWidth(7),
                                      const Icon(
                                        Icons.expand_circle_down_outlined,
                                        size: 22,
                                        color: colorBlack,
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              sizedBoxWidth(10),
                              Expanded(
                                child: SizedBox(
                                  height: 48,
                                  child: CustomTextField(
                                    textController: _phoneNumberController,
                                    isError: profileProvider.isPNumberError,
                                    keybordType: TextInputType.phone,
                                    focusNode: phoneFocus,
                                    inputFormat: <TextInputFormatter>[
                                      FilteringTextInputFormatter.digitsOnly
                                    ],
                                    onTapped: () {
                                      profileProvider.isPNumberError = false;
                                    },
                                    hintText: "",
                                    labelText: "Enter phone number here",
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      sizedBoxHeight(16),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Home Location',
                            style: size14_M_regular(),
                          ),
                          sizedBoxHeight(8),
                          CustomTextField(
                            hintText: "",
                            labelText: "lookup home address",
                            maxline: null,
                            focusNode: addressFocusNode,
                            isError: profileProvider.hLocationError,
                            textController: _homeLocationController,
                            keybordType: TextInputType.text,
                            textInputAction: TextInputAction.done,
                            onChanged: (value) {
                              if (value.isNotEmpty) {
                                autoCompleteSearch(value);
                              } else {
                                profileProvider.setSearchedPredictions = [];
                              }
                              profileProvider.pickedLocation = null;
                            },
                            onTapped: () {
                              profileProvider.hLocationError = false;
                            },
                          ),
                          sizedBoxHeight(8),
                          Container(
                            height: profileProvider.predictions!.isEmpty ||
                                    _homeLocationController.text.isEmpty
                                ? 0
                                : 280,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: Colors.transparent,
                              border: Border.all(
                                color: darkGreyColor,
                              ),
                            ),
                            child: ListView.builder(
                              primary: true,
                              shrinkWrap: true,
                              padding: EdgeInsets.zero,
                              itemCount: profileProvider.predictions!.length,
                              itemBuilder: (context, index) {
                                return ListTile(
                                  title: Text(
                                      profileProvider
                                          .predictions![index].description!,
                                      style: size12_M_medium(
                                          textColor: colorBlack)),
                                  leading: const CircleAvatar(
                                      radius: 12,
                                      backgroundColor: primaryColor,
                                      child: Icon(Icons.pin_drop,
                                          size: 20, color: colorBlack)),
                                  onTap: () {
                                    _homeLocationController.text =
                                        profileProvider
                                            .predictions![index].description!;

                                    profileProvider.pickedLocation =
                                        profileProvider.predictions![index];

                                    profileProvider.setSearchedPredictions = [];
                                    profileProvider.hLocationError = false;
                                  },
                                );
                              },
                            ),
                          ),
                        ],
                      ),
                      sizedBoxHeight(16),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'About (Optional)',
                            style: size14_M_regular(),
                          ),
                          sizedBoxHeight(8),
                          TextFormField(
                            controller: _aboutController,
                            maxLines: 4,
                            autovalidateMode:
                                AutovalidateMode.onUserInteraction,
                            keyboardType: TextInputType.text,
                            style: size14_M_medium(),
                            textCapitalization: TextCapitalization.sentences,
                            decoration: InputDecoration(
                              border: OutlineInputBorder(
                                  borderSide:
                                      const BorderSide(color: colorGrey),
                                  borderRadius: BorderRadius.circular(8)),
                              enabledBorder: OutlineInputBorder(
                                  borderSide:
                                      const BorderSide(color: colorGrey),
                                  borderRadius: BorderRadius.circular(8)),
                              alignLabelWithHint: true,
                              label: const Text(
                                "tell us a bit about your self",
                                textAlign: TextAlign.start,
                              ),
                              hintText: "",
                            ),
                            onFieldSubmitted: (value) =>
                                FocusScope.of(context).unfocus(),
                            onSaved: (value) =>
                                FocusScope.of(context).unfocus(),
                          ),
                        ],
                      ),
                      sizedBoxHeight(40),
                      const Spacer(),
                      Align(
                        alignment: Alignment.center,
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            minimumSize:
                                Size(ScreenSize.getWidth(context, 0.6), 48),
                          ),
                          onPressed: () async {
                            FocusManager.instance.primaryFocus?.unfocus();
                            onPressedContinueButton();
                          },
                          child: const Text("Continue"),
                        ),
                      ),
                      sizedBoxHeight(32),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  setUpFilledData() {
    ProfileUpdateRequestParam? request =
        Provider.of<ProfileProvider>(context, listen: false).updateRequest;
    if (request != null) {
      _firstNameController.text = request.firstName!;
      _firstNameController.selection = TextSelection(
          baseOffset: request.firstName!.length,
          extentOffset: request.firstName!.length);

      _lastNameController.text = request.lastName!;
      _lastNameController.selection = TextSelection(
          baseOffset: request.lastName!.length,
          extentOffset: request.lastName!.length);

      selectedCountry =
          CountryPickerUtils.getCountryByPhoneCode(request.areaCode.toString());

      _phoneNumberController.text = request.phone!;
      _phoneNumberController.selection = TextSelection(
          baseOffset: request.phone!.length,
          extentOffset: request.phone!.length);

      _homeLocationController.text = request.homeLocation!;
      _homeLocationController.selection = TextSelection(
          baseOffset: request.homeLocation!.length,
          extentOffset: request.homeLocation!.length);

      _aboutController.text = request.about!;
      _aboutController.selection = TextSelection(
          baseOffset: request.about!.length,
          extentOffset: request.about!.length);
    }
  }

  void autoCompleteSearch(String value) async {
    var result = await googlePlace.autocomplete.get(value);
    // for check error msg
    // var details = await googlePlace.autocomplete.getJson(value);
    // debugPrint(details.toString());
    //
    if (result != null && result.predictions != null && mounted) {
      Provider.of<ProfileProvider>(context, listen: false)
          .setSearchedPredictions = result.predictions!;
    }
  }

  Future<DetailsResult?> getDetails(String placeId) async {
    var result = await googlePlace.details.get(placeId);
    if (result != null && result.result != null && mounted) {
      detailsResult = result.result!;
      return detailsResult;
    }
    return null;
  }

  void _openCountryPickerDialog() => showDialog(
        context: context,
        builder: (context) => Theme(
          data:
              Theme.of(context).copyWith(dialogBackgroundColor: bgColorECF4F1),
          child: CountryPickerDialog(
            titlePadding: const EdgeInsets.all(8),
            searchCursorColor: Colors.black,
            searchInputDecoration: const InputDecoration(hintText: 'Search...'),
            isSearchable: true,
            title: Text(
              'Select your Country code',
              style: size18_M_medium(),
            ),
            onValuePicked: (Country country) =>
                setState(() => selectedCountry = country),
            priorityList: const [
              // CountryPickerUtils.getCountryByIsoCode('TR'),
              // CountryPickerUtils.getCountryByIsoCode('AU'),
            ],
            itemBuilder: _buildDropdownItem,
          ),
        ),
      );

  Widget _buildDropdownItem(Country country) => Row(
        children: <Widget>[
          SizedBox(
              height: 20,
              width: 30,
              child: CountryPickerUtils.getDefaultFlagImage(country)),
          sizedBoxWidth(8),
          Expanded(
            child: Row(
              children: [
                Text(
                  "(+${country.phoneCode})   ",
                  style: size12_M_regular(),
                ),
                Expanded(
                  child: Text(
                    country.name,
                    style: size12_M_regular(),
                  ),
                ),
              ],
            ),
          ),
        ],
      );

  onPressedContinueButton() async {
    AutocompletePrediction? location = profileProvider.pickedLocation;
    int? selectedCategory = profileProvider.selectedCategoryValue;

    bool isError = profileProvider.validationCheck(
        fName: _firstNameController.text,
        lName: _lastNameController.text,
        phone: _phoneNumberController.text);

    if (isError) {
      await showOKDialog(
        context: context,
        labelTitle: "Error !",
        labelContent: profileProvider.errorMessage,
      );
    } else {
      detailsResult = await getDetails(location!.placeId ?? "");

      ProfileUpdateRequestParam request = ProfileUpdateRequestParam(
        firstName: _firstNameController.text,
        lastName: _lastNameController.text,
        areaCode: selectedCountry!.phoneCode,
        phone: _phoneNumberController.text,
        categoryId: selectedCategory,
        homeLocation: location.description,
        homeLat: detailsResult!.geometry!.location!.lat!.toString(),
        homeLong: detailsResult!.geometry!.location!.lng!.toString(),
        about: _aboutController.text,
      );
      profileProvider.setprofileUpdateReq = request;
      AppStateServiceProvider.instance.showLoader;
      ApiResponse response =
          await ProfileRepository.instance.completeProfile(request);
      AppStateServiceProvider.instance.hideLoader;
      if (!response.result && mounted) {
        await showOKDialog(
          context: context,
          labelTitle: "Error !",
          labelContent: response.message,
        );
      } else if (response.result) {
        widget.pageController.nextPage(
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeInOut,
        );
      }
    }
  }
}
