import 'dart:io';

import 'package:wiki_events/auth/auth.dart';
import 'package:wiki_events/common/common.dart';
import 'package:wiki_events/profile/profile.dart';

class ProfileRepository extends BaseApi {
  ProfileRepository._()
      : super(interceptors: [
          ClientKeyInterceptor(),
          PrettyDioLogger(),
          AccessTokenInterceptor()
        ]);

  static ProfileRepository? _instance;

  static ProfileRepository get instance {
    _instance ??= ProfileRepository._();
    return _instance!;
  }

  Future<ApiResponse<User?>> getProfile() async {
    try {
      Response response = await get('profile');
      AuthUserResponse authUserResponse =
          AuthUserResponse.fromJson(response.data);
      if (authUserResponse.result != null && authUserResponse.result!) {
        if (authUserResponse.payload != null) {
          await AuthServiceProvider.instance
              .refreshUser(authUserResponse.payload);
          return ApiResponse<User?>(
            result: true,
            message: authUserResponse.message ?? 'Success',
            payload: authUserResponse.payload,
          );
        }
      }
      return ApiResponse<User?>(
        result: false,
        message: authUserResponse.message ?? 'Failed',
        payload: null,
      );
    } catch (e) {
      return ApiResponse<User?>(
        result: false,
        message: e.toString(),
        payload: null,
      );
    }
  }

  Future<ApiResponse<User?>> completeProfile(
      ProfileUpdateRequestParam request) async {
    try {
      Response response = await put('profile', data: request.toJson());
      AuthUserResponse authUserResponse =
          AuthUserResponse.fromJson(response.data);
      if (authUserResponse.result != null && authUserResponse.result!) {
        if (authUserResponse.payload != null) {
          // await AuthServiceProvider.instance
          //     .refreshUser(authUserResponse.payload);
          return ApiResponse<User?>(
            result: true,
            message: authUserResponse.message ?? 'Success',
            payload: authUserResponse.payload,
          );
        }
      }
      return ApiResponse<User?>(
        result: false,
        message: authUserResponse.message ?? 'Failed',
        payload: null,
      );
    } catch (e) {
      return ApiResponse<User?>(
        result: false,
        message: e.toString(),
        payload: null,
      );
    }
  }

  Future<ApiResponse<User?>> updateAvatar(File file) async {
    try {
      Response response = await post('profile/avatar',
          data: FormData.fromMap({
            "image": await MultipartFile.fromFile(file.path),
          }));
      AuthUserResponse authUserResponse =
          AuthUserResponse.fromJson(response.data);
      if (authUserResponse.result != null && authUserResponse.result!) {
        if (authUserResponse.payload != null) {
          return ApiResponse<User?>(
            result: true,
            message: authUserResponse.message ?? 'Success',
            payload: authUserResponse.payload,
          );
        }
      }
      return ApiResponse<User?>(
        result: false,
        message: authUserResponse.message ?? 'Failed',
        payload: null,
      );
    } catch (e) {
      return ApiResponse<User?>(
        result: false,
        message: e.toString(),
        payload: null,
      );
    }
  }

  Future<ApiResponse<List<UserCategory?>>> getCategoryData() async {
    try {
      Response response = await get('users/categories');
      CategoryResModel categoryResModel =
          CategoryResModel.fromJson(response.data);
      if (categoryResModel.result != null && categoryResModel.result!) {
        if (categoryResModel.categoryList != null) {
          ProfileProvider.instance.setCategoryList =
              categoryResModel.categoryList!;
          return ApiResponse<List<UserCategory?>>(
            result: true,
            message: categoryResModel.message ?? 'Success',
            payload: categoryResModel.categoryList,
          );
        }
      }
      return ApiResponse<List<UserCategory?>>(
        result: false,
        message: categoryResModel.message ?? 'Failed',
        payload: null,
      );
    } catch (e) {
      return ApiResponse<List<UserCategory?>>(
        result: false,
        message: e.toString(),
        payload: null,
      );
    }
  }
}
