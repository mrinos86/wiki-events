import 'package:flutter/material.dart';
import 'package:wiki_events/app/provider/provider.dart';
import 'package:wiki_events/auth/auth.dart';
import 'package:wiki_events/profile/profile.dart';
import 'package:wiki_events/utils/utils.dart';

class CompleteProfilePage extends StatefulWidget {
  const CompleteProfilePage({super.key});

  @override
  State<CompleteProfilePage> createState() => _CompleteProfilePageState();
}

class _CompleteProfilePageState extends State<CompleteProfilePage> {
  late PageController _pageController;
  int _selectedPageIndex = 0;

  @override
  void initState() {
    _pageController = PageController(initialPage: _selectedPageIndex);
    getCategoryDetails();
    debugPrint("Complete pro");
    super.initState();
  }

  Future<void> getCategoryDetails() async {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      AppStateServiceProvider.instance.showLoader;
      await ProfileRepository.instance.getCategoryData();
      AppStateServiceProvider.instance.hideLoader;
    });
  }

  @override
  void dispose() {
    _pageController.dispose();
    super.dispose();
  }

  String getStepTitle() {
    switch (_selectedPageIndex) {
      case 0:
        return "Welcome";
      case 1:
        return "Profile";
      case 2:
        return "Start Exploring";
      default:
        return "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: GestureDetector(
        onTap: () => FocusScope.of(context).unfocus(),
        child: Scaffold(
          appBar: AppBar(
            // title: const Text("Complete Profile"),
            automaticallyImplyLeading: false,
            leading: Builder(builder: (context) {
              if (_selectedPageIndex > 0) {
                return IconButton(
                  onPressed: () {
                    _pageController.previousPage(
                      duration: const Duration(milliseconds: 300),
                      curve: Curves.easeInOut,
                    );
                  },
                  icon: const Icon(Icons.arrow_back_ios),
                );
              }
              return const SizedBox.shrink();
            }),

            actions: [
              if (_selectedPageIndex == 1)
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: InkWell(
                    onTap: () {
                      _pageController.nextPage(
                        duration: const Duration(milliseconds: 300),
                        curve: Curves.easeInOut,
                      );
                      // AppStateServiceProvider.instance.showLoader;
                      // AuthServiceProvider.instance.signOut();
                      // AppStateServiceProvider.instance.hideLoader;
                    },
                    customBorder: const CircleBorder(),
                    child: Text("Skip", style: size16_M_regular()),
                  ),
                ),
              if (_selectedPageIndex == 0)
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: InkWell(
                    onTap: () {
                      Provider.of<ProfileProvider>(context, listen: false)
                          .clearProfileProviderData();
                      AppStateServiceProvider.instance.showLoader;
                      AuthServiceProvider.instance.signOut();
                      AppStateServiceProvider.instance.hideLoader;
                    },
                    customBorder: const CircleBorder(),
                    child: Text("Logout", style: size16_M_regular()),
                  ),
                ),
            ],
          ),
          body: Builder(
            builder: (context) {
              // if (isClient) {
              //   return const ClientBasicDetails();
              // } else {
              return Padding(
                padding: const EdgeInsets.all(15.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(getStepTitle(), style: size22_M_bold()),
                    sizedBoxHeight(15),
                    Row(
                      children: [
                        for (int i = 0; i < 3; i++)
                          SizedBox(
                            width: MediaQuery.of(context).size.width / 7.5,
                            child: AnimatedContainer(
                              duration: const Duration(milliseconds: 300),
                              margin: const EdgeInsets.symmetric(horizontal: 2),
                              height: 5,
                              decoration: ShapeDecoration(
                                shape: const StadiumBorder(),
                                color: _selectedPageIndex >= i
                                    ? primaryColor
                                    : Colors.grey,
                              ),
                            ),
                          ),
                      ],
                    ),
                    Expanded(
                      child: PageView(
                        controller: _pageController,
                        onPageChanged: (index) {
                          setState(() {
                            _selectedPageIndex = index;
                          });
                        },
                        physics: const NeverScrollableScrollPhysics(),
                        children: [
                          PersonalDetails(
                            pageController: _pageController,
                          ),
                          ProfilePhoto(pageController: _pageController),
                          AllSet(pageController: _pageController),
                        ],
                      ),
                    ),
                  ],
                ),
              );
              // }
            },
          ),
        ),
      ),
    );
  }
}
