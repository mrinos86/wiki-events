import 'package:flutter/material.dart';
import 'package:google_place/google_place.dart';
import 'package:wiki_events/profile/profile.dart';

class ProfileProvider with ChangeNotifier {
  ProfileProvider._() : super();

  static ProfileProvider? _instance;

  static ProfileProvider get instance {
    _instance ??= ProfileProvider._();
    return _instance!;
  }

  int? selectedCategoryValue;
  List<AutocompletePrediction>? predictions = [];
  AutocompletePrediction? pickedLocation;

  List<UserCategory> categoryList = [];
  ProfileUpdateRequestParam? updateRequest;

  String? pickedImageFile;

  set setCategoryList(List<UserCategory> list) {
    categoryList = list;
    notifyListeners();
  }

  set setSelectedCategoryValue(int? value) {
    selectedCategoryValue = value;
    notifyListeners();
  }

  set setSearchedPredictions(List<AutocompletePrediction>? value) {
    predictions = value;
    notifyListeners();
  }

  set setPickedLocation(AutocompletePrediction? value) {
    pickedLocation = value;
    notifyListeners();
  }

  set setprofileUpdateReq(ProfileUpdateRequestParam? request) {
    updateRequest = request;
    notifyListeners();
  }

  set setPickedImageFile(String? value) {
    pickedImageFile = value;
    notifyListeners();
  }

  void clearProfileProviderData() {
    selectedCategoryValue = null;
    predictions = [];
    pickedLocation = null;
    categoryList = [];
    updateRequest = null;
    pickedImageFile = null;
  }

  bool isFNameError = false;
  bool isLNameError = false;
  bool isPNumberError = false;
  bool hLocationError = false;
  bool categoryError = false;
  String errorMessage = "";

  bool validationCheck(
      {required String fName, required String lName, required String phone}) {
    bool validationError = false;

    if (pickedLocation == null) {
      hLocationError = true;
      errorMessage = "Please select Home Location";
      validationError = true;
    }

    if (phone.isEmpty) {
      isPNumberError = true;
      errorMessage = "Please enter Phone Number";
      validationError = true;
    }

    if (selectedCategoryValue == null) {
      errorMessage = "Please select any Category";
      validationError = true;
    }
    if (lName.isEmpty) {
      isLNameError = true;
      errorMessage = "Please enter Last Name";
      validationError = true;
    }
    if (fName.isEmpty) {
      isFNameError = true;
      errorMessage = "Please enter First Name ";
      validationError = true;
    }
    notifyListeners();
    return validationError;
  }
}
