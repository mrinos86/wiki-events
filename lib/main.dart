import 'package:wiki_events/app/view/app_main.dart';
import 'package:wiki_events/bootstrap.dart';

void main() {
  bootstrap(() => const WikiEventsApp());
}
